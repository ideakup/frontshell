<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'map_country';

    public function variable()
    {
        //$currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\CountryVariable', 'country_id', 'id');
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\CountryVariable', 'country_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\CountryVariable', 'country_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany('App\City', 'country_id', 'id');
    }
    
}
