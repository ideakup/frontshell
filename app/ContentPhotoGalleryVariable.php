<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentPhotoGalleryVariable extends Model
{
    protected $table = 'content_photogalleryvariable';

    public function content()
    {
        return $this->belongsTo('App\Content', 'id', 'content_id');
    }
   
}
