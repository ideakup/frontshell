<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuVariable extends Model
{
    protected $table = 'menuvariable';

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id', 'id');
    }
}
