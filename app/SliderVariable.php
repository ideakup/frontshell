<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderVariable extends Model
{
    protected $table = 'slidervariable';

    public function slider()
    {
        return $this->belongsTo('App\Slider', 'slider_id', 'id');
    }

}
