<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigKey extends Model
{
    protected $table = 'config_key';
}
