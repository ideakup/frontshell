<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Menu extends Model
{
    protected $table = 'menu';

    /*
        public function content()
        {
            return $this->belongsToMany('App\Content', 'menu_has_content')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
        }
    
        public function content()
        {
            return $this->belongsToMany('App\Content', 'top_has_sub', 'top_menu_id', 'sub_content_id')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
        }

        public function menuHasContent()
        {
            return $this->hasMany('App\MenuHasContent', 'menu_id', 'id')->orderBy('order', 'asc');
        }
    */

    public function topHasSub()
    {
        return $this->hasMany('App\NewTopHasSub', 'sub_id', 'id')->whereIn('sub_model', ['Menu', 'Module', 'Template'])->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function topHasSubContent()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('top_model','Menu')->where('status', 'active')->whereIn('sub_model', ['Content', 'MenuSum'])->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function topHasSubMenu()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('status', 'active')->where('sub_model', 'Menu')->where('deleted', 'no')->orderBy('order', 'asc');
    }

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\MenuVariable', 'menu_id', 'id');
    }

    public function slider()
    {
        return $this->hasMany('App\Slider', 'menu_id', 'id')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
}
