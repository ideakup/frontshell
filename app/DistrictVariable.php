<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistrictVariable extends Model
{
    protected $table = 'map_districtvariable';

    public function district()
    {
        return $this->hasOne('App\District', 'id', 'district_id');
    }

}
