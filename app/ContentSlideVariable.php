<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentSlideVariable extends Model
{
    protected $table = 'content_slidevariable';

    public function slide()
    {
        return $this->belongsTo('App\Content', 'content_id', 'id');
    }

}
