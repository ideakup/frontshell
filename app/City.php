<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'map_city';

    public function variable()
    {
        //$currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\CityVariable', 'city_id', 'id');
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\CityVariable', 'city_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\CityVariable', 'city_id', 'id');
    }

    public function counties()
    {
        return $this->hasMany('App\County', 'city_id', 'id');
    }

    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }
}
