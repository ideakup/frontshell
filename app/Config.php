<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';

    public function topKey()
    {   
        return $this->hasOne('App\Config', 'id', 'top_id');
    }

    public function subKey()
    {   
        return $this->hasMany('App\Config', 'top_id', 'id')->orderBy('order');
    }

    public function key()
    {
        return $this->belongsTo('App\ConfigKey');
    }

    public function value()
    {
        return $this->belongsTo('App\ConfigValue');
    }
}
