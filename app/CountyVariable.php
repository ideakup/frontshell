<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountyVariable extends Model
{
    protected $table = 'map_countyvariable';

    public function county()
    {
        return $this->hasOne('App\County', 'id', 'county_id');
    }
}
