<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigValue extends Model
{
    protected $table = 'config_value';
}
