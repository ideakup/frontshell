<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingOrderSettings extends Model
{
    protected $table = 'shopping_order_settings';
}
