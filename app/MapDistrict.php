<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapDistrict extends Model
{
    protected $table = 'map_district';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\MapDistrictVariable', 'district_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\MapDistrictVariable', 'district_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\MapDistrictVariable', 'district_id', 'id');
    }
}
