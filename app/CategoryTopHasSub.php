<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTopHasSub extends Model
{
	protected $table = 'category_top_has_sub';
	
	public function variable()
    {
        return $this->hasOne('App\CategoryVariable', 'category_id', 'category_id');
    }
    public function variableLang($langcode)
    {
        return $this->hasOne('App\CategoryVariable', 'category_id', 'category_id')->where('lang_code', $langcode)->first();
    }
    public function topCategoryVariable()
    {
        return $this->hasOne('App\CategoryVariable', 'category_id', 'top_category_id');
    }
    public function topCat()
    {
        return $this->hasOne('App\CategoryTopHasSub', 'category_id', 'top_category_id')->where('deleted', 'no');
    }
    public function subCat()
    {
        return $this->hasMany('App\CategoryTopHasSub', 'top_category_id', 'category_id')->where('deleted', 'no');
    }
    public function contentHasCategory()
    {
    return $this->hasMany('App\ContentHasCategory', 'category_id', 'id');
    }
    
}
