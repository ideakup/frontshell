<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuHasContent2 extends Model
{
    protected $table = 'menu_has_content';

    public function content()
    {
        return $this->hasOne('App\Content', 'id', 'content_id')->where('deleted', 'no')->where('status', 'active');
    }

    public function menu()
    {
        return $this->hasOne('App\Menu', 'id', 'content_id')->where('deleted', 'no')->where('status', 'active');
    }
}
