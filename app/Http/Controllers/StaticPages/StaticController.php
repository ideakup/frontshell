<?php

namespace App\Http\Controllers\StaticPages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\ShoppingAdresses;
use App\ShoppingCheckout;
use App\Menu;
use App\Shoppingtest;
use App\User;
use Cart;
use App\ContentVariable;
use Validator;
use Illuminate\Support\Facades\Input;
use Mail;
use App\Mail\ForgotPassword;
use App\ShoppingOrder;
use App\Mail\OrderShipped;
use App\ShoppingOrderSettings;
use App\Domain;
use Redirect;
use App\ModelHasRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\FormData;
use App\NewTopHasSub;
use App\Content;
use App\Mail\FormSendMail;





class StaticController extends Controller{



    public function logout(){
    
      if (isset($_COOKIE['shop_cookie'])) {
        unset($_COOKIE['shop_cookie']); 
        setcookie('shop_cookie', null, -1, '/');
      }   
      Auth::logout();

      return redirect('/');
    }

    public function changeLang(Request $request){

      setcookie("lang_cookie", $request->lang, time() + (86400 * 30), "/"); // 86400 = 1 day

      $menu = Menu::find($request->menu_id);
            

      return redirect('/'.$menu->variableLang($request->lang)->slug);
      
    }

    public function form_save(Request $request){

        $request=$request->input();
        $jsonEncodeReq = json_encode($request);
        $jsonDecodeReq = json_decode($jsonEncodeReq, true);
        Arr::pull($jsonDecodeReq, '_token');
        Arr::pull($jsonDecodeReq, 'lang');
        Arr::pull($jsonDecodeReq, 'source_type');
        Arr::pull($jsonDecodeReq, 'source_id');
        Arr::pull($jsonDecodeReq, 'form_id');

        $message = array(
            'success'           => 'Başarıyla Kaydedildi. Teşekkür Ederiz.',
            'error'             => 'Bir Hata Oluştu.',//'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.',
            'error_bot'         => 'Bot Detected! Form could not be processed! Please Try Again!',
            'error_unexpected'  => 'An <strong>unexpected error</strong> occured. Please Try Again later.',
            'recaptcha_invalid' => 'Captcha not Validated! Please Try Again!',
            'recaptcha_error'   => 'Captcha not Submitted! Please Try Again.'
        );

        $formdata = new FormData();
        $formdata->lang_code = $request['lang'];
        $formdata->source_type = $request['source_type'];
        $formdata->source_id = $request['source_id'];
        $formdata->form_id = $request['form_id'];
        $formdata->data = json_encode($jsonDecodeReq);
     
        $formdata->save();

            $ths = NewTopHasSub::find($request['ths_id']);
            $content = Content::where('id',$request['form_id'])->first();
            $contentVar = $content->variableLang($request['lang'])->content;

        if(empty(json_decode($ths->props)->props_eposta)){
            echo '{ "alert": "success", "message": "' . $message['success'] . '" }';

        }
        else{
            Mail::to([json_decode($ths->props)->props_eposta])->send(new FormSendMail($formdata, $contentVar));

            echo '{ "alert": "success", "message": "' . $message['success'] . '" }';
        //return back()->with('status', 'Profile updated!');

        }
      
    }

    public function infowithoutlogin(Request $request){

      $rules = array( 
        'name' => 'required',
        'lastname' => 'required',
        'email' => 'required|email',
        'identity_no' => 'required|digits:11',
        'phone' => 'required|digits:10',
        'city' => 'required',
        'town' => 'required',
        'adress' => 'required',
        'adressname' => 'required',
      );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
          return \Redirect::back()->withErrors($validator)->withInput();
      }

  	  $addres = new ShoppingAdresses();
             
      $addres->email=$request->email;
      $addres->city=$request->city;
      $addres->town=$request->town;
      $addres->adress_name=$request->adressname;

      $addresses = array();
      $addresses['firstname'] = $request->name;
      $addresses['lastname'] = $request->lastname;
      $addresses["address"] = $request->adress;
      $addresses['identity_no'] = $request->identity_no;
      $addresses['phone'] = $request->phone;
      $addres->adress= json_encode($addresses);

      $addres->save();

      $identifier = $_COOKIE["shop_cookie"];
      $ShoppingCheckout = new ShoppingCheckout();
      $ShoppingCheckout->user_email=$request->email;
      $ShoppingCheckout->shop_cookie= $identifier;

      $ShoppingCheckout->save();


      return redirect('shop/checkout');
    } 

    public function register(Request $request){

      $rules = array(
          'firstname'=> 'required', 
          'lastname'=> 'required',                                               
          'email'=> 'required|email',   
          'password'=> 'required',
          'repassword'=> 'required|same:password',
      );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
         return \Redirect::back()->withErrors($validator)->withInput();
      }

      if($request->password==$request->repassword){
        $user = new User();
        $user->firstname=$request->firstname;
        $user->lastname=$request->lastname;
        $user->email=$request->email;
        $user->password=bcrypt($request->password); 
        //default
        $user->status="active";
        $user->deleted="no";

        $user->save();
          
        if(!empty($_COOKIE["shop_cookie"])){
           $identifier = $_COOKIE["shop_cookie"];
           $ShoppingCheckout = new ShoppingCheckout();
           $ShoppingCheckout->user_id=$user->id;
           $ShoppingCheckout->user_email=$request->email;
           $ShoppingCheckout->shop_cookie= $identifier;

           $ShoppingCheckout->save();
        }}else{
            dd("(şifre aynı değil)");
          }

      return redirect('user/login')->withSuccess('BAŞARI İLE KAYIT OLUNDU GİRİŞ YAPABİLİRSİNİZ');

    }
    public function subrecord(Request $request){
      
      //dd($request->input());

      $rules = array(
          'firstname'=> 'required', 
          'lastname'=> 'required',                                               
          'email' => 'required|email',
          'password'=> 'required',
          'repassword'=> 'required|same:password',
      );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
         return \Redirect::back()->withErrors($validator)->withInput();
      }

      $subcheck = ShoppingOrder::where('email',$request->email)->count();


      if($subcheck > 0){
        return Redirect::back()->withErrors(['msg' => 'Ücretsiz deneme için Önceden kullanılmış e-mail']);
      }else{

        $usercheck = User::where('email',$request->email)->count();

        if($usercheck > 0){
          $user = User::where('email',$request->email)->first();
        }else{
          $user = new User();
          $user->firstname=$request->firstname;
          $user->lastname=$request->lastname;
          $user->email=$request->email;
          $user->password=bcrypt($request->password); 
          $user->status="active";
          $user->deleted="no";

          $user->save();
        }

        $package = ContentVariable::where('content_id',15)->first();//pro webshell
        $order_content = array();
        $order_content['firstname'] = $request->firstname;
        $order_content['lastname'] = $request->lastname;
        $order_content['package_name'] = $package->title;

        $freetry_order = new ShoppingOrder();
        $freetry_order->order_no = str_random(10);
        $freetry_order->product_type = 'sub';
        $freetry_order->user_id = $user->id;
        $freetry_order->email = $user->email;
        $freetry_order->order_content = json_encode($order_content);
        $freetry_order->address_information = "free";
        $freetry_order->payment_information = "free";
        $freetry_order->status = "free";
        $freetry_order->end_date = \Carbon\Carbon::now()->addDays(15);

        $freetry_order->save();

        $subfreedomain = new Domain();
        $subfreedomain->domain_name = str_random(5).".webshell.online";
        $subfreedomain->order_no = $freetry_order->order_no;
        $subfreedomain->database_name = $freetry_order->order_no.'_'.str_slug($subfreedomain->domain_name);
        $subfreedomain->package_id = 15; // pro webshell
        
        $subfreedomain->save();

        $modelhasroles = new ModelHasRoles();
        $modelhasroles->role_id = 2;
        $modelhasroles->model_type  = 'App\User';
        $modelhasroles->model_id  = $user->id;
        $modelhasroles->domain_id  = $subfreedomain->id;

        $modelhasroles->save();

      }

      

       return redirect()->back()->with('success', 'Kayıdınız başarıyla yapıldı. Mailinizi kontrol ediniz');   

    }

    public function addresrecord(Request $request){
      $rules = array(
        'email' => 'required|email',
        'identity_no' => 'required|digits:11',
        'phone' => 'required|digits:10',
        'name' => 'required',
        'lastname' => 'required',
        'city' => 'required',
        'town' => 'required',
        'adress' => 'required',
        'adressname' => 'required',
        );
      $validator = Validator::make(Input::all(), $rules);
      if($validator->fails()){
         return \Redirect::back()->withErrors($validator)->withInput();
      }

       $addres = new ShoppingAdresses();
       $user=User::where("email",$request->email)->first();
       $addres->user_id=$user->id;
       $addres->email=$request->email;
       $addres->city=$request->city;
       $addres->town=$request->town;
       $addres->adress_name=$request->adressname;

       $addresses = array();            
       $addresses["address"] = $request->adress;
       $addresses['phone'] = $request->phone;
       $addresses['identity_no'] = $request->identity_no;
       $addres->adress= json_encode($addresses);

       $addres->save();
       if($request->shop_type == 'sub'){
          if(!is_null($request->shoppingorder_id)){
            return redirect('shop/checkoutsub/'.$request->shoppingorder_id.'/'.$request->function_type);
          }else{
            return redirect('shop/checkoutsub');
          }
          
       }else{
          return redirect('shop/checkout');
       }
    }

    public function addresupdate(Request $request){
        
      $rules = array(
        'name' => 'required',
        'lastname' => 'required',
        'email' => 'required|email',
        'identity_no' => 'required|digits:11',
        'phone' => 'required|digits:10',
        'city' => 'required',
        'town' => 'required',
        'adress' => 'required',
        'adressname' => 'required',
      );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()){
         return \Redirect::back()->withErrors($validator)->withInput();
      }

      $addres =ShoppingAdresses::where('email',$request->email)->where('user_id',null)->first();
      //dd($addres);
      $addres->email=$request->email;
      $addres->city=$request->city;
      $addres->town=$request->town;
      $addres->adress_name=$request->adressname;

      $addresses = array();
      $addresses['firstname'] = $request->name;
      $addresses['lastname'] = $request->lastname;
      $addresses["address"] = $request->adress;
      $addresses['identity_no'] = $request->identity_no;
      $addresses['phone'] = $request->phone;
      $addres->adress= json_encode($addresses);

      $addres->save();

      return redirect('shop/checkout');
    }

    public function changePassword(Request $request){
        
        $rules = array(
            'oldPassword' => 'required|string|',
            'newPassword' => 'required|string|confirmed'
        );
        

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()){
           return \Redirect::back()->withErrors($validator)->withInput();
        }
        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->oldPassword])) {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->newPassword);
            $user->save();
        }else{
            $err = array("oldPassword" => "Eski şifrenizi yanlış girdiniz.");
            return \Redirect::back()->withErrors($err)->withInput();
        }

        return redirect('user/profile')->withSuccess('Şifreniz başarı ile değişti.');
    }  

    public function changeUserInfo(Request $request){
        
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required'
        );
        

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()){
           return \Redirect::back()->withErrors($validator)->withInput();
        }
            $user = User::find(Auth::user()->id);
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;

            $user->save();
      
        return redirect('user/profile')->withSuccess('Bilgileriniz başarı ile değişti.');
    } 
    public function forgetpassword(Request $request){

        $data=User::where('email',$request->forgotemail)->first();  
        if(is_null($data)){
            return redirect('user/login')->withErrors('non_exist_email');
        }
        Mail::to("uygar@ideakup.com")->send(new ForgotPassword($data));


        return redirect('user/login')->withSuccess('MAİLİNİZİ KONTROL EDİNİZ');

    } 
    public function changeforgotPassword(Request $request){
        $rules = array(
        
          'newPassword'=> 'required',
          'newPassword_confirmation'=> 'required|same:newPassword',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $user=User::where('remember_token',$request->remember_token)->first();
        $user->password = bcrypt($request->newPassword);
        $user->remember_token = str_random(60);
        $user->save();


        return redirect('user/login')->withSuccess('Şifreniz başarı ile değiştirildi');

    } 
    public function paymentCheck(Request $request){  
          
        $request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId("123456789");
        $request->setToken($_POST['token']);

        $options = new \Iyzipay\Options();
        $options->setApiKey("sandbox-B5lK8snfpfZ9J3Vawhfxb8wmpPq5xCDA");
        $options->setSecretKey("sandbox-rXLepmr9bnFhRDmhlq0rV1xvrclo8sJc");
        $options->setBaseUrl("https://sandbox-api.iyzipay.com");

        $checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($request, $options);

    
        //dd($checkoutForm);
        if($checkoutForm->getStatus()=="success"){
            
            $instance='wl';
            $identifier=$_COOKIE["shop_cookie"];
            $order_no=$checkoutForm->getBasketId();
            $order=ShoppingOrder::where('order_no',$order_no)->first();

            $order->status=str_slug('prepare');
            $order->save();
            Cart::instance($instance)->destroy();

            $Shoppingcart=Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->delete();
            if (isset($_COOKIE['shop_cookie'])) {
                unset($_COOKIE['shop_cookie']); 
                setcookie('shop_cookie', null, -1, '/');
            }
            
            $data=$order;
            //Mail::to("uygar@ideakup.com")->send(new OrderShipped($data));
            if(is_null($order->user_id)){
        
                return redirect('user/orders/'.$order_no); 
            }
            else{
                 return redirect('user/orders'); 
            }


            
        }
        else{
            $order_no=$checkoutForm->getBasketId();
            $order=ShoppingOrder::where('order_no',$order_no)->first();

            $order->status=str_slug('fail-payment');
            $order->save();
            return redirect('shop/checkout'); 
        }


    } 
    public function iyzico_place_order(Request $request){ 
        //dd($request->input());
        $adres_id=$request->adress_id;
        $cc_no=str_replace(" ","",$request->credit_card_no);
        $cc_name=$request->credit_card_name;
        $cc_month=$request->credit_card_month;
        $cc_year=$request->credit_card_year;
        $cc_cvs=$request->credit_card_cvs;
        $current_adress=ShoppingAdresses::find($request->adress_id);
        $adress_info=json_decode($current_adress->adress);
        $order_no=str_random(10);
        $total=number_format((float)$request->total, 2, '.', '');
        $alltotal=number_format((float)$request->alltotal, 2, '.', '');
        $identifier=$request->identifier;
        $instance=$request->product_type;
        $callbackurl=url('sta/save_order/2');
        $threeD=$request->threeDsecure;
        $product_type = $request->product_type;
       
        if(!empty($request->installment)){
          $payment_info = $request->payment_info.",".$request->installment;
          $array_installment = explode("_",$request->installment);
          $installment = $array_installment[0];
        }else{
          $payment_info = $request->payment_info.","."1_".$alltotal;
          $installment = 1;
        }


        if(!empty($request->order_id)){
               $order_id= $request->order_id;
        }

        if(!is_null($current_adress->user_id)){
            $user=User::find($current_adress->user_id);
            $firstname=$user->firstname;
            $lastname=$user->lastname;  
        }
        else{
            $firstname=$adress_info->firstname;
            $lastname=$adress_info->lastname;      
        }


        //dump($adress_info);
        //dd($current_adress);

        $shop_settigs=ShoppingOrderSettings::find(1);
        $API_keys=json_decode($shop_settigs->extra);

        $options = new \Iyzipay\Options();
        $options->setApiKey($API_keys->data1);
        $options->setSecretKey($API_keys->data2);
        
        $options->setBaseUrl($API_keys->url."");


        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId($order_no."-".$identifier."-".$adres_id);
        $request->setPrice($total);
        $request->setPaidPrice($alltotal);
        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setInstallment($installment);
        $request->setBasketId($identifier);
        $request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        if(!is_null($threeD)){
            $request->setCallbackUrl($callbackurl);
        }

        $paymentCard = new \Iyzipay\Model\PaymentCard();
        
        $paymentCard->setCardHolderName($cc_name);
        $paymentCard->setCardNumber($cc_no);
        $paymentCard->setExpireMonth($cc_month);
        $paymentCard->setExpireYear($cc_year);
        $paymentCard->setCvc($cc_cvs);
        $paymentCard->setRegisterCard(0);
        $request->setPaymentCard($paymentCard);
        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($adress_info->identity_no);
        $buyer->setName($firstname);
        $buyer->setSurname($lastname);
        $buyer->setGsmNumber("$adress_info->phone");
        $buyer->setEmail($current_adress->email);
        $buyer->setIdentityNumber($adress_info->identity_no);
       
        $buyer->setRegistrationAddress($adress_info->address);
        $buyer->setIp("85.34.78.112");
        $buyer->setCity($current_adress->city);
        $buyer->setCountry("Turkey");
        $request->setBuyer($buyer);

        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName($firstname." ".$lastname);
        $shippingAddress->setCity($current_adress->city);
        $shippingAddress->setCountry("Turkey");
        $shippingAddress->setAddress($adress_info->address);
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName($firstname." ".$lastname);
        $billingAddress->setCity($current_adress->city);
        $billingAddress->setCountry("Turkey");
        $billingAddress->setAddress($adress_info->address);
        $request->setBillingAddress($billingAddress);

        $piece = 0;

        
        Cart::instance($instance)->getstore($identifier,$instance);
        $products=Cart::instance($instance)->content();

      
        foreach($products as $product){

            $BasketItem = new \Iyzipay\Model\BasketItem();
            $BasketItem->setId($product->rowId);
            $BasketItem->setName($product->name);
            $BasketItem->setCategory1($product->name);
            $BasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
            $BasketItem->setPrice($product->total);
            $basketItems[$piece] = $BasketItem;

            $piece++;
        }
        
        $request->setBasketItems($basketItems);

        if(is_null($threeD)){
            $payment = \Iyzipay\Model\Payment::create($request, $options);
        }
        else{
            $threedsInitialize = \Iyzipay\Model\ThreedsInitialize::create($request, $options);
            $html=$threedsInitialize->getHtmlContent();

            if($threedsInitialize->getstatus()=='success'){
                echo "$html";
            }
            else{
                return redirect('shop/checkoutsub')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
            }
        }
        //dd($payment);
        if($payment->getstatus()=='success'){
            if(!empty( $order_id)){
                $url='sta/save_order/2/'.$order_no.'/'.$identifier.'/'.$adres_id.'/'.$product_type.'/'. $order_id.'/'.$payment_info;
            }else{
                $url='sta/save_order/2/'.$order_no.'/'.$identifier.'/'.$adres_id.'/'.$product_type.'/0'.'/'.$payment_info;
            }
            
            return redirect($url);
        }
        else{
            if($instance == 'sub'){
              if(!empty( $order_id)){
                 return redirect('shop/checkoutsub/'. $order_id)->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
              }else{
                  return redirect('shop/checkoutsub')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
              }
            }
            elseif($instance == 'wl'){
                  return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
            }
        }
       
    } 

    public function parampos_place_order(Request $request){  
          
        //dd($request->input());


        $cc_no=str_replace(" ","",$request->credit_card_no);
        $current_adress=ShoppingAdresses::find($request->adress_id);
        $adress_info=json_decode($current_adress->adress);
        $order_no=str_random(10);
        $alltotal=number_format((float)$request->alltotal, 2, ',', '');
        $identifier=$request->identifier;

        if(is_null($request->threeDsecure)){
            $sec_type='NS';
        }
        else{
            $sec_type=$request->threeDsecure;
        }
        
        $fail_url=url('sta/save_order/1');
        $success_url=url('sta/save_order/1');
        
        
        $hash="10738"."0c13d406-873b-403b-9c09-a5766840d98c"."1".$alltotal.$alltotal.$order_no.$fail_url.$success_url;
        //dump($hash);

        $hash_data='<?xml version="1.0" encoding="utf-8"?> <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <soap:Body>
            <SHA2B64 xmlns="https://turkpos.com.tr/">
            <Data>'.$hash.'</Data>
            </SHA2B64>
            </soap:Body>
            </soap:Envelope>';
        $ch_hash = curl_init();

            // CURL options
            $options_hash = array(
                CURLOPT_URL             => 'https://test-dmz.param.com.tr:4443/turkpos.ws/service_turkpos_test.asmx?wsdl',
                CURLOPT_POST            => true,
                CURLOPT_POSTFIELDS      => $hash_data,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_HEADER          => false,
                CURLOPT_HTTPHEADER      => array(
                    'Content-Type: text/xml; charset=utf-8',
                    'XSRF-TOKEN='.$request->_token,
                    'Content-Length: '.strlen($hash_data)
                ),
            );
            curl_setopt_array($ch_hash, $options_hash);

            $response_hash = curl_exec($ch_hash);
            curl_close($ch_hash);
            
            $cleanxml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $response_hash);
            $cleanxml = str_ireplace('NS1:','', $cleanxml);
            $xml = simplexml_load_string($cleanxml);
            $hash_code=$xml->Body->SHA2B64Response->SHA2B64Result[0];
            //dump($hash_code);

       $post_data = '<?xml version="1.0" encoding="utf-8"?> <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
            <Pos_Odeme xmlns="https://turkpos.com.tr/">
            <G>
            <CLIENT_CODE>10738</CLIENT_CODE>
            <CLIENT_USERNAME>Test</CLIENT_USERNAME>
            <CLIENT_PASSWORD>Test</CLIENT_PASSWORD>
            </G>
            <GUID>0c13d406-873b-403b-9c09-a5766840d98c</GUID>
            <KK_Sahibi>'.$request['cc_name'].'</KK_Sahibi>
            <KK_No>'.$cc_no.'</KK_No>
            <KK_SK_Ay>'.$request->credit_card_month.'</KK_SK_Ay>
            <KK_SK_Yil>'.$request->credit_card_year.'</KK_SK_Yil>
            <KK_CVC>'.$request->credit_card_cvs.'</KK_CVC>
            <KK_Sahibi_GSM>'.$adress_info->phone.'</KK_Sahibi_GSM>
            <Hata_URL>'.$fail_url.'</Hata_URL>
            <Basarili_URL>'.$success_url.'</Basarili_URL>
            <Siparis_ID>'.$order_no.'</Siparis_ID>
            <Siparis_Aciklama></Siparis_Aciklama>
            <Taksit>1</Taksit>
            <Islem_Tutar>'.$alltotal.'</Islem_Tutar>
            <Toplam_Tutar>'.$alltotal.'</Toplam_Tutar>
            <Islem_Hash>'.$hash_code.'</Islem_Hash>
            <Islem_Guvenlik_Tip>'.$sec_type.'</Islem_Guvenlik_Tip>
            <Islem_ID></Islem_ID>
            <IPAdr>127.0.0.1</IPAdr>
            <Ref_URL></Ref_URL>
            <Data1>'.$order_no.'</Data1>
            <Data2>'.$identifier.'</Data2>
            <Data3>'.$request->adress_id.'</Data3>
            <Data4></Data4>
            <Data5></Data5>
            <Data6></Data6>
            <Data7></Data7>
            <Data8></Data8>
            <Data9></Data9>
            <Data10></Data10>
            </Pos_Odeme>
            </soap:Body>
            </soap:Envelope>';

            //dump($post_data);

            $ch = curl_init();

            // CURL options
            $options = array(
                CURLOPT_URL             => 'https://test-dmz.param.com.tr:4443/turkpos.ws/service_turkpos_test.asmx?wsdl',
                CURLOPT_POST            => true,
                CURLOPT_POSTFIELDS      => $post_data,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_HEADER          => false,
                CURLOPT_HTTPHEADER      => array(
                    'Content-Type: text/xml; charset=utf-8',
                    'Content-Length: '.strlen($post_data)
                ),
            );
            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $cleanxml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $response);
            $cleanxml = str_ireplace('NS1:','', $cleanxml);
            $xml = simplexml_load_string($cleanxml);

            $Islem_ID=$xml->Body->Pos_OdemeResponse->Pos_OdemeResult->Islem_ID;
            $UCD_URL=$xml->Body->Pos_OdemeResponse->Pos_OdemeResult->UCD_URL;
            $Sonuc=$xml->Body->Pos_OdemeResponse->Pos_OdemeResult->Sonuc;
            $Sonuc_Str=$xml->Body->Pos_OdemeResponse->Pos_OdemeResult->Sonuc_Str;
            $Banka_Sonuc_Kod=$xml->Body->Pos_OdemeResponse->Pos_OdemeResult->Banka_Sonuc_Kod;

            if($Sonuc<1){
                return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
            }
            else{
                //dd($xml);
                if($UCD_URL=='NONSECURE'){
                    $url='sta/save_order/1/'.$order_no.'/'.$identifier.'/'.$request->adress_id;
                    return redirect($url);
                }
                else{
                    return redirect($UCD_URL);
                }
            }
    } 

    public function halk_bankası_place_order(Request $request){ 


        $shop_settigs=ShoppingOrderSettings::find(1);
        $API_keys=json_decode($shop_settigs->extra);
        
        $name = $API_keys->data1;
        $password = $API_keys->data2;
        $clientid = $API_keys->data3;

        $lip = $_SERVER['REMOTE_ADDR'];
  

        $adres_id=$request->adress_id;
        $cc_no=str_replace(" ","",$request->credit_card_no);
        $cc_name=$request->credit_card_name;
        $cc_month=$request->credit_card_month;
        $cc_year=$request->credit_card_year;
        $mmyy=$cc_month."/".$cc_year;

        $cc_cvs=$request->credit_card_cvs;
        $current_adress=ShoppingAdresses::find($request->adress_id);
        $adress_info=json_decode($current_adress->adress);
        $order_no=str_random(10);
        $total=number_format((float)$request->total, 2, '.', '');
        $alltotal=number_format((float)$request->alltotal, 2, '.', '');
        $identifier=$request->identifier;
        $instance='wl';
        $callbackurl=url('sta/save_order/2');
        $threeD=$request->threeDsecure;

        if(!is_null($current_adress->user_id)){
            $user=User::find($current_adress->user_id);
            $firstname=$user->firstname;
            $lastname=$user->lastname;  

        }
        else{
            $firstname=$adress_info->firstname;
            $lastname=$adress_info->lastname;      
        }
     

            $request= "DATA=<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?>
                <CC5Request>
                <Name>".$name."</Name>
                <Password>".$password."</Password>
                <ClientId>".$clientid."</ClientId>
                <IPAddress>".$lip."</IPAddress>
                <Email>".$current_adress->email."</Email>
                <OrderId>".$order_no."</OrderId>
                <GroupId></GroupId>
                <TransId></TransId>
                <UserId></UserId>
                <Type>Auth</Type>


                <Number>".$cc_no."</Number>
                <Expires>".$mmyy."</Expires>
                <Cvv2Val>".$cc_cvs."</Cvv2Val>
                <Total>".$alltotal."</Total>
                <Currency>949</Currency>
                <Taksit></Taksit>
                <BillTo>
                    <Name></Name>
                    <Street1></Street1>
                    <Street2></Street2>
                    <Street3></Street3>
                    <City></City>
                    <StateProv></StateProv>
                    <PostalCode></PostalCode>
                    <Country></Country>
                    <Company></Company>
                    <TelVoice></TelVoice>
                </BillTo>
                <ShipTo>
                    <Name></Name>
                    <Street1></Street1>
                    <Street2></Street2>
                    <Street3></Street3>
                    <City></City>
                    <StateProv></StateProv>
                    <PostalCode></PostalCode>
                    <Country></Country>
                </ShipTo>
                <Extra></Extra>
            </CC5Request>
            "; 
            // Sanal pos adresine baglanti kurulmasi
            // Test icin$url = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer";
            // Üretim ortami için $url = "https://sanalpos.halkbank.com.tr/servlet/cc5ApiServer"

            //$url = "https://sanalpos.halkbank.com.tr/servlet/cc5ApiServer";  //TEST
            //$url = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer";
            $url = "https://entegrasyon.asseco-see.com.tr/fim/api";
            //$url = "https://sanalpos.halkbank.com.tr/fim/api";
            //$url=$API_keys->url;

            //$url=(string)$API_keys->url;

            $ch = curl_init();    // initialize curl handle
            curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
            curl_setopt($ch, CURLOPT_TIMEOUT, 90); // times out after 4s
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request); // add POST fields

            $result = curl_exec($ch); // run the whole process
            if(curl_errno($ch)){
               print curl_error($ch);
            }else {
               curl_close($ch);
               //echo $result;
               //returns <xml>...</xml>
            }       
            $cleanxml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $result);
            $cleanxml = str_ireplace('NS1:','', $cleanxml);
            $xml = simplexml_load_string($cleanxml);

                if($xml->Response=='Approved'){
                    $url='sta/save_order/1/'.$order_no.'/'.$identifier.'/'.$adres_id;
                    return redirect($url);
                }
                else{
                    return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
                }
            
    } 
    public function save_order_get($post_type,$order_no,$identifier,$address_id,$product_type,$order_id,$payment){  
           
        return redirect($this->ordersave($order_no,$identifier,$address_id,$product_type,$order_id,$payment));
    }

    public function save_order_post(Request $request){  
            if($request->segment(3)=='1'){
                $sonuc=$request['TURKPOS_RETVAL_Sonuc'];
                $extra_data=explode('|', $request['TURKPOS_RETVAL_Ext_Data']);
                if($sonuc>0){
                    return redirect($this->ordersave($extra_data[0],$extra_data[1],$extra_data[2]));
                }
                else{
                    return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
                }
            }
            elseif($request->segment(3)=='2'){
                //dd($request->input());
                if($request->status=='success'){
                        $shop_settigs=ShoppingOrderSettings::find(1);
                        $API_keys=json_decode($shop_settigs->extra);

                        $options = new \Iyzipay\Options();
                        $options->setApiKey($API_keys->API_key);
                        $options->setSecretKey($API_keys->Sec_key);
                        $options->setBaseUrl("https://sandbox-api.iyzipay.com");

                        $request1 = new \Iyzipay\Request\CreateThreedsPaymentRequest();
                        $request1->setLocale(\Iyzipay\Model\Locale::TR);
                        $request1->setConversationId($request->conversationId);
                        $request1->setPaymentId($request->paymentId);
                        $request1->setConversationData($request->conversationData);

                        $threedsPayment = \Iyzipay\Model\ThreedsPayment::create($request1, $options);
                        //dd($threedsPayment->getStatus());
                        if($threedsPayment->getstatus()=='success'){
                             $data=explode('-', $request->conversationId);

                             return redirect($this->ordersave($data[0],$data[1],$data[2]));
                        }
                        else{
                            return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
                        }
                       
                }
                else{
                    return redirect('shop/checkout')->withErrors(['HATA! Kart bilgilerinizi kontrol ederek yeniden deneyiniz']);
                }
            }
                     
    }
    public function ordersave($order_no,$identifier,$address_id,$product_type,$order_id,$payment){
          if($order_id > 0){
                $user = Auth::user();
                $order  = ShoppingOrder::find($order_id);
                $shop_order_setting=ShoppingOrderSettings::find(1);
                $instance=$product_type;


                Cart::instance($instance)->getstore($identifier,$instance);
                $order_content = Cart::instance($instance)->content();


               
                $suborder_content = $order_content->first();
                $subscriptionperiod = $suborder_content->options->subscriptionperiod;

                $extend_order = new ShoppingOrder();

                $extend_order->order_no =$order->order_no;
                $extend_order->product_type =$order->product_type;
                $extend_order->user_id =$order->user_id;
                $extend_order->email =$order->email;
                $extend_order->order_content =$order_content;

                $current_adress=ShoppingAdresses::find($address_id);
                $_address_information=collect($current_adress);
                $_address_information->put('firstname',$user->firstname);
                $_address_information->put('lastname',$user->lastname);
                $_address_information->put('shipping_cost',$shop_order_setting->shipping_cost);
                $_address_information->put('shipping_type',$shop_order_setting->shipping_type);
                $_address_information->put('shipping_cost_price',$shop_order_setting->shipping_cost_price);
                $_address_information->put('min_shippingcost',$shop_order_setting->min_shippingcost);
                $extend_order->address_information=collect($_address_information);

                $extend_order->payment_information=$payment;
                $extend_order->status=str_slug('prepare');

                $extendendDateTime = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $order->end_date)->addMonth($subscriptionperiod);
                $extend_order->end_date =$extendendDateTime;
                $extend_order->save();

                return 'user/subscription';
        

          }else{

            $user = Auth::user();

            $shop_order_setting=ShoppingOrderSettings::find(1);
            
            if(!is_null($user)){
                $current_adress=ShoppingAdresses::find($address_id);
                $instance=$product_type;
                $endDateTime = null;
                
                Cart::instance($instance)->getstore($identifier,$instance);
                $order_content = Cart::instance($instance)->content();

                //dd($order_content);


                if($product_type == 'sub'){
                  $suborder_content = $order_content->first();
                  $subscriptionperiod = $suborder_content->options->subscriptionperiod;
                  $endDateTime = \Carbon\Carbon::now()->addMonth($subscriptionperiod);
                }
                
                //dd(collect($current_adress));
                $Confirmed_order = new ShoppingOrder();

                $Confirmed_order->order_no=$order_no;
                $Confirmed_order->user_id=$user->id;
                $Confirmed_order->product_type=$product_type;
                $Confirmed_order->email=$user->email;
                $Confirmed_order->order_content=$order_content;
                $Confirmed_order->end_date=$endDateTime;
                
                $_address_information=collect($current_adress);
                $_address_information->put('firstname',$user->firstname);
                $_address_information->put('lastname',$user->lastname);

                $_address_information->put('shipping_cost',$shop_order_setting->shipping_cost);
                $_address_information->put('shipping_type',$shop_order_setting->shipping_type);
                $_address_information->put('shipping_cost_price',$shop_order_setting->shipping_cost_price);
                $_address_information->put('min_shippingcost',$shop_order_setting->min_shippingcost);

                $Confirmed_order->address_information=collect($_address_information);
                $Confirmed_order->payment_information=$payment;
                $Confirmed_order->status=str_slug('prepare');
                
                $Confirmed_order->save();
                if($product_type == 'sub'){
                  $domain = new Domain();

                  $domain->domain_name = $order_content->first()->options->domain;
                  $domain->order_no = $order_no;
                  $domain->database_name = $order_no.'_'.str_slug($order_content->first()->options->domain);
                  $domain->package_id = $order_content->first()->id;

                  $domain->save();  


                  $modelhasroles = new ModelHasRoles();
                  $modelhasroles->role_id = 2;
                  $modelhasroles->model_type  = 'App\User';
                  $modelhasroles->model_id  = $user->id;
                  $modelhasroles->domain_id  = $domain->id;

                  $modelhasroles->save();
                }




                Cart::instance($instance)->destroy($identifier,$instance);
                Cart::instance($instance)->store($identifier,$instance);

                
                $data=$Confirmed_order;
             
                //Mail::to($Confirmed_order->email)->send(new OrderShipped($data));

                if($product_type == 'sub'){
                    return 'user/subscription';
                }else{
                    return 'user/orders';
                }
            }
            else{
                
                
                
                $useraddreses=ShoppingAdresses::find($address_id);
                
                $instance=$product_type;
                
                
                Cart::instance($instance)->getstore($identifier,$instance);
                $order_content=Cart::instance($instance)->content();
                //dd(collect($current_adress));
                $Confirmed_order = new ShoppingOrder();
                
                $Confirmed_order->order_no=$order_no;
                $Confirmed_order->user_id=null;
                $Confirmed_order->product_type=$product_type;
                $Confirmed_order->email=$useraddreses->email;
                $Confirmed_order->order_content=$order_content;

                $_address_information=collect($useraddreses);

                $_address_information->put('shipping_cost',$shop_order_setting->shipping_cost);
                $_address_information->put('shipping_type',$shop_order_setting->shipping_type);
                $_address_information->put('shipping_cost_price',$shop_order_setting->shipping_cost_price);
                $_address_information->put('min_shippingcost',$shop_order_setting->min_shippingcost);

                $Confirmed_order->address_information=collect($_address_information);



                $Confirmed_order->status=str_slug('prepare');
                $Confirmed_order->payment_information=$payment;
               
                $Confirmed_order->save();


                Cart::instance($instance)->destroy($identifier,$instance);
                Cart::instance($instance)->store($identifier,$instance);

                
                $data=$Confirmed_order;
                //Mail::to($Confirmed_order->email)->send(new OrderShipped($data));

                return 'user/orders/'.$order_no;
            }

          }
        
            
    }


    public function genel(){
       
        dd("asda");
    }
}

