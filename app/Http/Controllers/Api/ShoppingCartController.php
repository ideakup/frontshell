<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\ContentVariable;
use Cart;
use Session;
use App\Shoppingtest;
use App\Classes\classdemo;
use App\ShoppingCheckout;
use App\ShoppingAdresses;
use App\ShoppingOrder;
use Mail;
use App\Mail\OrderShipped;
use App\ShoppingOrderSettings;
use App\CityVariable;
use App\CountyVariable;
use App\County;



class ShoppingCartController extends Controller{

    public function add(Request $request){
        //dd($request->input());
        if(!empty($request->userid)){
           if(!empty($request->identifier)){
                $identifier=$request->identifier;
                if(ShoppingCheckout::where('shop_cookie',$identifier)->where('user_id',$request->userid)->count()==0){
                    $user=User::where("id",$request->userid)->first();

                    $ShoppingCheckout = new ShoppingCheckout();
                    $ShoppingCheckout->user_id=$user->id;
                    $ShoppingCheckout->user_email=$user->email;
                    $ShoppingCheckout->shop_cookie= $identifier;

                    $ShoppingCheckout->save();
                }                  
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
            } 
        }else{
            if(!empty($request->identifier)){
                $identifier=$request->identifier;
              
                if(Shoppingtest::where('identifier',$identifier)->where('instance','wl')->count()==0){
                    $identifier= str_random(20);
                    Cart::destroy();
                    setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
                }else{
                 $identifier=$request->identifier;
                } 
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day

            }
        }


        $instance="wl";
        //$identifier_flag = Shoppingtest::where('identifier', $identifier)->count();
        $itemVariable = ContentVariable::where("slug",$request->content_slug)->first();

        $itemContent = json_decode($itemVariable->content); 
        //dd( $itemContent);
        $final_price=$itemContent->price;
        if(!empty($itemContent->discounted_price)){
            $final_price=$itemContent->discounted_price;
        }
        if(empty($request->quantity)){
            $quantity=1;
        }
        else{
             $quantity=$request->quantity;
        }
        if(empty($itemContent->tax_rate)){
            $tax_rate=0;
        }
        else{
             $tax_rate=$itemContent->tax_rate;
        }
        
        if(Shoppingtest::where('identifier',$identifier)->where('instance','wl')->count()!=0){
            Cart::instance($instance)->getstore($identifier,$instance);
        }
        
        Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['img' => $itemContent->photo,'tax_rate' =>$tax_rate]);
        Cart::instance($instance)->store($identifier,$instance);


        $array=array();

        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        return json_encode($array,200);
    }
    public function subscribe(Request $request){
        
        //dd($request->input());
        if(!empty($request->userid)){
           if(!empty($request->identifier)){
                $identifier=$request->identifier;
                if(ShoppingCheckout::where('shop_cookie',$identifier)->where('user_id',$request->userid)->count()==0){
                    $user=User::where("id",$request->userid)->first();

                    $ShoppingCheckout = new ShoppingCheckout();
                    $ShoppingCheckout->user_id=$user->id;
                    $ShoppingCheckout->user_email=$user->email;
                    $ShoppingCheckout->shop_cookie= $identifier;

                    $ShoppingCheckout->save();
                }                  
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
            } 
        }else{
            if(!empty($request->identifier)){
                $identifier=$request->identifier;
              
                if(Shoppingtest::where('identifier',$identifier)->where('instance','sub')->count()==0){
                    $identifier= str_random(20);
                    Cart::destroy();
                    setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
                }else{
                 $identifier=$request->identifier;
                } 
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day

            }
        }

        $instance="sub";
        //$identifier_flag = Shoppingtest::where('identifier', $identifier)->count();
        $itemVariable = ContentVariable::where("slug",$request->content_slug)->first();

        $itemContent = json_decode($itemVariable->content);

        $periodandprice = collect($itemContent->periodandprice);
        $final_price=$periodandprice->first();
        if(!empty($itemContent->discounted_price)){
            $final_price=$itemContent->discounted_price;
        }
        if(empty($request->quantity)){
            $quantity=1;
        }
        else{
             $quantity=$request->quantity;
        }
        if(empty($itemContent->tax_rate)){
            $tax_rate=0;
        }
        else{
             $tax_rate=$itemContent->tax_rate;
        }

        Cart::instance($instance)->getstore($identifier,$instance);
        Cart::instance($instance)->destroy($identifier,$instance);
        
        Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$periodandprice->search($periodandprice->first()),'domain' =>" "]);
        Cart::instance($instance)->store($identifier,$instance);


        $array=array();

        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        return json_encode($array,200);
    }
    public function extend_upgrade(Request $request){

        
        if(!empty($request->userid)){
           if(!empty($request->identifier)){
                $identifier=$request->identifier;
                if(ShoppingCheckout::where('shop_cookie',$identifier)->where('user_id',$request->userid)->count()==0){
                    $user=User::where("id",$request->userid)->first();

                    $ShoppingCheckout = new ShoppingCheckout();
                    $ShoppingCheckout->user_id=$user->id;
                    $ShoppingCheckout->user_email=$user->email;
                    $ShoppingCheckout->shop_cookie= $identifier;

                    $ShoppingCheckout->save();
                }                  
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
            } 
        }else{
            if(!empty($request->identifier)){
                $identifier=$request->identifier;
              
                if(Shoppingtest::where('identifier',$identifier)->where('instance','sub')->count()==0){
                    $identifier= str_random(20);
                    Cart::destroy();
                    setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day
                }else{
                 $identifier=$request->identifier;
                } 
            }else{
                $identifier= str_random(20);
                Cart::destroy();
                setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day

            }
        }
        $x = explode("-",$request->order_content_id);
        $order_id = $x[0];
        $content_id = $x[1];
        $function_type = $x[2];
        $instance="sub"; 
        if($function_type == 'extend'){
        
            $itemVariable = ContentVariable::where("content_id",$content_id)->where("lang_code",$request->lang)->first();
            $itemContent = json_decode($itemVariable->content);
            $periodandprice = collect($itemContent->periodandprice);
            $final_price=$periodandprice->first();
            if(!empty($itemContent->discounted_price)){
                $final_price=$itemContent->discounted_price;
            }
            if(empty($request->quantity)){
                $quantity=1;
            }
            else{
                 $quantity=$request->quantity;
            }
            if(empty($itemContent->tax_rate)){
                $tax_rate=0;
            }
            else{
                 $tax_rate=$itemContent->tax_rate;
            }
            
            Cart::instance($instance)->getstore($identifier,$instance);
            Cart::instance($instance)->destroy($identifier,$instance);
            Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$periodandprice->search($periodandprice->first()),'domain' =>$request->domain]);
            Cart::instance($instance)->store($identifier,$instance);
        }
        elseif($function_type == 'subscribe'){
            $itemVariable = ContentVariable::where("content_id",$content_id)->where("lang_code",$request->lang)->first();
            $itemContent = json_decode($itemVariable->content);
            $periodandprice = collect($itemContent->periodandprice);
            $final_price=$periodandprice->first();
            if(!empty($itemContent->discounted_price)){
                $final_price=$itemContent->discounted_price;
            }
            if(empty($request->quantity)){
                $quantity=1;
            }
            else{
                 $quantity=$request->quantity;
            }
            if(empty($itemContent->tax_rate)){
                $tax_rate=0;
            }
            else{
                 $tax_rate=$itemContent->tax_rate;
            }
            
            Cart::instance($instance)->getstore($identifier,$instance);
            Cart::instance($instance)->destroy($identifier,$instance);
            
            Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$periodandprice->search($periodandprice->first()),'domain' =>$request->domain]);
            Cart::instance($instance)->store($identifier,$instance);
        }
        else if($function_type == 'upgrade'){

            $user_order=ShoppingOrder::find($order_id);
            $dt = \Carbon\Carbon::parse($user_order->end_date)->endOfDay();
            $now =\Carbon\Carbon::now()->startOfDay();
            $diff_months = $dt->diffInMonths($now);
            $asd = $dt->copy()->subMonths($diff_months);
            $diff_days = $now->diffInDays($asd);
            $free_days =$diff_days%15;


            if($diff_months < 12){
                $updated_period = 12;
            }elseif(12 <= $diff_months &&  $diff_months < 24){
                $updated_period = 24;
            }elseif(24 <= $diff_months &&  $diff_months < 36){
                $updated_period = 36;
            }elseif(36 <= $diff_months &&  $diff_months < 48){
                $updated_period = 48;
            }elseif(48 <= $diff_months){
                $updated_period = 60;
            }
            
            $older_package_id = array_values(json_decode($user_order->order_content, true))[0]['id'];
            $older_package_variable = ContentVariable::where("content_id",$older_package_id)->where("lang_code",$request->lang)->first();
            $older_package_price = (float)collect(json_decode($older_package_variable->content)->periodandprice)[$updated_period];

            // 15 olan conden id için daha iyi bir şey düşünülecek ve para düzenlemesi hangi paketten hangi pakate  geçileceği ile ölçülecek
            $itemVariable = ContentVariable::where("content_id",15)->where("lang_code",$request->lang)->first();
            $itemContent = json_decode($itemVariable->content);
            $upgradeted_price =((float)collect($itemContent->periodandprice)[$updated_period])-$older_package_price;

            $final_price=($upgradeted_price/$updated_period*$diff_months)+((($upgradeted_price/$updated_period)/30) * ($diff_days-$free_days));

            if(empty($request->quantity)){
                $quantity=1;
            }
            else{
                 $quantity=$request->quantity;
            }
            if(empty($itemContent->tax_rate)){
                $tax_rate=0;
            }
            else{
                 $tax_rate=$itemContent->tax_rate;
            }
            
            Cart::instance($instance)->getstore($identifier,$instance);
            Cart::instance($instance)->destroy($identifier,$instance);
            
            Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>0,'domain' =>$request->domain]);
            Cart::instance($instance)->store($identifier,$instance);
        }
        $array=array();

        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();
        $array['order_id'] = $order_id;
        $array['function_type'] = $function_type;

        return json_encode($array,200);
    }
    public function remove(Request $request){
       
        //dd( $request->rowId);
        if(!empty($request->identifier)){
            $identifier=$request->identifier;
        }else{
            echo "hata var olması gerek!!";
        }
        
        $instance="wl";
        //$identifier_flag = Shoppingtest::where('identifier', $identifier)->count();

        Cart::instance($instance)->getstore($identifier,$instance);
        Cart::instance($instance)->remove($request->rowId);
        Cart::instance($instance)->store($identifier,$instance);
        
        $array=array();
        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        return json_encode($array,200);
    }
    public function minus(Request $request){
       
        //dd( $request->rowId);
        if(!empty($request->identifier)){
            $identifier=$request->identifier;
        }else{
            echo "hata var olması gerek!!";
        }
        
        $instance="wl";
        //$identifier_flag = Shoppingtest::where('identifier', $identifier)->count();

        Cart::instance($instance)->getstore($identifier,$instance);
        $qty=Cart::instance($instance)->get($request->rowId)->qty;
        if($qty>0){
            $qty=$qty-1;
        }

        Cart::instance($instance)->update($request->rowId, ['qty' => $qty]);
        Cart::instance($instance)->store($identifier,$instance);

        $array=array();
        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        return json_encode($array,200);
    }
    public function plus(Request $request){
       
        //dd( $request->rowId);
        if(!empty($request->identifier)){
            $identifier=$request->identifier;
        }
        else{
            echo "hata var olması gerek!!";
        }
        
        $instance="wl";
        //$identifier_flag = Shoppingtest::where('identifier', $identifier)->count();

        Cart::instance($instance)->getstore($identifier,$instance);
                        //dd(Cart::instance($instance)->get($request->rowId)->options['tax_rate']);

        $qty=Cart::instance($instance)->get($request->rowId)->qty;
        $qty=$qty+1;

        Cart::instance($instance)->update($request->rowId, ['qty' => $qty]);
        Cart::instance($instance)->store($identifier,$instance); 

        $array=array();
        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        return json_encode($array,200);
    }
    public function city(Request $request){

        $city = CityVariable::where('name',$request->cityname)->first();
        $county_ids = County::where('city_id',$city->id)->pluck('id');
        $countys = CountyVariable::whereIn('county_id',$county_ids)->get();
      

        return $countys;
    }
    public function subchange(Request $request){

        if(!empty($request->identifier)){
            $identifier=$request->identifier;
        }else{
            echo "hata var olması gerek!!";
        }
        $subcahangetype = $request->subcahangetype;
        $instance="sub";
        if( $subcahangetype == 'period'){
            $itemVariable = ContentVariable::where('content_id',$request->contvarid)->first();
            $itemContent = json_decode($itemVariable->content);
            $periodandprice = collect($itemContent->periodandprice);
            
            $final_price=$periodandprice[$request->period];
            if(!empty($itemContent->discounted_price)){
                $final_price=$itemContent->discounted_price;
            }
            if(empty($request->quantity)){
                $quantity=1;
            }
            else{
                 $quantity=$request->quantity;
            }
            if(empty($itemContent->tax_rate)){
                $tax_rate=0;
            }
            else{
                 $tax_rate=$itemContent->tax_rate;
            }
            if(!is_null($request->domain)){
                $domain = $request->domain;
            }
            else{
                $domain = str_random(5).".webshell.online";
            }

            Cart::instance($instance)->getstore($identifier,$instance);
            Cart::instance($instance)->destroy($identifier,$instance);
            
            Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$request->period,'domain' =>$domain]);
            Cart::instance($instance)->store($identifier,$instance);
            $array['function_type'] = $subcahangetype;

        }elseif($subcahangetype == 'packagechange'){
            $itemVariable = ContentVariable::where('content_id',$request->package_id)->first();
            $itemContent = json_decode($itemVariable->content);
            $periodandprice = collect($itemContent->periodandprice);
            $subscriptionperiod = array_key_first($periodandprice->toArray());
            $final_price=$periodandprice->first();
            if(!empty($itemContent->discounted_price)){
                $final_price=$itemContent->discounted_price;
            }
            if(empty($request->quantity)){
                $quantity=1;
            }
            else{
                 $quantity=$request->quantity;
            }
            if(empty($itemContent->tax_rate)){
                $tax_rate=0;
            }
            else{
                 $tax_rate=$itemContent->tax_rate;
            }
            if(!is_null($request->domain)){
                $domain = $request->domain;
            }
            else{
                $domain = str_random(5).".webshell.online";
            }
            Cart::instance($instance)->getstore($identifier,$instance);
            Cart::instance($instance)->destroy($identifier,$instance);
            
            Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$subscriptionperiod,'domain' =>$domain]);
            Cart::instance($instance)->store($identifier,$instance);
        }
        

        $array=array();
        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();
        $array['function_type'] = $subcahangetype;

        //dd($array);
        return json_encode($array,200);
    }
    public function installment(Request $request){

        //dd($request->input());
        $cc_no=substr(str_replace(" ","",$request->card_no),0,6);
        $total=number_format((float)$request->total_price, 2, '.', '');
        $con_id=(string)$request->identifier;

        $request = new \Iyzipay\Request\RetrieveInstallmentInfoRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId($con_id);
        $request->setBinNumber($cc_no);
        $request->setPrice($total);

        $shop_settigs=ShoppingOrderSettings::find(1);
        $API_keys=json_decode($shop_settigs->extra);

        $options = new \Iyzipay\Options();
        $options->setApiKey($API_keys->data1);
        $options->setSecretKey($API_keys->data2);
        
        $options->setBaseUrl($API_keys->url."");

        $installmentInfo = \Iyzipay\Model\InstallmentInfo::retrieve($request, $options);
        if($installmentInfo->getStatus() == 'success'){
            $x = json_decode($installmentInfo->getRawResult())->installmentDetails[0];

            $array = array();

            $array['cardType'] = $x->cardType;
            $array['bankName'] = $x->bankName;
            $array['cardAssociation'] = $x->cardAssociation;
            $array['cardFamilyName'] = $x->cardFamilyName;
            $array['installmentPrices'] = $x->installmentPrices;

            return json_encode($array,200);
        }   
    }
    public function domain(Request $request){

        if(!empty($request->identifier)){
            $identifier=$request->identifier;
        }else{
            echo "hata var olması gerek!!";
        }
        
        $instance="sub";
        $itemVariable = ContentVariable::find($request->contvarid);

        $itemContent = json_decode($itemVariable->content);

        

        $periodandprice = collect($itemContent->periodandprice);
        
        $final_price=$periodandprice[$request->period];
        if(!empty($itemContent->discounted_price)){
            $final_price=$itemContent->discounted_price;
        }

        if(empty($request->quantity)){
            $quantity=1;
        }
        else{
            $quantity=$request->quantity;
        }
        if(empty($itemContent->tax_rate)){
            $tax_rate=0;
        }
        else{
            $tax_rate=$itemContent->tax_rate;
        }

        if(!is_null($request->domain)){
            $domain = $request->domain;
        }
        else{
            $domain = str_random(5).".webshell.online";
        }


        Cart::instance($instance)->getstore($identifier,$instance);
        Cart::instance($instance)->destroy($identifier,$instance);
        
        Cart::instance($instance)->add($itemVariable->content_id, $itemVariable->title, $quantity, $final_price,['tax_rate' =>$tax_rate,'subscriptionperiod' =>$request->period,'domain' =>$domain]);
        Cart::instance($instance)->store($identifier,$instance);

        $array=array();
        $array[0]=Cart::instance($instance)->content();
        $array[1][0]=Cart::instance($instance)->subtotal();
        $array[1][1]=Cart::instance($instance)->tax();
        $array[1][2]=Cart::instance($instance)->total();
        $array[1][3]=Cart::instance($instance)->count();

        

        return json_encode($array,200);
    }

}



