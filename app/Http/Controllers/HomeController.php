<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Validator;
use Request;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\SiteSettings;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\Language;
use App\CategoryTopHasSub;

use App\FormData;

use App\TopHasSub;
use App\ContentHasTag;
use App\ContentHasCategory;
use App\ShoppingOrderSettings;
use Illuminate\Support\Arr;
use FeedReader;

use Mail;
use App\Mail\FormSendMail;
use App\ShoppingAdresses;
use App\ShoppingCheckout;
use App\Shoppingtest;
use App\ShoppingOrder;
use App\County;
use App\City;
use App\NewTopHasSub;

class HomeController extends Controller
{

    public function __construct()
    {
       // $this->middleware('auth');
    }
    public function  display(){
        return view('types.menu.displayCart');
    }

    public function index($lang, $slug, $attr = null, $param = null, $param2 = null)
    {
        return view('home',$this->indexdata($lang, $slug, $attr , $param, $param2));
    }
         
    public function indexOneLang($slug, $attr = null, $param = null, $param2 = null)
    {
        if(!empty($_COOKIE["lang_cookie"])){
            $lang = $_COOKIE["lang_cookie"];
        }else{
            $lang =App::getLocale();
        }
        return view('home',$this->indexdata($lang, $slug, $attr , $param, $param2));
    }
     

    public function indexdata($lang,$slug, $attr = null, $param = null, $param2 = null)
    {
 
        
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $langSlug = '';



        $menu = MenuVariable::where('slug', $slug)->first()->menu;
        $section_ths=$menu->topHasSubContent;
        $content_ids=-1;
        $sub_content = array();
        foreach($section_ths as $content_ths){
            $sub_content[] = $content_ths->subContent;
        }
        if(!empty($sub_content)){
            $content_ids=$sub_content[0]->pluck('sub_id');
        }
        

        $content_ths="";

       /* $deneme=ContentVariable::whereIn('content_id',$content_ids)->pluck('content');
        dd($deneme);*/

        if((!empty(Request::get('tag')))){

           $tagslug=explode(",",Request::get('tag'));

           $tag_ids=TagVariable::select('tag_id')->whereIn('slug',$tagslug)->get();
           $content_ids=ContentHasTag::whereIn('tag_id',$tag_ids)->whereIn('content_id',$content_ids)->pluck('content_id');
           $content_ths=null;
           $pag_number=count($content_ids);
           
          /*$tagcontent1=App\NewTopHasSub::where('top_id',$menu->id)->where('top_model','Menu')->where('sub_model','Content')->whereIn('sub_id',$content_ids)->get();*/ 
        }
        if((!empty(Request::get('cat')))){

           $catslug=explode(",",Request::get('cat'));
           $cat_ids=CategoryVariable::select('category_id')->whereIn('slug',$catslug)->get();
           $chts=CategoryTopHasSub::whereIn('category_id',$cat_ids)->get();
           $arr=array();
           //dd($content_ids);
           $content_ids=ContentHasCategory::select('content_id')
           ->whereIn('content_id',$content_ids)
           ->whereIn('category_id',$this->subcat2($chts, $arr))->pluck('content_id');
           $pag_number=count($content_ids);
        }
        if((!empty(Request::get('search')))){
            $key=Request::get('search');
            
            $_ths=$_ths->whereIn('sub_id',$content_ids);
                       
            $combine=collect();
            foreach($_ths as $content_ths){
                $content_varaible=$content_ths->thatElementForContent->variable;
                $short_content=json_decode( $content_varaible->content)->short_content;
                 
                $combine->push([$content_ths->sub_id,$content_varaible->title." ".$short_content]);
                
            }   
            
            $filtered=$combine->filter(function ($item) use ($key) {
                
                // replace stristr with your choice of matching function
                return false !== stripos($item[1], $key);
            });
            $content_ids=$filtered->pluck(0);
           // dd($filtered);
            $pag_number=count($content_ids);
        }
        if((!empty(Request::get('pag')))){

            if(is_null(Request::get('p_dist'))){
                $p_dist=15;
            }
            else{
                $p_dist=Request::get('p_dist');
            }
            $pag=Request::get('pag');
            $content_ids=NewTopHasSub::select("*","final_price as finalPrice")->join('contentvariable','contentvariable.content_id','=','new_top_has_sub.sub_id')
            ->where('stock','var')
            ->where('deleted','no')->where('status','active')
            ->whereIn('new_top_has_sub.sub_id',$content_ids)->skip( ($pag-1)*$p_dist )->take($p_dist)->pluck('content_id');
        }
        if((!empty(Request::get('sort')))){
            $sorttype=Request::get('sort');
  
            //dd( $sorttype);
            //$content_ids=ContentVariable::whereIn('content_id',$content_ids)->orderBy('content->price')->pluck('content_id');;
            $content_ths=NewTopHasSub::select("*","final_price as finalPrice")->join('contentvariable','contentvariable.content_id','=','new_top_has_sub.sub_id')
            ->where('stock','var')
            ->where('deleted','no')->where('status','active')
            ->whereIn('new_top_has_sub.sub_id',$content_ids)->orderByRaw( 'CAST(finalPrice AS DECIMAL(10,2))'.$sorttype)->get();
             //dd($content_ths);
        }

        //-------------------------------SEO-------------------------------------------
        $seo_title =null;
        $seo_description =null;
        $seo_keywords =null;

        if(is_null($attr)){
            $menuvariable = MenuVariable::where('slug', $slug)->first();
            $seo = json_decode($menuvariable->seo);

            $seo_title = $seo->seo_title;
            $seo_description = $seo->seo_description;
            $seo_keywords = $seo->seo_keywords;

        }else{
            $contentvariable = ContentVariable::where('slug', $attr)->first();
            $menuvariable = MenuVariable::where('slug', $slug)->first();
            $seo = json_decode($menuvariable->seo);
            if(!empty(json_decode($contentvariable->content)->seo_title)){
               $seo_title = json_decode($contentvariable->content)->seo_title; 
            }
            if(!empty(json_decode($contentvariable->content)->seo_description)){
               $seo_description = json_decode($contentvariable->content)->seo_description; 
            }
            if(!empty(json_decode($contentvariable->content)->seo_description)){
                $seo_keywords = json_decode($contentvariable->content)->seo_description;
            }
            
            if(is_null($seo_title)){        
                $seo_title = $seo->seo_title;                 
            }
            if(is_null($seo_description)){
                $seo_description = $seo->seo_description; 
            }
            if(is_null($seo_keywords)){
                $seo_keywords = $seo->seo_keywords; 
            }
        }


        $menu = MenuVariable::where('slug', $slug)->first()->menu;
         
        $headerslug = $menu;
        while ($headerslug->top_id != null) {
            $headerslug = $headerslug->topMenu;
        }
        $headerslug = $headerslug->variables->where('lang_code', $lang)->first()->slug;
        $allcontent=ContentVariable::all();
        $shop_settigs=ShoppingOrderSettings::find(1);
        $shoppingcheckout=ShoppingCheckout::all();
        $alladdresses=ShoppingAdresses::all();
        $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $menuformodule=Menu::all(); 
        $menuall = NewTopHasSub::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $mainmenu = NewTopHasSub::where(
        function ($query){
            $query->where('props','like', '%"position":"all"%')->orWhere('props','like', '%"position":"top"%');
        }
        )->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first()->thatElementForMenu;

        $topmenus = NewTopHasSub::join('menu','menu.id','=','new_top_has_sub.sub_id')->where(
        function ($query){
            $query->where('props','not like', '%"position":"aside"%')->Where('props','not like', '%"position":"none"%');
        })->where('top_id', null)->where('sub_model','Menu')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $asidemenus = NewTopHasSub::join('menu','menu.id','=','new_top_has_sub.sub_id')->where(
        function ($query){
            $query->where('props','not like', '%"position":"top"%')->Where('props','not like', '%"position":"none"%');
        })->where('top_id', null)->where('sub_model','Menu')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $sitesettings = collect(config('ws.site_settings_data'));
        $startDate = Carbon::now()->copy()->startOfDay()->subMonths(3);

        $wsConfig = config('webshell.pageProperties.default');
        $webshellIndex = '';
        if (is_null($attr)) { $webshellIndex = $slug; }else{ $webshellIndex = $attr; }
        if (!is_null(config('webshell.pageProperties.'.$webshellIndex))) {
            foreach (config('webshell.pageProperties.'.$webshellIndex) as $key => $value) {
                $wsConfig[$key] = $value;
            }
        }
        
        return array(
            'lang' => $lang,
            'langSlug' => $langSlug,
            'slug' => $slug, 
            'attr' => $attr, 
            'param' => $param,
            'param2' => $param2,
            
            'langs' => $langs, 
            'menu' => $menu, 
            'menuall' => $menuall,
            'menuformodule'=> $menuformodule,
            'shoppingcheckout' =>$shoppingcheckout,
            'shop_settigs' => $shop_settigs,
            'allcontent' => $allcontent,
            'alladdresses'=>$alladdresses,
            'cities'=>$cities,
            'counties'=>$counties,
            'mainmenu' => $mainmenu, 
            'topmenus' => $topmenus, 
            'asidemenus' => $asidemenus, 
            'sitesettings' => $sitesettings, 
            'content_ids'=>$content_ids,
            'content_ths'=>$content_ths,

            'seo_title' => $seo_title, 
            'seo_description'=>$seo_description,
            'seo_keywords'=>$seo_keywords,

            'headerslug' => $headerslug,
            'wsConfig' => $wsConfig


        );
    }

    public function subcat2($_chts,$_array){
        
        foreach ($_chts as $_cht) {
            if($_cht->subCat->count() == 0){
                array_push($_array,$_cht->id);
            }else{
                $_array = array_merge($_array, $this->subcat2($_cht->subCat, $_array));
            }
        } 
        return array_unique($_array);
    }

}


