<?php
namespace App\Classes;

use App\Config;
use App\ConfigKey;
use App\ConfigValue;
use Illuminate\Support\Facades\Storage;

class ConfigHelper
{
    public function __construct()
    {
        
    }

    // Gönderilen key_name'e ait ilk config'i çeker.
    public function config_by_key($key_name)
    {
        $key = ConfigKey::where('key_name', $key_name)->first();
        if (!is_null($key)) {
            $config = Config::where('key_id', $key->id)->first();
            if (!is_null($config)) {
                return $config;
            }else {
                return null;
            }
        }else {
            return null;
        }    
    }

    // Eğer bir key_name gönderilirse o key_name'i db'den çekip array döndürür gönderilmezse top config'i array olarak döndürür.
    public function get_db($key_name = null)
    {
        if (is_null($key_name)) {
            $configs = Config::where('top_id', null)->orderBy('order')->get();
        }else{
            $topConfig = $this->config_by_key($key_name);
            if (is_null($topConfig)) {
                return null;
            }else{
                $configs = $topConfig->subKey;
            }
        };
        return $this->to_array($configs);
    }

    public function to_array($configs)
    {
        $arr = array();
        foreach ($configs as $config) {
            if ($config->status == 'active') {
                if ($config->subKey->count() == 0) {
                    $arr[$config->key->key_name] = (is_null($config->value) ? null : ($config->value->value_name == 'null' ? null : $config->value->value_name));
                }else{
                    $arr[$config->key->key_name] = $this->to_array($config->subKey);
                }
            }
        }
        return $arr;
    }

    // config/ws içindeki tüm config dosyalarını siler.
    public function delete_all_config_file()
    {
        $configFiles = Storage::disk('config')->files();
        Storage::disk('config')->delete($configFiles);
    }

    // Her kategoriyi içeriği ile birlikte dosya olarak kayıt eder.
    public function save_all_config_as_file()
    {
        $allConfig = $this->get_db();

        foreach ($allConfig as $configCategory => $value) {
            $this->save_config_file($configCategory);
        }
    }

    // Gönderilen kategoriyi içeriği ile birlikte dosya olarak kayıt eder.
    public function save_config_file($configCategory)
    {
        $str = preg_replace('#,(\s+|)\)#', '$1)', var_export($this->get_db($configCategory), true));
        $str = '<?php' . PHP_EOL . 'return ' . $str . ';';

        Storage::disk('config')->put($configCategory.'.php', $str);
    }
}

