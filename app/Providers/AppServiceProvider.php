<?php

namespace App\Providers;
use App\ShoppingCheckout;
use App\Shoppingtest;
use App\User;
use Cart;
use Auth;

use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      if (env('APP_ENV') !== 'local') {
          \URL::forceScheme('https');
      }
      view()->composer('*', function($view) {
          
          $view->with('cartsubcontent', $this->subrules('sub'));
          $view->with('cartcontent', $this->rules('wl'));
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }


    public function rules($instance){
        if(!empty(auth()->user())){
              $user = auth()->user();
              if(!empty($_COOKIE["shop_cookie"]) ){
                  $identifier = $_COOKIE["shop_cookie"];
                    if(ShoppingCheckout::where('shop_cookie',$identifier)->where('user_id',$user->id)->count()>0){
                      if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){

                        Cart::instance($instance)->getstore($identifier,$instance);
                        Cart::instance($instance)->destroy();
                        Cart::instance($instance)->getstore($identifier,$instance);
                      }
                    }  
                    else{
                      if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                           if(!empty(ShoppingCheckout::where('user_id',$user->id)->latest()->first())){
                                $user_last_shop_identifier=ShoppingCheckout::where('user_id',$user->id)->latest()->first()->shop_cookie;

                                Cart::instance($instance)->getstore($identifier,$instance);
                                Cart::instance($instance)->destroy();
                                Cart::instance($instance)->getstore($identifier,$instance);
                               //dd(Cart::instance($instance)->content());

                                 $tempcart = array();

                                foreach (Cart::instance($instance)->content() as $row) {
                                  //dd($row);

                                  array_push($tempcart, $row->id);
                                  array_push($tempcart, $row->name);
                                  array_push($tempcart, $row->qty);
                                  array_push($tempcart, $row->price);
                                  array_push($tempcart, $row->options->img);
                                  array_push($tempcart, $row->options->tax_rate);

                                }
                                Cart::instance($instance)->delete($identifier,$instance);

                                Cart::instance($instance)->getstore($user_last_shop_identifier,$instance);
                                Cart::instance($instance)->destroy();
                                Cart::instance($instance)->getstore($user_last_shop_identifier,$instance);
                               // dd(Cart::instance($instance)->content());
                                for ($i=0; $i <count($tempcart) ; $i=$i+6) { 
                                    Cart::instance($instance)->add($tempcart[$i], $tempcart[$i+1], $tempcart[$i+2], $tempcart[$i+3],['img' => $tempcart[$i+4],'tax_rate' => $tempcart[$i+5]]);
                                }
                                unset($tempcart);
                                $tempcart = array();

                                Cart::instance($instance)->store($user_last_shop_identifier,$instance);    
                                setcookie("shop_cookie", $user_last_shop_identifier, time() + (86400 * 30), "/"); // 86400 = 1 day  
                                //dd(Cart::instance($instance)->content());

                              }
                              else{

                              }

                      }
                    }                      
                }
                else{
                      if(!empty(ShoppingCheckout::where('user_id',$user->id)->latest()->first())){
                        $identifier=ShoppingCheckout::where('user_id',$user->id)->latest()->first()->shop_cookie;
                        if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                        
                          Cart::instance($instance)->getstore($identifier,$instance);
                          Cart::instance($instance)->destroy();
                          Cart::instance($instance)->getstore($identifier,$instance);
                          setcookie("shop_cookie", $identifier, time() + (86400 * 30), "/"); // 86400 = 1 day  
                        }

                      }
                      else{
                           Cart::instance($instance)->destroy();

                      }
                }
        } 
        else{
                if(!empty($_COOKIE["shop_cookie"]) ){
                    $identifier = $_COOKIE["shop_cookie"];
                    if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                      if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                     
                          Cart::instance($instance)->getstore($identifier,$instance);
                          Cart::instance($instance)->destroy();
                          Cart::instance($instance)->getstore($identifier,$instance);
                      }
                        
                    }    
                    else{
                        Cart::instance($instance)->destroy();

                    }
                }
                else{
                    Cart::instance($instance)->destroy();

                }       
        }     
        return Cart::instance($instance)->content();
    }
    public function subrules($instance){
        if(!empty(auth()->user())){
              $user = auth()->user();
              if(!empty($_COOKIE["shop_cookie"]) ){
                  $identifier = $_COOKIE["shop_cookie"];
                    if(ShoppingCheckout::where('shop_cookie',$identifier)->where('user_id',$user->id)->count()>0){
                      if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                        Cart::instance($instance)->getstore($identifier,$instance);
                        Cart::instance($instance)->destroy();
                        Cart::instance($instance)->getstore($identifier,$instance);
                      }
                    }  
                    else{
                      if(Shoppingtest::where('identifier',$identifier)->where('instance',$instance)->count()>0){
                           if(!empty(ShoppingCheckout::where('user_id',$user->id)->latest()->first())){
                                $user_last_shop_identifier=ShoppingCheckout::where('user_id',$user->id)->latest()->first()->shop_cookie;

                                Cart::instance($instance)->getstore($identifier,$instance);
                                Cart::instance($instance)->destroy();
                                Cart::instance($instance)->getstore($identifier,$instance);
                               //dd(Cart::instance($instance)->content());

                                 $tempcart = array();

                                foreach (Cart::instance($instance)->content() as $row) {
                                  //dd($row);

                                  array_push($tempcart, $row->id);
                                  array_push($tempcart, $row->name);
                                  array_push($tempcart, $row->qty);
                                  array_push($tempcart, $row->price);
                                  array_push($tempcart, $row->options->img);
                                  array_push($tempcart, $row->options->tax_rate);

                                }
                                Cart::instance($instance)->delete($identifier,$instance);

                                Cart::instance($instance)->getstore($user_last_shop_identifier,$instance);
                                Cart::instance($instance)->destroy();
                                Cart::instance($instance)->getstore($user_last_shop_identifier,$instance);
                               // dd(Cart::instance($instance)->content());
                                for ($i=0; $i <count($tempcart) ; $i=$i+6) { 
                                    Cart::instance($instance)->add($tempcart[$i], $tempcart[$i+1], $tempcart[$i+2], $tempcart[$i+3],['img' => $tempcart[$i+4],'tax_rate' => $tempcart[$i+5]]);
                                }
                                unset($tempcart);
                                $tempcart = array();

                                Cart::instance($instance)->store($user_last_shop_identifier,$instance);    
                                setcookie("shop_cookie", $user_last_shop_identifier, time() + (86400 * 30), "/"); // 86400 = 1 day  
                                //dd(Cart::instance($instance)->content());

                              }
                              else{

                              }

                      }
                    }                      
                }
        }    
        return Cart::instance($instance)->content();
    }

}
