<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryVariable extends Model
{
    protected $table = 'map_countryvariable';

    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }
}
