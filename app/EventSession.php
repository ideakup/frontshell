<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSession extends Model
{
   protected $table = 'event_session';


    public function topHasSub()
    {
       return $this->hasOne('App\NewTopHasSub', 'sub_id', 'id')->where('sub_model','EventSession')->where('deleted', 'no')->where('status', 'active');
    }
}
