<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormSendMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    protected $contentVar;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $contentVar)
    {
        $this->data = $data;
        $this->contentVar = $contentVar;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $contentVar = $this->contentVar;

        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))->subject('Form Send Mail')->view('mails.formsendmail', array('data' => $data, 'contentVar' => $contentVar));
    }
}
