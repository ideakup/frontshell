<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityVariable extends Model
{
    protected $table = 'map_cityvariable';

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }
}
