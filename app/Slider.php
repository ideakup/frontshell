<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Slider extends Model
{
    protected $table = 'slider';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\SliderVariable', 'slider_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\SliderVariable', 'slider_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\SliderVariable', 'slider_id', 'id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'id', 'menu_id');
    }
    
}
