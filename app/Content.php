<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Content extends Model
{
    protected $table = 'content';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\ContentVariable', 'content_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\ContentVariable', 'content_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\ContentVariable', 'content_id', 'id');
    }

    /** ----- **/



    public function subContentThs()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('top_model','Content')->whereIn('sub_model', ['Content', 'MenuSum'])->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
    }
    public function subEventSessionThs()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('top_model','Content')->where('sub_model', 'EventSession')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
    }
     public function subContentThsforproduct()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('sub_model', 'Content')->where('deleted', 'no')->where('status', 'passive')->orderBy('order', 'asc');
    }
    public function topHasSub()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'id')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
    }
    public function contentThs()
    {
        return $this->hasMany('App\NewTopHasSub', 'sub_id', 'id')->where('sub_model', 'content')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
    }

    /** ----- **/

    public function photogallery($langcode)
    {
        return $this->hasMany('App\ContentPhotoGalleryVariable', 'content_id', 'id')->where('deleted', 'no')->where('lang', $langcode)->orderBy('order', 'asc')->get();
    }

    public function slide()
    {
        return $this->hasMany('App\ContentSlideVariable', 'content_id', 'id')->where('deleted', 'no');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'content_has_tag');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'content_has_category');
    }
  
}
