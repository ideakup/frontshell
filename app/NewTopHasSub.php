<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewTopHasSub extends Model
{

    protected $table = 'new_top_has_sub';

      public function thatElementForMenu()
    {	
        return $this->hasOne('App\Menu', 'id', 'sub_id');
    }
    public function topElementForMenu()
    {	
        return $this->hasOne('App\Menu', 'id', 'top_id');
    }
    public function thatElementForContent()
    {	
        return $this->hasOne('App\Content', 'id', 'sub_id');
    }
    public function topElementForContent()
    {	
        return $this->hasOne('App\Content', 'id', 'top_id');
    }
    public function thatElementForMenusum()
    {   
        return $this->hasOne('App\Menu', 'id', 'sub_id');
    }
    public function eventSession()
    {   
        return $this->hasOne('App\EventSession', 'id', 'sub_id');
    }

    public function subMenu()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('sub_model', 'Menu')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function subContent()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('sub_model', 'Content')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function subHeaderContent()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('sub_model', 'H-Content')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function subFooterZones()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('sub_model', 'Zone')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    public function subFooterContent()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('sub_model', 'F-Content')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }


      public function subMenuTop()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('props','not like', '%"position":aside"%')->where('props','not like', '%"position":none"%')->where('status', 'active')->where('deleted', 'no')->where('sub_model', 'Menu')->orderBy('order', 'asc');
    }

    public function subMenuAside()
    {
        return $this->hasMany('App\NewTopHasSub', 'top_id', 'sub_id')->where('props','not like', '%"position":top"%')->where('props','not like', '%"position":none"%')->where('status', 'active')->where('deleted', 'no')->where('sub_model', 'Menu')->orderBy('order', 'asc');
    }
    public function ContentHasTag()
    {
        return $this->hasMany('App\ContentHasTag', 'content_id', 'sub_id');
    }
    public function topThs()
    {   
        return $this->hasOne('App\NewTopHasSub', 'sub_id', 'top_id');
    }
    
    
}
