<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $table = 'map_county';

    public function variable()
    {
        //$currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\CountyVariable', 'county_id', 'id');
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\CountyVariable', 'county_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\CountyVariable', 'county_id', 'id');
    }

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }
}
