let mix = require('laravel-mix');

/* Default Assets */
mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/favicons', 'public/favicons');
mix.copy('resources/assets/logos', 'public/logos');
mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');
mix.copy('resources/assets/custom_modules/theme_6.5.3_custom/css/fonts', 'public/css/fonts');
mix.copy('resources/assets/custom/variables.css', 'public/css');

/* Custom Assets */
//mix.copy('resources/assets/custom/favicons', 'public/favicons');
//mix.copy('resources/assets/custom/logos', 'public/logos');
//mix.copy('resources/assets/custom/cimages', 'public/cimages');

mix.combine([
	/* Default Scripts */
	'resources/assets/custom_modules/theme_6.5.3_custom/js/jquery.js',
	//'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.easing.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.bootstrap.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.form.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.ajaxform.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.animations.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.carousel.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.flexslider.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.hoveranimation.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.imagesloaded.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.isotope.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.lightbox.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.masonrythumbs.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.parallax.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.quantity.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.readmore.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/plugins.tabs.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/intersection-observer.js',
	'resources/assets/custom_modules/theme_6.5.3_custom/js/functions.js',
	//'resources/assets/custom_modules/theme_custom/js/functions.js',
	//'resources/assets/custom_modules/theme_6.5.3/js/jquery.gmap.js', 

	/* NPM Scripts */
    'node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
    'node_modules/@fortawesome/fontawesome-free/js/all.js',
	'node_modules/formBuilder/dist/form-render.min.js',
	'node_modules/select2/dist/js/select2.min.js',
	'node_modules/video.js/dist/video.min.js',
	/*revelation slider*/
	'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/jquery.themepunch.tools.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/jquery.themepunch.revolution.min.js',

    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.video.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.actions.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.migration.min.js',
    'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js',

	/* Custom Scripts */
	//'resources/assets/custom/script.js',

], 'public/js/app.js').version();
/*
mix.combine([
	// Default CSS 
	'resources/assets/custom_modules/theme_6.5.3/css/bootstrap.css',
	'resources/assets/custom_modules/theme_6.5.3/style.css',
	'resources/assets/custom_modules/theme_6.5.3/css/swiper.css',
	'resources/assets/custom_modules/theme_6.5.3/css/dark.css',
	'resources/assets/custom_modules/theme_6.5.3/css/font-icons.css',
	'resources/assets/custom_modules/theme_6.5.3/css/animate.css',
	'resources/assets/custom_modules/theme_6.5.3/css/magnific-popup.css',

	// NPM CSS 
	'node_modules/@fortawesome/fontawesome-free/css/all.css',
	'node_modules/select2/dist/css/select2.min.css',
	'node_modules/video.js/dist/video-js.css',
	
	// Custom CSS 
	'resources/assets/style.css',
	'resources/assets/custom/style.css',

], 'public/css/app.css').version();
*/
/* Default Canvas CSS */
mix.sass('resources/assets/custom_modules/theme_6.5.3_custom/style.scss', 'public/css/app.css').version();

mix.combine([
	/* Default CSS */
	'resources/assets/custom_modules/theme_6.5.3_custom/css/swiper.css',
	'resources/assets/custom_modules/theme_6.5.3_custom/css/font-icons.css',
	'resources/assets/custom_modules/theme_6.5.3_custom/css/animate.css',
	'resources/assets/custom_modules/theme_6.5.3_custom/css/magnific-popup.css',
	'resources/assets/custom_modules/icofont/icofont.min.css',

	/* NPM CSS */
	'node_modules/@fortawesome/fontawesome-free/css/all.css',
	'node_modules/select2/dist/css/select2.min.css',
	'node_modules/video.js/dist/video-js.css',

	//revolation slider css
	'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/css/settings.css',
	'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/css/layers.css',
	'resources/assets/custom_modules/theme_6.5.3_custom/include/rs-plugin/css/navigation.css',

	// Custom CSS 
	//'resources/assets/custom/style.css',
], 'public/css/package.css').version();