<?php

//dd("static");
/*
|--------------------------------------------------------------------------
| Static Routes
|--------------------------------------------------------------------------
|
*/
/*
//Route::get('sta/listening','StaticController@index');
Route::post('sta/infowithoutlogin','StaticController@infowithoutlogin');

Route::post('addresrecord', 'StaticController@addresrecord');
Route::post('sta/manualregister', 'StaticController@register');
Route::post('addresupdate', 'StaticController@addresupdate');
Route::post('manuallogout', 'StaticController@logout');
Route::post('changePassword', 'StaticController@changePassword');
Route::post('changeUserInfo', 'StaticController@changeUserInfo');
Route::post('forgetpassword', 'StaticController@forgetpassword');
Route::post('changeforgotPassword', 'StaticController@changeforgotPassword');


//Route::post('paymentcheck', 'StaticController@paymentCheck');

Route::post('parampos_place_order', 'StaticController@parampos_place_order');
Route::post('iyzico_place_order', 'StaticController@iyzico_place_order');
Route::post('halk_bankası_place_order', 'StaticController@halk_bankası_place_order');

Route::get('sta/save_order/{post_type}/{order_no}/{identifier}/{address_id}', 'StaticController@save_order_get');
Route::post('sta/save_order/{post_type}', 'StaticController@save_order_post');



Route::post('sta/changeLang', 'StaticController@changeLang');*/


Route::group(['prefix' => '/sta'], function () {
	Route::post('infowithoutlogin','StaticController@infowithoutlogin');

	Route::post('addresrecord', 'StaticController@addresrecord');
	Route::post('manualregister', 'StaticController@register');
	Route::post('addresupdate', 'StaticController@addresupdate');
	Route::post('manuallogout', 'StaticController@logout');
	Route::post('changePassword', 'StaticController@changePassword');
	Route::post('changeUserInfo', 'StaticController@changeUserInfo');
	Route::post('forgetpassword', 'StaticController@forgetpassword');
	Route::post('changeforgotPassword', 'StaticController@changeforgotPassword');
	Route::post('subrecord', 'StaticController@subrecord');


	//Route::post('paymentcheck', 'StaticController@paymentCheck');

	Route::post('parampos_place_order', 'StaticController@parampos_place_order');
	Route::post('iyzico_place_order', 'StaticController@iyzico_place_order');
	Route::post('halk_bankası_place_order', 'StaticController@halk_bankası_place_order');

	Route::get('save_order/{post_type}/{order_no}/{identifier}/{address_id}/{product_type}/{order_id?}/{payment?}', 'StaticController@save_order_get');
	Route::post('save_order/{post_type}', 'StaticController@save_order_post');

	Route::post('{lang}/form_save', 'StaticController@form_save');
	
	Route::post('changeLang', 'StaticController@changeLang');
});