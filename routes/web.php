<?php
//dd("web");

//dd(Request::segments());

//$activeLangCount = App\Language::where('deleted', 'no')->where('status', 'active')->count();
$defaultLang = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
$langs_code = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->pluck('code');
if(!empty($_COOKIE["lang_cookie"])){
	if($langs_code->contains($_COOKIE["lang_cookie"])){
		$lang = $_COOKIE["lang_cookie"];
	}else{
		$lang =$defaultLang->code;
		setcookie("lang_cookie", $lang); 
	}
}else{
    $lang =$defaultLang->code;
}
App::setLocale($lang);
global $mainMenu;
$mainMenu = App\NewTopHasSub::where(
	function ($query){
		$query->where('props','like', '%"position":"all"%')->orWhere('props','like', '%"position":"top"%');
	}
)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first()->thatElementForMenu;


if(count(Request::segments()) == 0){

		header('HTTP/1.1 301 Moved Permanently');

		header("Expires: 0");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		header('Location: /'.$mainMenu->variableLang(App::getLocale())->slug);

		exit();
}else{

	$firstsegment=Request::segments()[0];
	if($firstsegment == "api" || $firstsegment == "sta" || $firstsegment == "login" ){

	}else{	

		

		
            $slugcheck = App\MenuVariable::where('slug',$firstsegment)->count();
			if($slugcheck == 0){
				header('HTTP/1.1 301 Moved Permanently');
				header("Expires: 0");
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				header('Location: /'.$mainMenu->variableLang($lang)->slug);
				exit();
			}
					
				
				
			Route::get('/{slug}/{attr?}/{param?}/{param2?}', 'HomeController@indexOneLang');

		

	}
}

Auth::routes();

