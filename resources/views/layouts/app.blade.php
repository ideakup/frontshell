<!DOCTYPE html>
<html dir="ltr" lang="{{ $lang }}">
<head>

  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
        header("Cache-Control: max-age=3600"); //HTTP 1.1
    ?>



        @if(!empty($seo_title) && $seo_title != 'null')
            <title>{{ $seo_title }}</title>
            <meta name="title" content="{{ $seo_title }}">
        @endif
        @if(!empty($seo_description) && $seo_description != 'null')
            <meta name="description" content="{{ $seo_description }}">
        @endif
        @if(!empty($seo_keywords) && $seo_keywords != 'null')
            <meta name="keywords" content="{{ $seo_keywords }}">
        @endif


    <!-- <meta name="robots" content="index, nofollow"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="İdeaküp Web Teknolojileri Ltd. Şti.">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/favicon-16x16.png">
    <link rel="manifest" href="{{ env('APP_UPLOAD_PATH_V3') }}/favicons/manifest.json">

    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&family=PT+Serif:ital@0;1&family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/icofont/icofont.min.css') }}">
    <link href="{{ mix('css/variables.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/package.css') }}" rel="stylesheet">
    <link href="{{ url('css/custom.css') }}" rel="stylesheet">

    <style>

      .revo-slider-emphasis-text {
        font-size: 64px;
        font-weight: 700;
        letter-spacing: -1px;
        font-family: 'Poppins', sans-serif;
        padding: 15px 20px;
        border-top: 2px solid #FFF;
        border-bottom: 2px solid #FFF;
      }

      .revo-slider-desc-text {
        font-size: 20px;
        font-family: 'Lato', sans-serif;
        width: 650px;
        text-align: center;
        line-height: 1.5;
      }

      .revo-slider-caps-text {
        font-size: 16px;
        font-weight: 400;
        letter-spacing: 3px;
        font-family: 'Poppins', sans-serif;
      }
      .tp-video-play-button { display: none !important; }

      .tp-caption { white-space: nowrap; }

    </style>


</head>

<body class="stretched {{ $slug }}  no-transition">

    @php
        //dd($menu->topHasSub);
        //dump(App::getLocale());
        $__headertheme =config('webshell.pageProperties.default.headerClass');
        $__slidertype = json_decode($menu->topHasSub->first()->props)->slidertype;
        $__breadcrumbvisible ="no";
        $segments = '';
        for ($i=1; $i < count(Request::segments()); $i++) { 
            $segments = $segments.'/'.Request::segments()[$i];
        }
    @endphp

    <div id="wrapper" class="clearfix">

        @include('partials.header.header')
        @include('partials.breadcrumb')
        @include('partials.slidertype')

        @yield('content')

        @include('partials.footer.footer')

        <div id="myModalVideo" class="modal fade bs-example-modal-lg show" style="display: none;" tabindex="-1" aria-labelledby="myModalVideo" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-body">
                    <div class="modal-content"><iframe id="myModalVideoFrame" width="700" height="394" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
                </div>
            </div>
        </div>


    </div>
    
    <script src="{{ mix('js/app.js') }}"></script>

    @yield('inline-scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4dLiSF8_8s_IpcNQaguGO3cxglgqcvxc&callback=initMap" async defer></script>

    <script>
        /*$(function() {

            $('.entry-list-continer>.entry-title').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
            $('.entry-list-continer>.entry-content').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });


            $('.oc-item .entry-c, .oc-item .entry-image').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });

            
            $('.game-result-live-w').matchHeight({
                byRow: false,
                property: 'height',
                target: null,
                remove: false
            });
            
        });*/
    </script>

    {!! $sitesettings->get('google_analytic_code') !!}

    <script src="https://www.google.com/recaptcha/api.js"></script>

     <!-- product script -->
    <script type="text/javascript">
        $(document).ready(function(){
            var imgbaseurl="{{ url(env('APP_UPLOAD_PATH_V3').'medium/')}}";
            $(".add-to-cart").click(function(event){

                event.preventDefault();

                var content_slug=this.attributes["data-slug"].value;
                var x = getCookie("shop_cookie");

                
                var quantity= $('#quantity').val();
                if(x!==""){
                  var shop_cookie=x;
                }
                console.log(shop_cookie);
                $.ajax({
                  url: "/api/add",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  content_slug:content_slug,
                  identifier:shop_cookie,
                  quantity:quantity,
                  userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
                },
                success:function(response){
                  if(response) {    
                    var obj = jQuery.parseJSON( response );
                    $('.insideofcart').remove();
                    $('.cartbottom').remove();

                                
                    for (const [key, value] of Object.entries(obj[0])) {
                      $("#cartcontent ").append('\
                        <div id="insideofcart" class="top-cart-items insideofcart">\
                        <div class="top-cart-item clearfix">\
                        <div class="top-cart-item-image">\
                        <a href="#"><img src="'+imgbaseurl+`${value["options"]["img"]}`+'"/></a>\
                        </div>\
                        <div class="top-cart-item-desc">\
                        <a href="#">'+`${value["name"]}`+'</a>\
                        <span class="top-cart-item-price">'+`${value["price"]+value["tax"]}`+'</span>\
                        <span id="iocqty" class="top-cart-item-quantity"> '+`${value["qty"]}`+'</span>\
                        </div>\
                        </div>\
                        </div>\
                      ');
                    }
                    $("#cartcontent ").append('\
                      <div class="top-cart-action clearfix cartbottom">\
                      <span class="fleft top-checkout-price alltotal">'+obj[1][2]+'</span>\
                      <a href="/shop/cart"><button class="button button-3d button-small nomargin fright">Sepete Git</button></a>\
                      </div>\
                    ');
                    $('.success').text(response.success);            
                    $('#top-cart-count').text(obj[1][3]);                  
                  }
                },
               });
            });
            $(".buy-now").click(function(event){
                event.preventDefault();

                var content_slug=this.attributes["data-slug"].value;
                var x = getCookie("shop_cookie");
                var quantity= $('#quantity').val();

                if(x!==""){
                  var shop_cookie=x;
                }

                $.ajax({
                  url: "/api/add",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  content_slug:content_slug,
                  identifier:shop_cookie,
                  quantity:quantity,
                  userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
                },
                success:function(response){
                  location.href = "{{url('/shop/cart')}}"
                },
               });
            });

            $(".subscribe").click(function(event){
                event.preventDefault();

                var content_slug=this.attributes["data-slug"].value;
                var x = getCookie("shop_cookie");


                if(x!==""){
                  var shop_cookie=x;
                }
                console.log(shop_cookie);

                $.ajax({
                  url: "/api/subscribe",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  content_slug:content_slug,
                  identifier:shop_cookie,
                  userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
                },
                success:function(response){
                  location.href = "{{url('/shop/cartsub')}}"
                },
               });
            });

            /*sub-*/


            function getCookie(cname) {
              var name = cname + "=";
              var decodedCookie = decodeURIComponent(document.cookie);
              var ca = decodedCookie.split(';');
              for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                  c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
                }
              }
              return "";
            }
            function setCookie(cname, cvalue) {
              document.cookie = cname + "=" + cvalue;
            }

            $(document).ready(function(){
                  $( "#logoutBtn" ).click(function() {
                      $('#logoutForm').submit();
                  });
            });
        });



    </script>
    @php
    if($__slidertype == 'slider'){
      $sliderheight = 'fullwidth';
    }elseif($__slidertype == 'full-slider'){
      $sliderheight = 'fullscreen';
    }else{
      $sliderheight = 'fullwidth';
    }
    @endphp


    <script>
      var tpj=jQuery;
      tpj.noConflict();
      var $ = jQuery.noConflict();


      tpj(document).ready(function() {
        var apiRevoSlider = tpj('#rev_slider_k_fullwidth').show().revolution(
        {
          sliderType:"standard",
          sliderLayout:"{{$sliderheight}}",
          delay:9000,
          navigation: {
            arrows:{enable:true}
          },
          responsiveLevels:[1240,1024,778,480],
          visibilityLevels:[1240,1024,778,480],
          gridwidth:[1240,1024,778,480],
          gridheight:[600,768,960,720],
        });

        apiRevoSlider.on("revolution.slide.onloaded",function (e) {
          setTimeout( function(){ SEMICOLON.slider.sliderDimensions(); }, 400 );
        });

        apiRevoSlider.on("revolution.slide.onchange",function (e,data) {
          SEMICOLON.slider.revolutionSliderMenu();
        });
      });
    </script>


    

     

</body>
</html>
