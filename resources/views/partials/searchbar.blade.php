@php
	
@endphp

<div class="widget  clearfix">
	<div class="input-group divcenter">
		<input id="search" type="text" class="form-control" 
		@if(is_null(Request::get('search')))
			placeholder="Ara..."
		@else
			placeholder="{{Request::get('search')}}"
			 >
		@endif
		<div class="input-group-append">
			<button id="searchbtn" class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
		</div>
	</div>
</div>