@php
$footer = App\NewTopHasSub::where('sub_model','Footer')->first();

$bottom_footer = App\NewTopHasSub::where('sub_model','BottomFooter')->first();

$top_footer = App\NewTopHasSub::where('sub_model','TopFooter')->first();
@endphp
        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">
            @if(json_decode($footer->props)->topfooter != 'none')
                <div class="container">
                    <!-- Footer Widgets
                    ============================================= -->
                    <div class="footer-widgets-wrap">

                        <div class="row col-mb-50">
                                

                            @foreach($top_footer->subFooterZones as $zonetths)

                                <div class="{{json_decode($zonetths->props)->props_colvalue}}">
                                    <div class="row">

                                        @foreach($zonetths->subFooterContent as $zonecontentths)

                                            @php
                                                if($zonecontentths->sub_model=='F-Content'){
                                                    $fcontent = $zonecontentths->thatElementForContent;
                                                }
                                                
                                                if (empty($fcontent->variableLang($lang))) {
                                                    $contVariable = $fcontent->variable;
                                                }else{
                                                    $contVariable = $fcontent->variableLang($lang);
                                                }
                                                $group_content=json_decode($contVariable->content);
                                                
                                                $textAlign = json_decode($zonecontentths->props)->text_align;
                                            @endphp
                                            <div class="{{json_decode($zonecontentths->props)->props_colvalue}}">

                                                @if ($fcontent->type == 'link')
                                                    @include('types.menupartials.footercontents.link')
                                                @elseif ($fcontent->type == 'social_media_button')
                                                    @include('types.menupartials.footercontents.socialmedia')
                                                @elseif ($fcontent->type == 'contactinfo')
                                                    @include('types.menupartials.footercontents.contactinfo')
                                                @elseif ($fcontent->type == 'footermenu')
                                                    @include('types.menupartials.footercontents.topfootermenu')
                                                @elseif($fcontent->type == 'text')
                                                    @include('types.menupartials.basiccontents.text')
                                                @elseif($fcontent->type == 'footerlogo')
                                                    @include('types.menupartials.footercontents.footerlogo')
                                                @elseif($fcontent->type == 'footercopyright')
                                                    @include('types.menupartials.footercontents.footercopyright')
                                                @endif

                                            </div>      
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
     

                        </div>

                    </div><!-- .footer-widgets-wrap end -->
                </div>
            @endif
            <div id="copyrights">
                <div class="container">

                    <div class="row">
                        @foreach($bottom_footer->subFooterZones as $zonetths)

                            <div class="{{json_decode($zonetths->props)->props_colvalue}}">
                                <div class="row">
                                    @foreach($zonetths->subFooterContent as $bottom_footer_content_ths)

                                        @php
                                            
                                            $fcontent = $bottom_footer_content_ths->thatElementForContent;
                                            
                                            if (empty($fcontent->variableLang($lang))) {
                                                $contVariable = $fcontent->variable;
                                            }else{
                                                $contVariable = $fcontent->variableLang($lang);
                                            }
                                            $group_content=json_decode($contVariable->content);

                                            $textAlign = json_decode($bottom_footer_content_ths->props)->text_align;
                                        @endphp
                                        <div class="{{json_decode($bottom_footer_content_ths->props)->props_colvalue}}">

                                            @if ($fcontent->type == 'link')
                                                @include('types.menupartials.footercontents.link')
                                            @elseif ($fcontent->type == 'social_media_button')
                                                @include('types.menupartials.footercontents.socialmedia')
                                            @elseif ($fcontent->type == 'contactinfo')
                                                @include('types.menupartials.footercontents.contactinfo')
                                            @elseif ($fcontent->type == 'footermenubottom')
                                                @include('types.menupartials.footercontents.footermenu')
                                            @elseif($fcontent->type == 'text')
                                                 @include('types.menupartials.basiccontents.text')
                                            @elseif($fcontent->type == 'footercopyright')
                                                @include('types.menupartials.footercontents.footercopyright')
                                            @endif
                                        </div>
                                               
                                    @endforeach
                                </div>
                            </div>
                        @endforeach        
                    </div>  

                </div>

            </div>

        </footer><!-- #footer end -->