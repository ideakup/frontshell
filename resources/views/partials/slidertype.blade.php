
@switch($__slidertype)
    
    @case("image")
        
        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $menu->variableLang($lang)->stvalue }}'); background-size: 100%; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px 0px;">

            <div class="container clearfix page-image">
                <h2>{{ $menu->variableLang($lang)->name }}</h2>
                
                @include('partials.breadcrumb')
                
            </div>

        </section><!-- #page-title end -->

        @break

    @case("slider")
        @if(is_null($attr) || $content->type != 'mapturkey')

            <section id="slider" class="slider-element revslider-wrap  min-vh-75 ">
                <div class="slider-inner">
                    <div id="rev_slider_k_fullwidth_wrapper" class="rev_slider_wrapper fullwidth-container"  style="padding:0px;">
                        <div id="rev_slider_k_fullwidth" class="rev_slider fullwidthbanner" style="display:none;" data-version="5.1.4">
                            <ul>
                                @foreach ($menu->slider as $sElement)
                                    @php
                                        if (empty($sElement->variableLang($lang))) {
                                            $elementVariable = $sElement->variable;
                                        }else{
                                            $elementVariable = $sElement->variableLang($lang);
                                        }
                                        $alwal = explode(':',$sElement->align);
                                    @endphp
                                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}" data-delay="15000"  data-saveperformance="off"  data-title="Unlimited Possibilities">
                                        <!-- MAIN IMAGE -->
                                        <img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}"  alt="kenburns6" data-bgposition="center bottom" data-bgpositionend="center top" data-kenburns="on" data-duration="25000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="140" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                                        <!-- LAYERS -->

                                        <div class="tp-caption ltl tp-resizeme revo-slider-emphasis-text p-0 border-0"
                                        data-x="middle" data-hoffset="0"
                                        data-y="top" data-voffset="185"
                                        data-fontsize="['60','50','50','40']"
                                        data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                        data-speed="800"
                                        data-start="1200"
                                        data-easing="easeOutQuad"
                                        data-splitin="none"
                                        data-splitout="none"
                                        data-elementdelay="0.01"
                                        data-endelementdelay="0.1"
                                        data-endspeed="1000"
                                        data-endeasing="Power4.easeIn" style="z-index: 3; color: #333; white-space: nowrap;">{{ $elementVariable->title }}</div>

                                        <div class="tp-caption ltl tp-resizeme revo-slider-desc-text"
                                        data-x="middle" data-hoffset="0"
                                        data-y="top" data-voffset="295"
                                        data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                        data-speed="800"
                                        data-lineheight="['30','30','34','26']"
                                        data-width="['750','750','480','360']"
                                        data-start="1400"
                                        data-easing="easeOutQuad"
                                        data-splitin="none"
                                        data-splitout="none"
                                        data-elementdelay="0.01"
                                        data-endelementdelay="0.1"
                                        data-endspeed="1000"
                                        data-textAlign="center"
                                        data-endeasing="Power4.easeIn" style="z-index: 3; color: #333; max-width: 650px; white-space: normal;">{{ $elementVariable->description }}</div>

                                        <div class="tp-caption ltl tp-resizeme"
                                        data-x="middle" data-hoffset="0"
                                        data-y="top" data-voffset="405"
                                        data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                        data-speed="800"
                                        data-start="1550"
                                        data-easing="easeOutQuad"
                                        data-splitin="none"
                                        data-splitout="none"
                                        data-elementdelay="0.01"
                                        data-endelementdelay="0.1"
                                        data-endspeed="1000"
                                        data-endeasing="Power4.easeIn" style="z-index: 3;"><a href="{{ $elementVariable->button_url }}" class="button button-border button-large button-rounded text-end m-0"><span>{{ $elementVariable->button_text }}</span><i class="icon-angle-right"></i></a></div>

                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @break
    @case("full-slider")
        <section id="slider" class="slider-element revslider-wrap  min-vh-100 include-header">
            <div class="slider-inner">
                <div id="rev_slider_k_fullwidth_wrapper" class="rev_slider_wrapper fullscreen-container"  style="padding:0px;">
                    <div id="rev_slider_k_fullwidth" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.4">
                        <ul>
                            @foreach ($menu->slider as $sElement)
                                @php
                                    if (empty($sElement->variableLang($lang))) {
                                        $elementVariable = $sElement->variable;
                                    }else{
                                        $elementVariable = $sElement->variableLang($lang);
                                    }
                                    $alwal = explode(':',$sElement->align);
                                @endphp
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}" data-delay="15000"  data-saveperformance="off"  data-title="Unlimited Possibilities">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}"  alt="kenburns6" data-bgposition="center bottom" data-bgpositionend="center top" data-kenburns="on" data-duration="25000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="140" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->

                                    <div class="tp-caption ltl tp-resizeme revo-slider-emphasis-text p-0 border-0"
                                    data-x="middle" data-hoffset="0"
                                    data-y="top" data-voffset="185"
                                    data-fontsize="['60','50','50','40']"
                                    data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                    data-speed="800"
                                    data-start="1200"
                                    data-easing="easeOutQuad"
                                    data-splitin="none"
                                    data-splitout="none"
                                    data-elementdelay="0.01"
                                    data-endelementdelay="0.1"
                                    data-endspeed="1000"
                                    data-endeasing="Power4.easeIn" style="z-index: 3; color: #333; white-space: nowrap;">{{ $elementVariable->title }}</div>

                                    <div class="tp-caption ltl tp-resizeme revo-slider-desc-text"
                                    data-x="middle" data-hoffset="0"
                                    data-y="top" data-voffset="295"
                                    data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                    data-speed="800"
                                    data-lineheight="['30','30','34','26']"
                                    data-width="['750','750','480','360']"
                                    data-start="1400"
                                    data-easing="easeOutQuad"
                                    data-splitin="none"
                                    data-splitout="none"
                                    data-elementdelay="0.01"
                                    data-endelementdelay="0.1"
                                    data-endspeed="1000"
                                    data-textAlign="center"
                                    data-endeasing="Power4.easeIn" style="z-index: 3; color: #333; max-width: 650px; white-space: normal;">{{ $elementVariable->description }}</div>

                                    <div class="tp-caption ltl tp-resizeme"
                                    data-x="middle" data-hoffset="0"
                                    data-y="top" data-voffset="405"
                                    data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                    data-speed="800"
                                    data-start="1550"
                                    data-easing="easeOutQuad"
                                    data-splitin="none"
                                    data-splitout="none"
                                    data-elementdelay="0.01"
                                    data-endelementdelay="0.1"
                                    data-endspeed="1000"
                                    data-endeasing="Power4.easeIn" style="z-index: 3;"><a href="{{ $elementVariable->button_url }}" class="button button-border button-large button-rounded text-end m-0"><span>{{ $elementVariable->button_text }}</span><i class="icon-angle-right"></i></a></div>

                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @break

    @default
        
        
@endswitch





