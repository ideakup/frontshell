@php
if(is_null(Request::get('p_dist'))){
    $p_numbers=$pag_number/15;
}
else{
     $p_numbers=$pag_number/Request::get('p_dist');
}


//dd($p_numbers);
@endphp
<div @if($p_numbers<=1) style="display: none;"@endif class="row">
    <div class="col-md-4 offset-md-4">
        <ul class="pagination ">
        @for($i=1; $i<=(int)$p_numbers+1; $i++)
            @php
            $link="";
                if((!empty(Request::all()))){
                    if(is_null(Request::get('pag'))){
                        $link=Request::fullUrl()."&pag=".$i;
                    }
                    else{
                        $pag=Request::all();
                       
                       // dump($pag);
                        $pag['pag']=$i;
                        
                        $link=Request::url()."?".http_build_query($pag, '', '&amp;');

                    }
                }
                else{
                    $link=Request::fullUrl()."?pag=".$i;
                }
                
               // dd(  $pag);
            @endphp
            <li  class="page-item"><a class="page-link @if(Request::input('pag') == $i) chosenpag @endif " href="{{ $link }}">{{$i}}</a></li> 
         @endfor
    </ul>
    </div> 
    
</div>

