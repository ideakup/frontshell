@php
$sort = array(0=>"Akıllı Sıralama",'ASC'=>"En düşük fiyat",'DESC'=>"En yüksek fiyat");
$pag=array("15","30","60","120");

@endphp

<div style="border-bottom: 1px solid #eee; margin: 0 0 30px;"  class="row">
  <div class="col-lg-6">
    <div>
      <label >
        Sırala
      </label>
      <select id="sirala" name="sirala">
        @foreach($sort as $key => $srt)
          <option value="{{$key}}"@if(Request::get('sort')==$key) selected @endif>{{ $srt}}</option>
        @endforeach
      </select>
    </div>
  </div>
   <div class="col-lg-6 ">
     <div class="fright pag">
      <label >
        Göster
      </label>
       <select id="p_dist" name="goster">
        @foreach($pag as  $pg)
          <option value="{{$pg}}"@if(Request::get('p_dist')==$pg) selected @endif>{{ $pg}}</option>
        @endforeach
      </select>
    </div>
   </div>
</div>
