 @php
 //dump($menuitem);
 //dd("asdas");
 @endphp   
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
      
        <li class="menu-item">
             @if (!is_null($menuitemVariable))                   
                <a class="menu-link"  href="@if($menuitem->type != 'menuitem') {{ url($langSlug.'/'.$menuitemVariable->slug) }} @else # @endif">
                    @if($setting->menu_appearance == 'style-5')
                        <i class="{{ json_decode($menuitemVariable->props)->menuicon }}"></i>
                    @endif
                    <div class="d-inline-block">{{ $menuitemVariable->menutitle }}</div>
                    @if($setting->menu_appearance == 'sub-title')
                        <span>{{ json_decode($menuitemVariable->props)->subtitle }}</span>
                    @endif
                </a>
            @endif
            @if ($menuitem->subMenuTop->count() > 0)
                <ul class="sub-menu-container" >

                    @foreach ($menuitem->subMenuTop as $menuitem)

                        @php
                            if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
                                $menuitemVariable = $menuitem->thatElementForMenu->variable;
                            }else{
                                $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.header.menu.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        @if(json_decode($menuitem->props)->position == 'top' || json_decode($menuitem->props)->position == 'all')
            <li class="menu-item">
                @if (!is_null($menuitemVariable))                   
                    <a class="menu-link"  href="@if($menuitem->type != 'menuitem') {{ url($langSlug.'/'.$menuitemVariable->slug) }} @else # @endif">
                        @if($setting->menu_appearance == 'style-5')
                            <i class="{{ json_decode($menuitemVariable->props)->menuicon }}"></i>
                        @endif
                        <div class="d-inline-block">{{ $menuitemVariable->menutitle }}</div>
                        @if($setting->menu_appearance == 'sub-title')
                            <span>{{ json_decode($menuitemVariable->props)->subtitle }}</span>
                        @endif
                    </a>
                @endif
            </li>
        @endif
    @endif

    
