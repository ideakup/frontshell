@if (empty($submenu))
    @if ($menuitem->subMenuTop->count() > 0)
    
        @php $i++; @endphp

        <li>
            <a href="@if ($menuitem->type == 'menuitem')#@else{{ url($langSlug.'/'.$menuitemVariable->slug) }}@endif">
                <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' * '.$i --}} </div>
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul>
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
        
    @else
        @if($menuitem->position == 'top' || $menuitem->position == 'all')
            <li>
                @if (!is_null($menuitemVariable))
                    @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery' || $menuitem->type == 'mixed')
                        <a href="{{ url($langSlug.'/'.$menuitemVariable->slug) }}">
                            <div>{{ $menuitemVariable->menutitle }}</div>
                        </a>
                    @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                        <a href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'menuitem')
                        <a href="#">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @endif
                @endif
            </li>
        @endif
    @endif
@else
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li>
            <a href="@if ($menuitem->type == 'menuitem')#@else{{ url($langSlug.'/'.$menuitemVariable->slug) }}@endif">
                <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' ? '.$i --}}</div>
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul>
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        @php $i++; @endphp
        @if($menuitem->position == 'top' || $menuitem->position == 'all')
            @if (!is_null($menuitemVariable))
                <li>
                    @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery' || $menuitem->type == 'mixed')
                        <a href="{{ url($langSlug.'/'.$menuitemVariable->slug) }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                        <a href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'menuitem')
                        <a href="#">
                            <div>{{ $menuitemVariable->menutitle }}</div>
                        </a>
                    @endif
                </li>
            @endif
        @endif
    @endif
@endif