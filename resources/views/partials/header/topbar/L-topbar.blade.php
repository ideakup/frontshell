@php
//dump($l_topbar->subHeaderContent[0]->thatElementForContent)
@endphp
<div class="top-links on-click">
    <ul class="top-links-container">
        @foreach($l_topbar->subHeaderContent as $lcontentths)

        	@php
        	if($lcontentths->sub_model=='H-Content'){
                    $lcontent = $lcontentths->thatElementForContent;
                }
                
                if (empty($lcontent->variableLang($lang))) {
                    $contVariable = $lcontent->variable;
                }else{
                    $contVariable = $lcontent->variableLang($lang);
                }
                $group_content=json_decode($contVariable->content);  
        	@endphp

                @if ($lcontent->type == 'link')
                    @include('types.menupartials.headercontents.link')
                @elseif ($lcontent->type == 'contactinfo')
                    @include('types.menupartials.headercontents.contactinfo')
                @elseif ($lcontent->type == 'social_media_button')
                    @include('types.menupartials.headercontents.socialmedia')   
                @endif
                   

        @endforeach
    </ul>
</div>