@php
$l_topbar = App\NewTopHasSub::where('sub_model','L-Topbar')->first();
$r_topbar = App\NewTopHasSub::where('sub_model','R-Topbar')->first();

@endphp

<div id="top-bar" class="{{$setting->color_thema}}">
    <div class=" {{$setting->topbar}} clearfix">
        <div class="row justify-content-between">
            <div class="col-12 col-md-auto"> 
                @include('partials.header.topbar.L-topbar')
            </div>
            <div class="col-12 col-md-auto"> 
                @include('partials.header.topbar.R-topbar')
            </div>
        </div>
    </div>
</div>