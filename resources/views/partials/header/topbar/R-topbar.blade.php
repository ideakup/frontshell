@php
//dump($l_topbar->subHeaderContent[0]->thatElementForContent)t
@endphp

<div class="top-links on-click">
    <ul class="top-links-container">

        @foreach($r_topbar->subHeaderContent as $lcontentths)

        	@php
        	if($lcontentths->sub_model=='H-Content'){
                    $lcontent = $lcontentths->thatElementForContent;
                }
                
                if (empty($lcontent->variableLang($lang))) {
                    $contVariable = $lcontent->variable;
                }else{
                    $contVariable = $lcontent->variableLang($lang);
                }
                $group_content=json_decode($contVariable->content); 
                //dd($group_content); 
        	@endphp
                
                @if ($lcontent->type == 'link')
                    @include('types.menupartials.headercontents.link')
                @elseif ($lcontent->type == 'contactinfo')
                    @include('types.menupartials.headercontents.contactinfo')
                @elseif ($lcontent->type == 'social_media_button')
                    @include('types.menupartials.headercontents.socialmedia')   
                @endif
                  

        @endforeach

        @if($langs->count() > 1)
            <li class="top-links-item">
                <form style="margin-bottom: unset;"  name="withoutLogin-form" class="nobottommargin" action="{{url('sta/changeLang')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="menu_id" value="{{ $menu->id }}">
                    <div style="padding: 12px" class="btn-group btn-group-sm" role="group" aria-label="First group">
                        @foreach($langs as $lng)
                            @if(!empty($menu->variableLang($lng->code)->slug))   
                                <button @if($lang == $lng->code ) style="background-color: #5C636A;" @endif type="submit" class="btn btn-secondary" name="lang" value="{{ $lng->code }}">{{ $lng->code }}</button>
                            @endif
                        @endforeach
                    </div>
                </form> 
            </li> 
        @endif
  </ul>
</div>
        

                 