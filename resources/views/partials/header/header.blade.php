@php
    $header = App\NewTopHasSub::where('sub_model','Header')->first();
    $setting = json_decode($header->props);
@endphp

@if($setting->topbar != 'none')
   @include('partials.header.topbar.topbar')
@endif

<header id="header" class="@if($setting->menu_position == "below") header-size-sm @endif {{$setting->color_thema}}">
    @if($setting->menu_position == "side")
        <div id="header-wrap">
    @endif
        <div class="{{$setting->header_width}}">
            <div class="header-row {{$setting->menu_alignment}}@if($setting->menu_position == "below") flex-column flex-lg-row justify-content-center justify-content-lg-start @endif">
                <div id="logo" class="@if($setting->menu_position == "below") ws-m-auto @endif">
                    <a href="{{ url('/') }}" class="standard-logo" data-light-logo="{{ env('APP_UPLOAD_PATH_V3') }}/logos/logo-light-tr.png">
                        <img src="{{ env('APP_UPLOAD_PATH_V3') }}/logos/logo-light-tr.png" alt="" />
                    </a>
                    <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="{{ env('APP_UPLOAD_PATH_V3') }}/logos/logo-dark-tr.png">
                        <img src="{{ env('APP_UPLOAD_PATH_V3') }}/logos/logo-dark-tr.png" alt="" />
                    </a>
                </div>

                @if($setting->side_area)
                    <div class="header-misc ws-side-area mb-4 mb-lg-0 order-lg-last">
                        @include('partials.header.sidearea.sidearea')                             
                    </div>
                @endif

                @if($setting->menu_position == "side")
                    <div class="header-misc">
                        @if($setting->search_position == "default")
                            <div id="top-search" class="header-misc-icon d-none d-lg-block">
                                <a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                            </div>
                        @endif

                        @if($menuformodule->where('type','module-shop')->count()>0)
                            @include('types.shop.shopping-topcart')
                        @endif

                        @if($menuformodule->where('type','module-user')->count()>0)
                            @include('types.user.user-topaccount')
                        @endif
                    </div>

                    <div id="primary-menu-trigger">
                        <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                    </div>
                    <nav class="primary-menu {{$setting->menu_appearance}}">
                        <ul class="menu-container">
                            @if($setting->search_position == "default")
                                <form id="search_form form-group" class="d-lg-none">
                                    <div class="input-group has-validation">
                                        <input id="search_input" type="text" class="form-control" value="" placeholder="Ara..." autocomplete="off">
                                        <div class="input-group-prepend">
                                            <button class="btn"><i class="icon-line-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                            @php 
                                $i = 0; 
                            @endphp
                            @foreach ($topmenus as $menuitem)
                                @if(count($menuitem->thatElementForMenu->topHasSubContent()->get()) > 0)
                                    @php
                                        if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
                                            $menuitemVariable = $menuitem->thatElementForMenu->variable;
                                        }else{
                                            $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
                                        }
                                    @endphp
                                    @include('partials.header.menu.headermenu')
                                @endif
                            @endforeach
                        </ul>
                    </nav>

                    @if($setting->search_position == "default")
                        <form id="search_form" class="top-search-form" >
                            <input id="search_input" type="text"  class="form-control" value="" placeholder="Ara..." autocomplete="off">
                        </form>
                    @endif
                @endif
            </div>
        </div>

        @if($setting->menu_position == "below")
            <div id="header-wrap">
                <div class="{{$setting->header_width}}">
                    <!-- menu_alignment classları burası için düzenlenecek  -->
                    <div class="header-row header-row justify-content-between flex-row-reverse flex-lg-row top-search-parent {{$setting->menu_alignment}}-below">
                        <div class="header-misc ">
                            @if($setting->search_position == "default")
                                <div id="top-search" class="header-misc-icon">
                                    <a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                                </div>
                            @endif

                            @if($menuformodule->where('type','module-shop')->count()>0)
                                @include('types.shop.shopping-topcart')
                            @endif

                            @if($menuformodule->where('type','module-user')->count()>0)
                                @include('types.user.user-topaccount')
                            @endif
                        </div>

                        <div id="primary-menu-trigger">
                            <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                        </div>
                        <nav class="primary-menu {{$setting->menu_appearance}}">
                            <ul class="menu-container">
                                @php $i = 0; 
                                    //dd($topmenus[]->thatElementForMenu->topHasSubContent($season_id,$seasons_id));
                                @endphp
                                @foreach ($topmenus as $menuitem)
                                    @if(count($menuitem->thatElementForMenu->topHasSubContent()->get()) > 0)
                                        @php
                                            if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
                                                $menuitemVariable = $menuitem->thatElementForMenu->variable;
                                            }else{
                                                $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
                                            }
                                        @endphp
                                        @include('partials.header.menu.headermenu')
                                    @endif
                                @endforeach
                            </ul>
                        </nav>

                        @if($setting->search_position == "default")
                             <form id="search_form" class="top-search-form" >
                                <input id="search_input" type="text"  class="form-control" value="" placeholder="Ara..." autocomplete="off">
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        @endif

    @if($setting->menu_position == "side")
        </div>
    @endif
    <div class="header-wrap-clone"></div>
</header>




