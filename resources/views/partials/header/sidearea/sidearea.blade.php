@php
$sidearea = App\NewTopHasSub::where('sub_model','Sidearea')->first();
@endphp
<ul class="header-extras me-0">
    
    @foreach($sidearea->subHeaderContent as $scontentths)
        <li>
        	@php
        	if($scontentths->sub_model=='H-Content'){
                    $scontent = $scontentths->thatElementForContent;
                }
                if (empty($scontent->variableLang($lang))) {
                    $contVariable = $scontentths->variable;
                }else{
                    $contVariable = $scontent->variableLang($lang);
                }
                $group_content=json_decode($contVariable->content);  
        	@endphp
                @if($scontent->type == 'photo')
                    @include('types.menupartials.basiccontents.photo')
                @elseif($scontent->type == 'text')
                    @include('types.menupartials.basiccontents.text')
                @elseif($scontent->type == 'group-icon')
                    @include('types.menupartials.headercontents.icon')
                @elseif($scontent->type == 'link')
                   @include('types.menupartials.headercontents.sidearealink')
                @endif
        </li>
    @endforeach
    
</ul>
