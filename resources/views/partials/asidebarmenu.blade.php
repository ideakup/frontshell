@php
//dd($asidemenus[2]->subMenuAside);
@endphp
    @if ($menuitem->subMenuAside->count() > 0)
    	@php $pp++; @endphp
		<li>
            <a      
                @php
                $suballmenu=$menuitem->subMenu;                                                           
                @endphp
                @foreach($suballmenu as $sbm)
                        @if(!starts_with($sbm->thatElementForMenu->type, 'list'))
                            href="/{{$sbm->thatElementForMenu->variable->slug}}"
                        @endif    
                @endforeach
                href="/{{$menuitem->thatElementForMenu->variableLang($lang)->slug}}">
                <div>{{ $menuitem->thatElementForMenu->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' * '.$pp --}}</div>
            </a>
        </li>
        
        @if ($menuitem->subMenuAside->count() > 0)
			<ul>
                @foreach ($menuitem->subMenuAside as $menuitem)
                    @if (json_decode($menuitem->props)->position != 'top')
                        @include('partials.asidebarmenu', ['menuitem' => $menuitem, 'submenu' => true])
                    @endif
                @endforeach
            </ul>
        @endif
    @else
        @if(json_decode($menuitem->props)->position == 'aside' || json_decode($menuitem->props)->position == 'all')
            @if (!is_null($menuitem->thatElementForMenu->variableLang($lang)))
                
                @if ($menuitem->thatElementForMenu->type == 'content' || starts_with($menuitem->thatElementForMenu->type, 'list') || $menuitem->thatElementForMenu->type == 'photogallery')
                    <li>
                        <a href="{{ url($langSlug.'/'.$menuitem->thatElementForMenu->variableLang($lang)->slug) }}">
                            <div>{{ $menuitem->thatElementForMenu->variableLang($lang)->menutitle }}</div>
                        </a>
                    </li>
                @elseif ($menuitem->thatElementForMenu->type == 'link' && !is_null($menuitem->thatElementForMenu->variableLang($lang)->stvalue))
                    <li>
                        <a href="{{ json_decode($menuitem->thatElementForMenu->variableLang($lang)->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang($lang)->stvalue)->target }}">
                            <div>{{ $menuitem->thatElementForMenu->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' + '.$i --}}</div>
                        </a>
                    </li>
                @endif
                
            @endif
        @endif
    @endif
