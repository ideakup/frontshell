@php
    if($menu->type == 'blog' || $menu->type == 'product' || $menu->type == 'playlist'){
        if(!empty($content)){
            $_menu = $content->contentThs->first()->topElementForContent->contentThs->first()->topElementForMenu;
        }
        else{
            $_menu = $menu;
        }
    }else{
        $_menu = $menu;
    }
@endphp
@if($__slidertype == 'breadcrumb')
    <section id="page-title" class="page-title-mini ">
        <div class="container clearfix">
            
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/{{$_menu->variable->slug}}">{{$_menu->variable->name}}</a></li>
                @if(!empty($content))
                    <li class="breadcrumb-item"><a href="/{{$_menu->variable->slug}}/{{$content->variable->slug}}">{{$content->variable->title}}</a></li>
                @endif
            </ol>
	    </div>
	</section>
@elseif($__slidertype == 'image_breadcrumb')
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $menu->            variableLang($lang)->stvalue }}'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;"                             data-top-bottom="background-position:0px -300px;">

        <div class="container clearfix">
            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/{{$_menu->variable->slug}}">{{$_menu->variable->name}}</a></li>
                    @if(!empty($content))
                        <li class="breadcrumb-item"><a href="/{{$_menu->variable->slug}}/{{$content->variable->slug}}">{{$content->variable->title}}</a></li>
                    @endif
                </ol>
            </div>
        </div>

    </section><!-- #page-title end -->
@endif

     