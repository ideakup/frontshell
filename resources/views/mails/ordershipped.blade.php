@php use Carbon\Carbon; @endphp
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- If you delete this meta tag, the ground will open and swallow you. -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>From Send Mail</title>
    <style type="text/css">

        #content .container {
            position: relative;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }
        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }
        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        th {
            text-align: inherit;
        }
        .fright {
            float: right !important;
        }

    </style>
</head>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

    <div style="width: 700px; margin:auto;" id="qqqq" class="container clearfix">
        <div  class="row clearfix"> 
            <div class="col-lg-1"></div>
            @php
                $order=App\ShoppingOrder::where('order_no',$data->order_no)->first();
                //dd($order);
            @endphp
            @if(!is_null($order))
                <div class="col-lg-10">
                    <div class="card">
                        <div class="card-body text-center">
                            <h3>
                                @php


                                    
                                    $card_total_amount=0;
                                    $total_product_amount=0;
                                    
                                    $customers_info=json_decode($order->address_information)->adress;

                                    if(!is_null($order->user_id)){
                                        $user=App\User::find($order->user_id);
                                        $firstname=$user->firstname;
                                        $lastname=$user->lastname;  

                                    }
                                    else{
                                        $firstname=json_decode($customers_info)->lastname;
                                        $lastname=json_decode($customers_info)->firstname;  
                                    }

                                    $customer_name= $firstname." ".$lastname;
                                    $adress=json_decode($customers_info)->address;
                                    $town_city=json_decode($order->address_information)->town."/".json_decode($order->address_information)->city;
                                    
                                    $phone=json_decode($customers_info)->phone;
                                    $shipping_cost=json_decode($order->address_information)->shipping_cost;
                                    $shipping_type=json_decode($order->address_information)->shipping_type;
                                    $shipping_cost_price=json_decode($order->address_information)->shipping_cost_price;
                                    $min_shippingcost=json_decode($order->address_information)->min_shippingcost;


                                    //EMAİL GİZLEME
                                    $emailArr = explode('@', $order->email);
                                    $emailFirst = substr($emailArr[0], 0, 1);
                                    $emailLast = substr($emailArr[0], -1, 1);
                                    $emailChrCount = strlen($emailArr[0]);
                                    $emailSecret = '';
                                    for ($i=0; $i < $emailChrCount-2; $i++) { 
                                        $emailSecret .= '*';
                                    }
                                    $userLimit['email'] = $emailFirst.$emailSecret.$emailLast.'@'.$emailArr[1];
                                    //ADRESS GİZLEME
                                    $adressArr = explode(' ', $adress);
                                    for ($i=0; $i < count($adressArr)-2; $i++) { 
                                        $emailSecret .= '*';
                                    }
                                    $newadress=$adressArr[0].$emailSecret.end($adressArr);

                                    //TELEFON GİZLEME
                                    $phoneS = $phone;
                                    if(strlen($phone) == 10){
                                        $phoneS = '0'.$phone;
                                    }elseif(strlen($phone) == 11){
                                        $phoneS = $phone;
                                    }
                                    $phoneSecret = substr($phoneS, 0, 1).' ('.substr($phoneS, 2, 1).') * ** *'.substr($phoneS, -2, 2);
                                    $userLimit['phone'] = $phoneSecret;
                                    

                                @endphp
                                    <h3>Sipariş No:{{$data->order_no}}</h3>
                                    <p>Siparişin için Teşekkürler <strong>{{$customer_name}} </strong>:)</p>
                                    <a href="{{url('user/orders/'.$data->order_no)}}">Sipariş detayı için tıklayınız</a>
                            </h3>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table order">
                                @if($order->status=='Hazırlanıyor')<span class="badge badge-warning">Hazırlanıyor</span>
                                @elseif($order->status=='Kargoda')<span  class="badge badge-info">Kargoda</span>
                                @elseif($order->status=='teslim_edildi')<span class="badge badge-success">Teslim Edildi</span>
                                @endif
                        <thead>
                            <tr>
                                <th class="order-date">{{$order->created_at->format('d.m.Y H:i')}}</th>
                                <th class="order-product-name">Ürün</th>
                                <th class="order-product-quantity">Miktar</th>
                                <th class="order-product-price">Fiyat</th>
                                <th class="order-product-subtotal">Tutar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(json_decode($order->order_content) as $row)
                    
                                <tr class="order_item">
                                    <td class="order-product-thumbnail">
                                        @php
                                         $allcontent=$allcontent->where('content_id',$row->id)->first();
                                        @endphp
                                        @php
                                            $group_content=json_decode($allcontent->content);
                                        @endphp
                                        <img width="64" height="64" src="{{ url(env('APP_UPLOAD_PATH_V3').'medium/'. $group_content->photo) }}">
                                     </td>
                                     <td class="order-product-name">
                                       {{$row->name}}
                                     </td>
                                     <td class="order-product-quantity">
                                        <span>{{ $row->qty}} </span>    
                                    </td>
                                    <td class="cart-product-price text-center">
                                      <span class="price text-center">{{number_format((float)$row->price, 2, ',', '')}} ₺</span>
                                    </td>
                                    @php
                                      $rowtotal=$row->price+(($row->price)*($row->options->tax_rate)/100);
                                      $rowtotal1=$rowtotal*$row->qty;
                                    @endphp 
                                    <td class="order-product-subtotal">
                                        <span class="amount">{{number_format((float)$rowtotal1, 2, ',', '')}} ₺</span>
                                        @php
                                        $card_total_amount=$card_total_amount+($rowtotal*$row->qty);
                                        $total_product_amount=$total_product_amount+$row->qty;
                                        @endphp
                                   </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>

                        @php
                            if($shipping_type==1){
                                $shipping_cost_price=$shipping_cost_price*$total_product_amount;
                            }
                        @endphp
                        <div class="fright">
                            @if($shipping_cost=='active')
                                @if($card_total_amount<$min_shippingcost)
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                          <strong>Kargo Ücreti:</strong>
                                        </td>
                                        <td class="cart-product-name">
                                            <span class="amount">{{number_format((float)$shipping_cost_price, 2, ',', '')}} ₺</span>
                                        </td>
                                      </tr>
                                @endif
                              @endif
                              <br>
                            <tr class="cart_item">
                            <td class="cart-product-name">
                              <strong>Toplam:</strong>
                            </td>
                            <td class="cart-product-name">

                                @php
                                
                                    if($shipping_cost=='active'){
                                        if($card_total_amount<$min_shippingcost){
                                            $card_total_amount=$card_total_amount+$shipping_cost_price;
                                        }
                                    }

                                @endphp
                              <span  class=" order-total"><strong>{{number_format((float)$card_total_amount, 2, ',', '')}} ₺</strong></span>
                            </td>
                          </tr>
                        </div>
                        
                        </div>
                    </div>
                
                    <div class="card">
                        <div class="card-body">
                            <p><strong>Teslimat Bilgileri </strong></p>
                            <p><strong>Adres:</strong> {{$newadress}} <strong>{{$town_city}}</strong></p>
                            <p><strong>Telefon No: </strong>{{$userLimit['phone']}}</p>
                        </div>
                    </div>          
                </div>

            @else   
                <div class="col-lg-10">
                    <div class="style-msg alertmsg text-center">
                        <div class="sb-msg"><i class="icon-warning-sign"></i><strong>Uyarı!</strong> Sipariş Numarası Doğru Değil
                        </div>
                    </div>      
                </div>
            @endif
            <div class="col-lg-1"></div>
        </div>
    </div>

</body>

</html>
