@extends('layouts.app')

@section('content')
    <section id="content">

        <div class="content-wrap">

            
            @if (is_null($attr) || $menu->type == 'list-query')
                
                @if ($menu->type == 'content')
                    @include('types.menu.content')     
                @elseif ($menu->type == 'product') 
                    @include('types.menu.product')  
                @elseif ($menu->type == 'blog') 
                    @include('types.menu.blog')
                @elseif ($menu->type == 'playlist') 
                    @include('types.menu.playlist')        
                @elseif ($menu->type == 'menuitem')
                @elseif (starts_with($menu->type, 'module'))
                    @php
                        $modulename=explode("-",$menu->type);
                    @endphp
                    @include('types.'.$modulename[1].'.'.$modulename[1])
                @endif
                
            @else

                @php 
                    $content = App\Content::find(App\ContentVariable::where('slug', $attr)->first()->content_id); 
                    if (empty($content->variableLang($lang))) {
                        $contVariable = $content->variable;
                    }else{
                        $contVariable = $content->variableLang($lang);
                    }

                @endphp
                
                @if ($menu->asidevisible == 'yes')
                    <div class="container clearfix">
                        <div class="postcontent nobottommargin clearfix">
                @endif

                    <div class="section {{ 'light'/*$_props_colortheme*/ }}  notopmargin nobottommargin">
                        <div class="container clearfix">
                             
                               
                                @if (starts_with($content->type , 'group'))
                                    @include('types.menupartials.'.$content->type)
                                @elseif ($content->type == 'module-shop')
                                    @include('types.shop.'.$contVariable->content)
                                @elseif ($content->type == 'module-user')
                                    @include('types.user.'.$contVariable->content)       
                                @elseif ($content->type == 'photogallery')
                                    @include('types.menupartials.photogallery')
       
                                @elseif ($content->type == 'mapturkey')
                                    @include('types.menupartials.mapdetail')
                                @endif

                        </div>
                    </div>
                    
                @if ($menu->asidevisible == 'yes')
                            </div>
                       @include('types.sidebar.sidebar')
                    </div>
                @endif

            @endif

        </div>

    </section>
        
@endsection

@section('inline-scripts')
@php

@endphp

    @if (is_null($attr) || $menu->type == 'list-query')
        @foreach ($menu->topHasSubContent as $ths)
            @foreach($ths->subContent as $sub_ths)
                @if(!is_null($sub_ths->sub_id))
                    @php
                        $element = $sub_ths->thatElementForContent;

                    @endphp
                    @if(!is_null($element))
                        @if ($element->type == 'form')
                            @include('types.menupartials.complexcontents.formjs')
                        @endif

                        @if ($element->type == 'mapturkey')
                            @include('types.menupartials.complexcontents.mapturkeyjs')
                        @endif
                    @endif
                @endif
            @endforeach

        @endforeach
    @else

        @foreach ($content->contentThs as $ths)
            @foreach($ths->subContent as $sub_ths)
                @if(!is_null($sub_ths->sub_id))
                    @php
                        $element = $sub_ths->thatElementForContent;

                    @endphp
                    @if(!is_null($element))
                        @if ($element->type == 'form')
                            @include('types.menupartials.complexcontents.formjs')
                        @endif

                        @if ($element->type == 'mapturkey')
                            @include('types.menupartials.complexcontents.mapturkeyjs')
                        @endif
                    @endif
                @endif
            @endforeach
        @endforeach
    @endif
    
    <script>

        var map;

        function initMap() {
            if(typeof uluru != 'undefined'){
                map = new google.maps.Map(document.getElementById('map'), {
                    center: uluru,
                    zoom: 16,
                    styles: googleMapStyles
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    icon: '{{ asset('cimages/marker.png') }}',
                    map: map
                });
            }
        }

         $(document).ready(function(){

                $( "#logoutBtn" ).click(function() {
                    $('#logoutForm').submit();
                });

            });
        
    </script>

@endsection