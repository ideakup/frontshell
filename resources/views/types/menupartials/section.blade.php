@php

    $section_props_section=json_decode($ths->props)->section_props_section;
    $_props_colortheme = json_decode($ths->props)->props_colortheme;
    $container_class = json_decode($ths->props)->props_fullwidth;

    $section_solid='';

    if($container_class == 'container container-solid'){
        $section_solid='section_solid';
    }

    $_section_props_section=json_decode($ths->props)->section_props_section;
    if($_section_props_section=='container'){
       $_section_props_section= "";
    }
    $section_type = json_decode($contVariable->content)->type;
    

@endphp
<div class="section parallax {{$contVariable->slug}} {{ $_props_colortheme }} {{ $section_solid}} {{ $_section_props_section }} notopmargin" 
    @if(json_decode($ths->props)->bg_img == 'true')
        @if(!empty(json_decode($contVariable->content)->section_bg_img))
            style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ json_decode($contVariable->content)->section_bg_img }}');
             background-repeat: no-repeat ; background-size: cover;"
        @endif
    @endif
 > 
 <div class="{{ $container_class }} "> 
        @if(!empty(json_decode( $contVariable->content)->show_title) && (json_decode( $contVariable->content)->show_title != 'none'))
            <h6 class="theme-title theme-title-section" style="text-align:{{json_decode($contVariable->content)->show_title}}" >{{$contVariable->title}}</h6>
        @endif
        @if(starts_with($section_type,'carousel'))
            @php 
                $data_item = (explode("::",$section_type));
            @endphp
            <div class="owl-carousel  carousel-widget" {!!$data_item[1]!!}>
        @elseif($section_type == 'grid')
            <div class="row">
        @endif
             @php
                $contentThs = $content->subContentThs;
                if(!empty(Request::get('tag')) || !empty(Request::get('cat')) ){
                    $contentThs = $contentThs->whereIn('sub_id',$content_ids);
                }
                //dump($contentThs);
            @endphp
            @foreach ($contentThs as $ths)
                @php
                    if($ths->sub_model=='Content'){
                        $content = $ths->thatElementForContent;  
                    }elseif($ths->sub_model=='MenuSum'){
                         $content = $ths->thatElementForMenusum;  
                    }
                    
                    if (empty($content->variableLang($lang))) {
                        $contVariable = $content->variable;
                    }else{
                        $contVariable = $content->variableLang($lang);
                    }
                    $element=$content;
                    //dd($content);
                @endphp
                
                
                @if(starts_with($section_type,'carousel'))
                    @if($content->type != 'summary')
                      <div class="oc-item">
                    @endif  
                @elseif($section_type == 'grid')
                     @if($content->type != 'summary')
                        <div class=" {{ json_decode($ths->props)->props_colvalue }}">
                    @endif
                @endif
               

                    <!-- Basic Contents-->
                    @if ($content->type == 'text')
                        @include('types.menupartials.basiccontents.text')
                    @elseif ($content->type == 'title')
                        @include('types.menupartials.basiccontents.title')
                    @elseif ($content->type == 'photo')
                        @include('types.menupartials.basiccontents.photo')
                    @elseif ($content->type == 'link')
                        @include('types.menupartials.basiccontents.link')
                    @elseif ($content->type == 'video')
                        @include('types.menupartials.basiccontents.video')
                    @elseif ($content->type == 'seperator')
                        @include('types.menupartials.basiccontents.seperator')
                    @elseif ($content->type == 'code')
                        @include('types.menupartials.basiccontents.code')
                    @elseif ($content->type == 'social_media_button')
                        @include('types.menupartials.basiccontents.social_media')

                    <!-- Complex Contents-->   
                    @elseif ($content->type == 'photogallery')
                        @include('types.menupartials.complexcontents.photogallery')
                    @elseif ($content->type == 'slide')
                        @php $slideData = $content->slide; @endphp
                        @include('types.menupartials.complexcontents.slide')
                    @elseif ($content->type == 'form')
                        @include('types.menupartials.complexcontents.form')
                    @elseif ($content->type == 'mapturkey')
                        @include('types.menupartials.complexcontents.mapturkey')

                    <!-- Group Contents-->
                    @elseif (starts_with($content->type, 'group'))
                        @php
                            $groupname = explode("-",$content->type);
                            if (empty($content->variableLang($lang))) {
                                $contVariable = $content->variable;
                            }else{
                                $contVariable = $content->variableLang($lang);
                            }
                            $group_content=json_decode($contVariable->content);  
                           // dump($groupname) ; 
                        @endphp
                        @include('types.menupartials.groupcontents.'.$groupname[1])
                                        <!-- summary-->

                    <!-- summary-->   
                    @elseif ($content->type == 'summary')
                        @include('types.menupartials.summary')                        
                    @endif    
                  
                
                    @if($content->type != 'summary')
                        </div>
                    @endif 
            
            @endforeach
        </div>
     </div> 
  
</div>

