
@php
  $props_sidebar = json_decode($content->ContentThs->first()->props)->sidebar;
  if($props_sidebar == 'template'){
      $sidebar_side =json_decode($menu->topHasSub->first()->props)->sidebar;
      $sidebar_id = $menu->id;
  }
  elseif($props_sidebar == 'uppermenu'){
      $uppermenu_ths=$content->contentThs->first()->topElementForContent->contentThs->first()->topElementForMenu->topHasSub->first();

      $sidebar_side =json_decode($uppermenu_ths->props)->sidebar;
      $sidebar_id = $uppermenu_ths->sub_id;
  }
  else{
      $sidebar_side = $props_sidebar;
      $sidebar_id = $content->id;
  }

@endphp
<div class="row">
@if ($sidebar_side != 'none') 
    <div class="postcontent col-lg-9 
    @if($sidebar_side == 'left') 
        order-lg-last
    @endif    
    ">      
@endif

<div class="row">
    @if($content->variable->title != "")
      <div class="fancy-title title-bottom-border">
        <h2>
         @php
            //dump($menuformodule);
            $shop_flag=$menuformodule->where('type',"module-shop")->count();
            if (empty($content->variableLang($lang))) {
                print_r($content->variable->title);
                $contVariable = $content->variable;

            }else{
                print_r($content->variableLang($lang)->title);
                $contVariable = $content->variableLang($lang);
            }
            $group_content=json_decode($contVariable->content);
            $single_cart = array();

            foreach ($content->subContentThsforproduct as $key ) {
                   array_push($single_cart,$key->thatElementForContent->variable);
            }
          @endphp
        </h2>
      </div>
    @endif
    <div class="col-lg-12">
            <div class="single-product">

              <div class="product">
                <div class="row">

                  <div class="col-lg-4">

                      <!-- Product Single - Gallery
                      ============================================= -->
                      <div class="product-image">
                          <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                              <div class="flexslider">
                                  <div class="slider-wrap" data-lightbox="gallery">
                                        @foreach ($single_cart[0]->contentdata->photogallery($lang) as $image)
                                          <div class="slide" data-thumb="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $image->url) }}"><a href="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $image->url) }}" data-lightbox="gallery-item"><img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $image->url) }}"></a>
                                          </div>
                                        @endforeach
                                  </div>
                              </div>
                          </div>
                         <!-- <div class="sale-flash"></div>-->
                      </div><!-- Product Single - Gallery End -->

                  </div>

                  <div class="col-lg-8">

                      <!-- Product Single - Price
                      ============================================= -->
                    <div class="product-price">
                        @if(!($group_content->stock=='yok'))
                            @if(!empty($group_content->discounted_price) && !empty($group_content->price))
                                <div class="float-lg-start text-center">
                                    <del>{{number_format((float)$group_content->price+($group_content->price*$group_content->tax_rate/100), 2, '.', '')}} ₺</del>
                                    <ins>{{number_format((float)$group_content->discounted_price+($group_content->discounted_price*$group_content->tax_rate/100), 2, '.', '')}} ₺</ins>
                                </div>
                            @else
                                @if(!empty($group_content->price))  
                                    <div class="float-lg-start text-center">
                                        <ins>{{number_format((float)$group_content->price+($group_content->price*$group_content->tax_rate/100), 2, '.', '')}} ₺</ins>
                                    </div>
                                @endif
                            @endif
                        @endif
                        <div class="product-rating float-lg-end text-center">
                            <i class="icon-star3"></i>
                            <i class="icon-star3"></i>
                            <i class="icon-star3"></i>
                            <i class="icon-star3"></i>
                            <i class="icon-star-half-full"></i>
                        </div>
                    </div> 
                      
                      <br><hr>
              

                      @if($shop_flag==1)
                          <!-- Product Single - Quantity & Cart Button
                          ============================================= -->
                        @if($group_content->stock=='var') 
           
                          <form id="Shoppingcartadd">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="row ">
                              <div class="col-lg-4 col-md-12">
                                <div class="quantity clearfix">
                                  <input type="button" value="-" class="minus">
                                  <input type="text" step="1" min="1"  id="quantity" value="1" title="Qty" class="qty" size="4" />
                                  <input type="button" value="+" class="plus">
                                </div>
                              </div>
                              <div class="col-lg-8 col-md-12">
                                <div class="text-end">
                                    <a href="#" data-slug="{{$contVariable->slug}}" class="add-to-cart button nomargin mb-lg-0"><i class="icon-shopping-cart"></i><span> Sepete Ekle</span></a>
                                    <a href="#" data-slug="{{$contVariable->slug}}" class="buy-now button nomargin mb-0"><i class="fas fa-shopping-bag"></i><span> Hemen Al</span></a>
                                </div>
                              </div>
                            </div>
                            
                            
                          </form><!-- Product Single - Quantity & Cart Button End -->
                          <hr>

                        @endif
                          
                      @endif    

                      <!-- Product Single - Short Description
                      ============================================= -->
                      @if(!empty(json_decode($single_cart[1]->content)->html_text))
                        {!!json_decode($single_cart[1]->content)->html_text!!}
                      @endif

                  </div>
                </div>  
              </div>
            </div>
    </div>

    @foreach ($content->subContentThs as $ths)
      @php
            if($ths->sub_model=='Content'){
                $content = $ths->thatElementForContent;  
            }elseif($ths->sub_model=='MenuSum'){
                 $content = $ths->thatElementForMenusum;  
            }
            
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
            $element=$content;
            //dd($content);
        @endphp
      
        @if ($content->type == 'text')
            @include('types.menupartials.basiccontents.text')
        @elseif ($content->type == 'title')
            @include('types.menupartials.basiccontents.title')
        @elseif ($content->type == 'photo')
            @include('types.menupartials.basiccontents.photo')
        @elseif ($content->type == 'link')
            @include('types.menupartials.basiccontents.link')
        @elseif ($content->type == 'video')
            @include('types.menupartials.basiccontents.video')
        @elseif ($content->type == 'seperator')
            @include('types.menupartials.basiccontents.seperator')
        @elseif ($content->type == 'code')
            @include('types.menupartials.basiccontents.code')

        <!-- Complex Contents-->   
        @elseif ($content->type == 'photogallery')
            @include('types.menupartials.complexcontents.photogallery')
        @elseif ($content->type == 'slide')
            @php $slideData = $content->slide; @endphp
            @include('types.menupartials.complexcontents.slide')
        @elseif ($content->type == 'form')
            @include('types.menupartials.complexcontents.form')
        @elseif ($content->type == 'mapturkey')
            @include('types.menupartials.complexcontents.mapturkey')

        <!-- Group Contents-->
        @elseif (starts_with($content->type, 'group'))
            @php
                $groupname = explode("-",$content->type);
                if (empty($content->variableLang($lang))) {
                    $contVariable = $content->variable;
                }else{
                    $contVariable = $content->variableLang($lang);
                }
                $group_content=json_decode($contVariable->content);  
               // dump($groupname) ; 
            @endphp
            @include('types.menupartials.groupcontents.'.$groupname[1])
        @endif
    @endforeach
</div>
@if ($sidebar_side != 'none') 

        </div>
            @include('types.sidebar.sidebar')     
    </div>
@endif

        

