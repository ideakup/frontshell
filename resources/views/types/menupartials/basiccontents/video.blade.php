	@if(!empty(json_decode($contVariable->content)->photo) && json_decode($contVariable->content)->toogle == 'on')
		<a class="grid-item" href="https://www.youtube.com/watch?v={{json_decode($contVariable->content)->embed_code}}" data-lightbox="iframe">
			<div class="grid-inner">
				<img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{json_decode( $contVariable->content)->photo }}" alt="Youtube Video">
				<div class="bg-overlay">
					<div class="bg-overlay-content dark">
						<i class="icon-youtube-play h4 mb-0" data-hover-animate="fadeIn"></i>
					</div>
					<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
				</div>
			</div>
		</a>
	@else
		<div class="ws-video-container">
			<iframe class="ws-video-content" allowFullScreen="allowFullScreen" src="https://www.youtube.com/embed/{{json_decode($contVariable->content)->embed_code}}?modestbranding=1&showinfo=0&fs=1&rel=0"></iframe>
		</div>
	@endif