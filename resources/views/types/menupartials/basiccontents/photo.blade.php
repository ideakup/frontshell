<div class="feature-box media-box">
	<div class="fbox-media">
		@if(!empty(json_decode( $contVariable->content)->photo))
			<a @if(!empty(json_decode( $contVariable->content)->link)) href="{{json_decode( $contVariable->content)->link}}" target="_blank" @endif>
				<img class="{{ str_slug($contVariable->title,'-') }}" alt="{{$contVariable->title}}" src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{json_decode( $contVariable->content)->photo }}"/>
			</a>
		@endif
	</div>
	@if(!empty(json_decode( $contVariable->content)->toogle_title) && (json_decode( $contVariable->content)->toogle_title == 'on') || !empty(json_decode( $contVariable->content)->short_content))
		<div class="fbox-content px-0">
			@if(!empty(json_decode( $contVariable->content)->toogle_title) && (json_decode( $contVariable->content)->toogle_title == 'on'))
				<h3>
					<a @if(!empty(json_decode( $contVariable->content)->link)) href="{{json_decode( $contVariable->content)->link}}" target="_blank" @endif> {{$contVariable->title}}
					</a>
				</h3>
			@endif
			@if(!empty(json_decode( $contVariable->content)->short_content))
				<p>{{json_decode( $contVariable->content)->short_content}}</p>
			@endif
		</div>
	@endif
</div>