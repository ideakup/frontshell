@if((!empty(json_decode($contVariable->content)->link)))
	<a class="button button-rounded button-reveal button-border linkBtn" href="{!! json_decode($content->variableLang($lang)->content)->link !!}" target="{{ (json_decode($content->variableLang($lang)->content)->toogle == 'on') ? '_blank' : '_self' }}">
		<i class="icon-line-link"></i><span>{{ $contVariable->title }}</span>
	</a>
@endif
