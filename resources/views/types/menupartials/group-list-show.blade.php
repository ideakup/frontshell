@php
	if(!empty(Request::segment(3))){
		
		$eventSession=App\EventSession::where('eventsession_date',Request::segment(3))->first();
		$eventSessionths=$eventSession->topHasSub;

		$Event=$eventSessionths->topElementForContent;
		$EventVariable=$Event->variable;
		//dd($EventVariable);
	}
	else{
		$EventVariable=App\ContentVariable::where('slug',Request::segment(2))->first();
		$Event=$EventVariable->contentdata;

		$eventSessionths=$Event->subEventSessionThs->sortBy('eventsession_date')->first();
		$eventSession=$eventSessionths->eventSession;

	}
		$eventSessionPrice=json_decode($eventSession->price);
		$group_content=json_decode($EventVariable->content);

		$aylar = array(1=>"Ocak",2=>"Şubat",3=>"Mart",4=>"Nisan",5=>"Mayıs",6=>"Haziran",7=>"Temmuz",8=>"Ağustos",9=>"Eylül",10=>"Ekim",11=>"Kasım",12=>"Aralık");
		$kisaaylar = array(1=>"Oca",2=>"Şub",3=>"Mar",4=>"Nis",5=>"May",6=>"Haz",7=>"Tem",8=>"Ağu",9=>"Eyl",10=>"Eki",11=>"Kas",12=>"Ara");
		$today=\Carbon\Carbon::now();
		$dt = \Carbon\Carbon::parse($eventSession->eventsession_date);
		$diff = $dt->diff($today);
	
@endphp




				<div class="container">

					<div class="postcontent nobottommargin clearfix">

						<div class="single-event">

							<div class="col_three_fourth">
								<div class="entry-image nobottommargin">
									<img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $group_content->photo) }}" />								
								</div>
							</div>
							<div class="col_one_fourth col_last">
								<div class="card events-meta mb-3">
									<div class="card-header"><h5 class="mb-0">Etkinlik Bilgileri:</h5></div>
									<div class="card-body">
										<ul class="iconlist nobottommargin">
											<li><i class="icon-calendar3"></i> {{$dt->day}} {{$aylar[$dt->month]}} {{$dt->year}}</li>
											<li><i class="icon-time"></i> {{$dt->format('H:i')}}</li>
											<li><i class="icon-map-marker2"></i> <span class="badge badge-warning">{{$eventSession->show_type}}</span></li>
											<li>
												<i class="icon-money-bill-wave"></i> 
												<strong>
													 @if(!empty($eventSessionPrice->discounted_price) && !empty($eventSessionPrice->price))
									                      <del>{{number_format((float)$eventSessionPrice->price+($eventSessionPrice->price*$eventSessionPrice->tax_rate/100), 2, '.', '')}} ₺</del>
									                      <ins>{{number_format((float)$eventSessionPrice->discounted_price+($eventSessionPrice->discounted_price*$eventSessionPrice->tax_rate/100), 2, '.', '')}} ₺</ins>
									                   @else
									                      @if(!empty($eventSessionPrice->price))  
									                          <ins>{{number_format((float)$eventSessionPrice->price+($eventSessionPrice->price*$eventSessionPrice->tax_rate/100), 2, '.', '')}} ₺</ins>
									                      @endif
									                   @endif
												</strong>
											</li>
										</ul>
									</div>
								</div>
								@if($eventSession->sale_status=='on_sale')
									<a href="#" class="btn bilet-al btn-block btn-lg">Bilet Al</a>
								@else
									<a href="/oyunlar/{{$EventVariable->slug}}" class="btn btn-warning btn-block btn-lg">Oyun Hakkında</a>
								@endif
							</div>

							<div class="clear"></div>

							<div class="col_three_fourth">

								<h3>Detaylar</h3>

								<p>{!!$group_content->short_content!!}</p>

								

						

							</div>

			



						</div>

					</div>

					<div class="sidebar nobottommargin col_last clearfix">
						<div class="sidebar-widgets-wrap">

							<div class="widget clearfix">

								<h4>Bu oyun için diğer gösterimler</h4>
								<div id="post-list-footer">
									@foreach($Event->subEventSessionThs as $event_ths )
										@php
										$shows_variable=$event_ths->eventSession;
										$eventsessionslug=$shows_variable->eventsession_date;
										$updt = \Carbon\Carbon::parse($shows_variable->eventsession_date);	
										@endphp
										<div class="spost clearfix">
											<div class="entry-image">
												<a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/{{$eventsessionslug}}" class="nobg"><img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $group_content->photo) }}" /></a>
											</div>
											<div class="entry-c">
												<div class="entry-title">
													<h4><a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/{{$eventsessionslug}}">{{$EventVariable->title}}</a></h4>
												</div>

												<ul class="entry-meta">
													{{$updt->day}} {{$aylar[$updt->month]}} {{$updt->year}}
												</ul>
											</div>
										</div>
									@endforeach	

									

									

								</div>

							</div>

						</div>
					</div>
				
				

				</div>
			

