@if(!empty($content->subContentThs[0]))
	@php
		if(empty(Request::segment(3))){
			$CurrentCont=$content->subContentThs[0]->thatElementForContent;
			$CurrentContVarianle=$CurrentCont->variable;	
		}
		else{
			$video_slug=Request::segment(3);
			$CurrentContVarianle=$allcontent->where('slug',$video_slug)->first();
			if(empty($CurrentContVarianle)){
				$CurrentCont=$content->subContentThs[0]->thatElementForContent;
				$CurrentContVarianle=$CurrentCont->variable;	
			}
			else{
				$CurrentContVarianle=$allcontent->where('slug',$video_slug)->first();
				$CurrentCont=$CurrentContVarianle->contentdata;
			}
		}

	@endphp	

	<div class="row">
		<div class="col-lg-8">
			<div class="ws-video-container">
				<iframe class="ws-video-content" allowFullScreen="allowFullScreen" src="https://www.youtube.com/embed/{{json_decode($CurrentContVarianle->content)->embed_code}}?modestbranding=1&showinfo=0&fs=1&rel=0"></iframe>
			</div>
			<div class="video-title">
				<h2>{{$CurrentContVarianle->title}}</h2>
			</div>
			<div class="video-content">
				<p>{{json_decode($CurrentContVarianle->content)->short_content}}</p>
			</div>
		</div>

		<div class="col-lg-4">
			<h4>{{$content->variable->title}}</h4>
			
			<div class="posts-sm row col-mb-30 ws-playlist" id="last-viewed-shop-list-sidebar">
				@foreach ($content->subContentThs as $ths)
			    	@php
			            $content = $ths->thatElementForContent;
			           //dd($content->variable);
			            if (empty($content->variableLang($lang))) {
			                $contVariable = $content->variable;
			            }else{
			                $contVariable = $content->variableLang($lang);
			            }
			
			        @endphp	
					<div class="entry col-12">
						<div class="grid-inner row g-0">
							<div class="col-auto">
								<div class="entry-image">
									<a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/{{$contVariable->slug}}" class="nobg"><img  src="https://i.ytimg.com/vi/{{json_decode($contVariable->content)->embed_code}}/hqdefault.jpg" alt=""></a>
								</div>
							</div>
							<div class="col ps-3">
								<div class="entry-title">
									<a href="/{{Request::segment(1)}}/{{Request::segment(2)}}/{{$contVariable->slug}}" class="nobg"><h4 @if(Request::segment(3)==$contVariable->slug)style="color: #eb1b2b" @endif>{{$contVariable->title}}</h4></a>
								</div>
								<div class="entry-meta no-separator">
									<ul>
										@if(!empty(json_decode($contVariable->content)->short_content))
											<li>{{json_decode($contVariable->content)->short_content}}</li>
										@endif
									</ul>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			
		</div>
	</div>
	
	<div class="row">
		@foreach ($CurrentCont->subContentThs as $ths)
            @php
                if($ths->sub_model=='Content'){
                    $content = $ths->thatElementForContent;  
                }elseif($ths->sub_model=='MenuSum'){
                     $content = $ths->thatElementForMenusum;  
                }
                
                if (empty($content->variableLang($lang))) {
                    $contVariable = $content->variable;
                }else{
                    $contVariable = $content->variableLang($lang);
                }
                $element=$content;
                //dd($content);
            @endphp
            
            
         
            @if($content->type != 'summary')
                <div class="{{ json_decode($ths->props)->props_colvalue }}">
            @endif
           
           

                <!-- Basic Contents-->
                @if ($content->type == 'text')
                    @include('types.menupartials.basiccontents.text')
                @elseif ($content->type == 'title')
                    @include('types.menupartials.basiccontents.title')
                @elseif ($content->type == 'photo')
                    @include('types.menupartials.basiccontents.photo')
                @elseif ($content->type == 'link')
                    @include('types.menupartials.basiccontents.link')
                @elseif ($content->type == 'video')
                    @include('types.menupartials.basiccontents.video')
                @elseif ($content->type == 'seperator')
                    @include('types.menupartials.basiccontents.seperator')
                @elseif ($content->type == 'code')
                    @include('types.menupartials.basiccontents.code')
                @elseif ($content->type == 'social_media_button')
                    @include('types.menupartials.basiccontents.social_media')

                <!-- Complex Contents-->   
                @elseif ($content->type == 'photogallery')
                    @include('types.menupartials.complexcontents.photogallery')
                @elseif ($content->type == 'slide')
                    @php $slideData = $content->slide; @endphp
                    @include('types.menupartials.complexcontents.slide')
                @elseif ($content->type == 'form')
                    @include('types.menupartials.complexcontents.form')
                @elseif ($content->type == 'mapturkey')
                    @include('types.menupartials.complexcontents.mapturkey')

                <!-- Group Contents-->
                @elseif (starts_with($content->type, 'group'))
                    @php
                        $groupname = explode("-",$content->type);
                        if (empty($content->variableLang($lang))) {
                            $contVariable = $content->variable;
                        }else{
                            $contVariable = $content->variableLang($lang);
                        }
                        $group_content=json_decode($contVariable->content);  
                       // dump($groupname) ; 
                    @endphp
                    @include('types.menupartials.groupcontents.'.$groupname[1])
                                    <!-- summary-->
                @elseif ($content->type == 'summary')
                    @include('types.menupartials.summary')
                <!-- tournament-->
                @elseif ($content->type == 'tournament-widgets')
                    @include('types.menupartials.tournament.widgets')
                @elseif ($content->type == 'tournament-schedule')
                    @include('types.menupartials.tournament.schedule')
                @elseif ($content->type == 'tournament-schedule-elimination')
                    @include('types.menupartials.tournament.schedule-elimination')
                @elseif ($content->type == 'tournament-standing')
                    @include('types.menupartials.tournament.standing')
                @elseif ($content->type == 'tournament-live-matches')
                    @include('types.menupartials.tournament.live-matches')
                @elseif ($content->type == 'tournament-bracket')
                    @include('types.menupartials.tournament.bracket')
                @elseif ($content->type == 'tournament-counter')
                    @include('types.menupartials.tournament.counter')
                @elseif ($content->type == 'tournament-scorer')
                    @include('types.menupartials.tournament.scorer')
                @elseif ($content->type == 'tournament-card')
                    @include('types.menupartials.tournament.card')
                @endif    
              
            
                @if($content->type != 'summary')
                    </div>
                @endif 
        
        @endforeach
	</div>
						

@endif