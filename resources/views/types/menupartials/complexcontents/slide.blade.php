@php
$id_name=explode(" ",$contVariable->title);
//title-açıklama-button 
@endphp
<!-- OK -->
    <section id="slider" class="slider-element boxed-slider {{implode("-",$id_name)}} ">
            <div class="fslider" data-animation="fade">
                <div class="flexslider">
                    <div class="slider-wrap">
                        @foreach ($slideData as $sElement)
                            @if (!is_null($sElement->image_url))
                                <div class="slide" data-thumb="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $sElement->image_url }}">
                                    <a href="#" class="d-block position-relative">
                                        <img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $sElement->image_url }}" alt="Slide 2">
                                        <!--
                                        <div class="bg-overlay">
                                            <div class="bg-overlay-content justify-content-start align-items-end">
                                                <div class="h3 fw-light py-2 px-3 bg-light text-dark ms-3 mb-3 rounded"></div>
                                            </div>
                                        </div>
                                        -->
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
    </section>




