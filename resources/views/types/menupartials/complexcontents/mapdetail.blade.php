<!-- OK -->

		@php

			if($param == 'bolge'){
				//dump($param.' - '.$param2);
				$districtVariable = App\DistrictVariable::where('slug', $param2)->first();
				$district = $districtVariable->district;
				//dump($district);
				$staffs = App\Staff::where('district_id', $district->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $districtVariable->name;
			}else if($param == 'il'){
				//dump($param.' - '.$param2);
				$cityVariable = App\CityVariable::where('slug', $param2)->first();
				$city = $cityVariable->city;
				//dump($city);
				$staffs = App\Staff::where('city_id', $city->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $cityVariable->name;
			}else if($param == 'ildetay'){
				//dump($param.' - '.$param2);
				$cityVariable = App\CityVariable::where('slug', $param2)->first();
				$city = $cityVariable->city;
				//dump($city);
				$staffs = App\Staff::where('city_id', $city->id)->where('county_id', NULL)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $cityVariable->name;
			}else if($param == 'ilcedetay'){
				//dump($param.' - '.$param2);
				$countyVariable = App\CountyVariable::where('slug', $param2)->first();
				$county = $countyVariable->county;
				//dump($county);
				$staffs = App\Staff::where('county_id', $county->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $countyVariable->name;
			}

		@endphp
		<div class="container clearfix">
			<div class="col-lg-12">
				<div class="fancy-title title-dotted-border title-center" style="padding-bottom: 30px;">
					<h1>{{ $pageTitle }}</h1>
				</div>
			</div>
			

			<div" class="col-lg-12">

				@include('types.menupartials.mapturkey')
			</div>	
			<div style="margin-top: 80px;" class=" col-lg-10 offset-lg-1 ">
				@php
					$__cityid = '';
					$__countyid = '';
					//dd($staffs[0]->city->variable);
				@endphp
				@foreach($staffs as $staff)
					<div class="team team-list clearfix ">
	                    <div class="team-image staff-image">
	                        @if (empty($staff->variable->photo_url))
	                            <img src="{{ url('logos/logo-light-tr.png') }}" />
	                        @else
	                            <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$staff->variable->photo_url) }}" />
	                        @endif
	                    </div>
	                    <div class="team-desc">
	                        <div class="team-title">
	                            <h3>{{ $staff->variable->name }}</h3>
	                            <span>{{$staff->city->variable->name}} @if(!empty($staff->county)) {{$staff->county->variable->name}} @endif</span>
	                        </div>
	                        <div class="team-content">
								<p><strong>TEL: </strong>{{$staff->variable->phone}}</p>
								<p><strong>ADRES: </strong>{{$staff->variable->address}}</p>
	                        </div>
	                    </div>
	                </div>
	                <div class="divider divider-rounded divider-center"><i class="icon-map-marker"></i></div>
				@endforeach
		

			</div>
		</div>




		<div class="clear"></div>

