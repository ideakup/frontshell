@php
//dd($element);
@endphp
<script>
    $(document).ready(function(){

        $('#form_element_{{ $element->id }}').formRender({
            dataType: 'json',
            formData: formData_{{ $element->id }},
            layoutTemplates: {
                default: function(field, label, help, data) {
                    return $('<div/>').addClass('col_full').append(label, field, help);
                },
                /*
                    noLabel: function(field, label, help, data){
                        console.log('--noLabel--');
                        console.log(field);
                        console.log(label);
                        console.log(help);
                        console.log(data);
                        console.log('-----------');
                        return false;
                    },
                    hidden: function(field){
                        console.log('--hidden--');
                        console.log(field);
                        console.log('-----------');
                        return false;
                    },
                */
                label: function(label, data) {
                    return $('<label/>').attr('for', data.id).append(label);
                },
                help: function(helpText, data) {
                    return false; //$('<div/>').addClass('help').append(helpText);
                }
            }
        });

        
        $('#form_submit_{{ $element->id }}').click(function(e) {
            e.preventDefault();
            var btn = $("#form_submit_{{ $element->id }}");
            var form = $("#form_{{ $element->id }}");
            //btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });

    });
</script>