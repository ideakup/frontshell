<!-- OK -->
@php 
	if(empty($ths)){
		$ths = $content->contentThsadvance($menu->id)->first();
	}
@endphp

	@php
		$_colValue = '3';
		$c_v = json_decode($ths->props)->props_type;//propstype c_v değişkenine atandı.
		$display_number=json_decode($ths->props)->display_number;//propstype c_v değişkenine atandı.
		$href_link =$menu->variable->slug. "/".$contVariable->slug;
	@endphp
	@if ($c_v == 'col-2x')
		@php $_colValue = '2';@endphp
	@elseif($c_v == 'col-3x')
		@php $_colValue = '3';@endphp
	@elseif($c_v == 'col-4x')
		@php $_colValue = '4';@endphp
	@elseif($c_v == 'col-5x')
		@php $_colValue = '5';@endphp
	@elseif($c_v == 'col-6x')
		@php $_colValue = '6';@endphp		
	@endif
	
	@if(json_decode($ths->props)->display_type == 'horizontal')
		<div class="masonry-thumbs grid-container grid-{{ $_colValue }}"  data-lightbox="gallery">
			@foreach ($contVariable->contentdata->photogallery($lang) as $image)
				<a class="grid-item" href="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $image->url }}" data-lightbox="gallery-item" @if($loop->index > $display_number - 1) style="display:none;" @endif >
					<img src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $image->url }}" alt="{{ $image->name }}" />
				</a>
			@endforeach
		</div>
	@elseif(json_decode($ths->props)->display_type == 'album')
		<div class="feature-box media-box">
			<div class="fbox-media">
				<div class="fslider" data-arrows="false">
					<div class="flexslider">
						<div class="slider-wrap">
							@foreach($contVariable->contentdata->photogallery($lang)->splice(2) as $image)
								<div class="slide">
									<a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $image->url }}" alt="{{ $image->name }}" /></a>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="fbox-content px-0">
				<h3><a href="{{$href_link}}">{{$contVariable->title}}</a></h3>
			</div>
		</div>	
	@else
		@foreach ($contVariable->contentdata->photogallery($lang) as $image)
			<img class="vertical_photos" src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $image->url }}"/>
		@endforeach
	@endif		
