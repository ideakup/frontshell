<script>
    $(document).ready(function(){

        $('#sehirselect').select2({
            placeholder: "Seçiniz..."
        });
        $('#bolgeselect').select2({
            placeholder: "Seçiniz..."
        });

        $('#sehirselect').on('select2:select', function (e) {
            console.log(this.value);
            window.location.href = this.value;
        });

        $('#bolgeselect').on('select2:select', function (e) {
            console.log(this.value);
            window.location.href = this.value;
        });

        am4core.ready(function() {

            am4core.useTheme(am4themes_animated);

            var chart = am4core.create("chartdiv", am4maps.MapChart);
            chart.geodata = am4geodata_turkeyLow;
            chart.projection = new am4maps.projections.Miller();

            chart.zoomControl = new am4maps.ZoomControl();
            chart.zoomControl.valign = "top";

            var homeButton = new am4core.Button();
            homeButton.events.on("hit", function(){
                chart.goHome();
            });

            homeButton.icon = new am4core.Sprite();
            homeButton.padding(7, 5, 7, 5);
            homeButton.width = 30;
            homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
            homeButton.marginBottom = 10;
            homeButton.parent = chart.zoomControl;
            homeButton.insertBefore(chart.zoomControl.plusButton);
            
            var onlyCityPolygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
            onlyCityPolygonSeries.useGeodata = true;

            var onlyCityPolygonTemplate = onlyCityPolygonSeries.mapPolygons.template;
           // onlyCityPolygonTemplate.fill = am4core.color('#232062');//chart.colors.getIndex(0);
           // onlyCityPolygonTemplate.stroke = am4core.color('#888888');

            chart.smallMap = new am4maps.SmallMap();
            chart.smallMap.align = "right";
            chart.smallMap.valign = "bottom";
            chart.smallMap.series.push(onlyCityPolygonSeries);
            

            var excludedCities = [];
            var lastSelected = new Array();
            var lastSelectedDPS = new Array();

            bolgeler.forEach(function(group) {
                
                var districtPolygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                districtPolygonSeries.name = group.name;
                districtPolygonSeries.useGeodata = true;

                districtPolygonSeries.tooltip.autoTextColor = false;
                districtPolygonSeries.tooltip.label.fill = am4core.color('#333333');
                districtPolygonSeries.tooltip.getFillFromObject = false;
                districtPolygonSeries.tooltip.background.fill = am4core.color('#f2da97');

                var includedCities = [];

                group.data.forEach(function(city) {
                    includedCities.push(city.id);
                    excludedCities.push(city.id);
                });

                districtPolygonSeries.include = includedCities;
              //  districtPolygonSeries.fill = am4core.color('#FF0000');//group.color

                districtPolygonSeries.setStateOnChildren = true;
                districtPolygonSeries.calculateVisualCenter = true;

                var districtMapPolygonTemplate = districtPolygonSeries.mapPolygons.template; 
               // districtMapPolygonTemplate.fill = am4core.color('#333333');
                districtMapPolygonTemplate.fillOpacity = 0;
               // districtMapPolygonTemplate.stroke = am4core.color('#888888');
                districtMapPolygonTemplate.strokeOpacity = 1;
                districtMapPolygonTemplate.nonScalingStroke = true;
                districtMapPolygonTemplate.tooltipPosition = "pointer";

                districtMapPolygonTemplate.events.on("over", function(event) {
                    districtPolygonSeries.mapPolygons.each(function(mapPolygon) {
                        mapPolygon.isHover = true;
                    })
                    event.target.isHover = false;
                    event.target.isHover = true;
                })

                districtMapPolygonTemplate.events.on("out", function(event) {
                    districtPolygonSeries.mapPolygons.each(function(mapPolygon) {
                        mapPolygon.isHover = false;
                    })
                })
                
                districtMapPolygonTemplate.events.on("hit", function(event) {

                    if(event.target.isActive){
                        $(location).attr('href', event.target.dataItem.dataContext.clickUrl);
                    }else{
                        for(var j = 0; j < lastSelected.length; j++) {
                            //lastSelected[j].isActive = false;
                            lastSelectedDPS.getPolygonById(lastSelected[j]).isActive = false;
                        }

                        var zoomTo = new Array();
                        districtPolygonSeries.mapPolygons.each(function(mapPolygon) {
                            zoomTo.push(mapPolygon._dataItem._dataContext.id);
                        })
                        var north, south, west, east;
                        for(var i = 0; i < zoomTo.length; i++) {
                            var country = districtPolygonSeries.getPolygonById(zoomTo[i]);
                            if (north == undefined || (country.north > north)) {
                                north = country.north;
                            }
                            if (south == undefined || (country.south < south)) {
                                south = country.south;
                            }
                            if (west == undefined || (country.west < west)) {
                                west = country.west;
                            }
                            if (east == undefined || (country.east > east)) {
                                east = country.east;
                            }
                            country.isActive = true;
                        }
                        chart.zoomToRectangle(north, east, south, west, 1, true);

                        lastSelected = zoomTo;
                        lastSelectedDPS = districtPolygonSeries;
                    }
                })
                
                var ss = districtMapPolygonTemplate.states.create("active");
                //ss.properties.fill = am4core.color('#f2da97');
                ss.properties.fillOpacity = 1;

                var hoverState = districtMapPolygonTemplate.states.create("hover");
               // hoverState.properties.fill = am4core.color('#232062');
                hoverState.properties.fillOpacity = 1;

                districtMapPolygonTemplate.tooltipHTML = "<center>{name} Bayi"+((group.name == '') ? '' : '<br>'+group.name)+" </center>";
                districtPolygonSeries.data = JSON.parse(JSON.stringify(group.data));
                
            });
            
            var districtSeries = chart.series.push(new am4maps.MapPolygonSeries());
            var districtSeriesName = "Türkiye";
            districtSeries.name = districtSeriesName;
            districtSeries.useGeodata = true;
            districtSeries.exclude = excludedCities;
            districtSeries.fillOpacity = 0.8;
            districtSeries.hiddenInLegend = false;
            districtSeries.mapPolygons.template.nonScalingStroke = true;
            
            // Image series
            var imageSeries2 = chart.series.push(new am4maps.MapImageSeries());
            var imageSeriesTemplate2 = imageSeries2.mapImages.template;
            var marker2 = imageSeriesTemplate2.createChild(am4core.Image);
            marker2.href = "{{ url('cimages/icon.png') }}";
            marker2.width = 8;
            marker2.height = 8;
            marker2.nonScaling = true;
            marker2.tooltipText = "{title}";
            marker2.horizontalCenter = "middle";
            marker2.verticalCenter = "middle";

            imageSeriesTemplate2.propertyFields.latitude = "latitude";
            imageSeriesTemplate2.propertyFields.longitude = "longitude";
            imageSeries2.data = marker_county_data;

            imageSeriesTemplate2.events.on("hit", function(event) {
                $(location).attr('href', event.target.dataItem.dataContext.clickUrl);
            });
            
            // Image series
            var imageSeries = chart.series.push(new am4maps.MapImageSeries());
            var imageSeriesTemplate = imageSeries.mapImages.template;
            var marker = imageSeriesTemplate.createChild(am4core.Image);
            marker.href = "{{ url('cimages/icon.png') }}";
            marker.width = 8;
            marker.height = 8;
            marker.nonScaling = true;
            marker.tooltipText = "{title}";
            marker.horizontalCenter = "middle";
            marker.verticalCenter = "middle";

            imageSeriesTemplate.propertyFields.latitude = "latitude";
            imageSeriesTemplate.propertyFields.longitude = "longitude";
            imageSeries.data = marker_city_data;

            imageSeriesTemplate.events.on("hit", function(event) {
                $(location).attr('href', event.target.dataItem.dataContext.clickUrl);
            });

        }); // end am4core.ready()
    });
</script>