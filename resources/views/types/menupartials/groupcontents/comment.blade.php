<div class="testimonial">
	<div class="testi-image">
		<img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $group_content->photo }}">
	</div>
	<div class="testi-content">
		@if(!empty($group_content->comment))
			{!! $group_content->comment !!}
		@endif
		<div class="testi-meta">
			@if(!empty($group_content->name_surname))
				{{$group_content->name_surname}}
			@endif
			@if(!empty($group_content->title))
				<span>{{$group_content->title}}</span>
			@endif
		</div>
	</div>
</div>