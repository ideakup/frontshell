@php
if(!empty(json_decode($contVariable->content)->periodandprice)){
	$periodandprice = collect(json_decode($contVariable->content)->periodandprice);
	//dd($periodandprice->search($periodandprice->first()));
	$permonth = ($periodandprice->first())/($periodandprice->search($periodandprice->first()));

}
@endphp

@if(!empty(json_decode($contVariable->content)->periodandprice))

	<div class="pricing-box text-center">
		<div class="pricing-title text-uppercase title-sm">
			<h3>{{$contVariable->title}}  </h3>
	    <h6>( {{$periodandprice->search($periodandprice->first())}} month )</h6>
		</div>
		<div class="pricing-price">
			<span class="price-unit">₺</span>{{number_format((float)$permonth, 2, ',', '')}}<span class="price-tenure">/month</span>
		</div>
		<div class="pricing-features">
			@if(!empty($group_content->content ))
				{!! $group_content->content !!}
			@endif
		</div>
		<div class="pricing-action px-4">
			@if($menuformodule->where('type',"module-shop")->count() > 0 )
				@if(Auth::check())
					<a data-slug="{{$contVariable->slug}}" href="#" class="btn btn-danger w-100 btn-lg subscribe ">Abone Ol</a>
				@else
					<a href="{{ url('user/login')}}" class="btn btn-danger w-100 btn-lg">Abone Ol</a>
				@endif
			@endif
		</div>
	</div>
@endif
