@php
    $photo = $group_content->photo; 
    $title = $contVariable->title;
    $href_link = 'playlist/'.$contVariable->slug;
@endphp

    
<div class="grid-inner">
    @if(!empty($photo))
        <div class="entry-image">
            <a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$photo }}" alt="{{$title}}"></a>
        </div>
    @endif
    <div class="entry-title">
        <h2><a href="{{$href_link}}">{{$title}}</a></h2>
    </div>
</div>