@php
//dd($group_content);
@endphp

@if((!empty(json_decode($contVariable->content)->unvan)))
	<div class="team">
		<div class="team-image">
			@if((!empty(json_decode($contVariable->content)->photo)))
				<img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{json_decode( $contVariable->content)->photo }}" alt="{{$contVariable->title}}">
			@endif
		</div>
		<div class="team-desc ">
			<div class="team-title center"><h4>{{$contVariable->title}}</h4><span>{{$group_content->unvan}}</span></div>
		</div>
	</div>
@endif

