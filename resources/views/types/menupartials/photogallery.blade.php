@php

$contVariable = App\ContentVariable::where('slug',Request::segment(2))->first();
$content = $contVariable->contentdata;
if(empty($ths)){
	$ths = $content->contentThs->first();
}
$_colValue = '3';
$c_v = json_decode($ths->props)->props_type;//propstype c_v değişkenine atandı.
$display_number=json_decode($ths->props)->display_number;//propstype c_v değişkenine atandı.

@endphp


<div class="col-lg-12">
	<div class="masonry-thumbs grid-container grid-{{ $_colValue }}"  data-lightbox="gallery">
		@foreach ($contVariable->contentdata->photogallery($lang) as $image)
			<a class="grid-item" href="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $image->url }}" data-lightbox="gallery-item" @if($loop->index > $display_number - 1) style="display:none;" @endif >
				<img src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $image->url }}" alt="{{ $image->name }}" />
			</a>
		@endforeach
	</div>
</div>