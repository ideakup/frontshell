@php
$props_sidebar = json_decode($content->ContentThs->first()->props)->sidebar;
if($props_sidebar == 'template'){
    $sidebar_side =json_decode($menu->topHasSub->first()->props)->sidebar;
    $sidebar_id = $menu->id;
}
elseif($props_sidebar == 'uppermenu'){
    $uppermenu_ths=$content->contentThs->first()->topElementForContent->contentThs->first()->topElementForMenu->topHasSub->first();

    $sidebar_side =json_decode($uppermenu_ths->props)->sidebar;
    $sidebar_id = $uppermenu_ths->sub_id;
}
else{
    $sidebar_side = $props_sidebar;
    $sidebar_id = $content->id;
}

@endphp
<div class="row">
@if ($sidebar_side != 'none') 
    <div class="postcontent col-lg-9 
    @if($sidebar_side == 'left') 
        order-lg-last
    @endif    
    ">      
@endif

<div class="row">
    @if($content->variable->title != "")
        <div class="col-lg-12">
            <h2 class="blog-title">{{ $content->variableLang($lang)->title }}</h2>
            <p class="blog-spot-p">{!! json_decode($content->variableLang($lang)->content)->short_content !!}</p>
        </div>
    @endif

    @foreach ($content->subContentThs as $ths)
    	@php
            if($ths->sub_model=='Content'){
                $content = $ths->thatElementForContent;  
            }elseif($ths->sub_model=='MenuSum'){
                 $content = $ths->thatElementForMenusum;  
            }
            
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
            $element=$content;
            //dd($ths);
        @endphp
        <div class=" {{ json_decode($ths->props)->props_colvalue }}">
    	
            @if ($content->type == 'text')
                @include('types.menupartials.basiccontents.text')
            @elseif ($content->type == 'title')
                @include('types.menupartials.basiccontents.title')
            @elseif ($content->type == 'photo')
                @include('types.menupartials.basiccontents.photo')
            @elseif ($content->type == 'link')
                @include('types.menupartials.basiccontents.link')
            @elseif ($content->type == 'video')
                @include('types.menupartials.basiccontents.video')
            @elseif ($content->type == 'seperator')
                @include('types.menupartials.basiccontents.seperator')
            @elseif ($content->type == 'code')
                @include('types.menupartials.basiccontents.code')

            <!-- Complex Contents-->   
            @elseif ($content->type == 'photogallery')
                @include('types.menupartials.complexcontents.photogallery')
            @elseif ($content->type == 'slide')
                @php $slideData = $content->slide; @endphp
                @include('types.menupartials.complexcontents.slide')
            @elseif ($content->type == 'form')
                @include('types.menupartials.complexcontents.form')
            @elseif ($content->type == 'mapturkey')
                @include('types.menupartials.complexcontents.mapturkey')

            <!-- Group Contents-->
            @elseif (starts_with($content->type, 'group'))
                @php
                    $groupname = explode("-",$content->type);
                    if (empty($content->variableLang($lang))) {
                        $contVariable = $content->variable;
                    }else{
                        $contVariable = $content->variableLang($lang);
                    }
                    $group_content=json_decode($contVariable->content);  
                   // dump($groupname) ; 
                @endphp
                @include('types.menupartials.groupcontents.'.$groupname[1])
            @endif
        </div>
    @endforeach
</div>
@if ($sidebar_side != 'none') 

        </div>
            @include('types.sidebar.sidebar')     
    </div>
@endif

