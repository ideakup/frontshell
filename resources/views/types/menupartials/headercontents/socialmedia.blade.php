@php
//dd($group_content);
@endphp

<!-- Top Social
						============================================= -->
<ul id="top-social">
	@if(collect($group_content->type)->contains('facebook'))
		@if (!is_null($sitesettings->get('facebook_url')))
			<li>
				<a href="{{ $sitesettings->get('facebook_url') }}" class="si-facebook">
					<span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span>
				</a>
			</li>  
        @endif
    @endif
    @if(collect($group_content->type)->contains('instagram'))
        @if (!is_null($sitesettings->get('instagram_url')))
            <li>
            	<a href="{{ $sitesettings->get('instagram_url') }}" class="si-instagram">
            		<span class="ts-icon"><i class="icon-instagram"></i></span><span class="ts-text">Instagram</span>
            	</a>
            </li>
            
        @endif
    @endif
 	@if(collect($group_content->type)->contains('linkedin'))
        @if (!is_null($sitesettings->get('linkedin_url')))
        	<li>
        		<a href="{{ $sitesettings->get('linkedin_url') }}"class="si-linkedin">
        			<span class="ts-icon"><i class="icon-linkedin"></i></span><span class="ts-text">Linkedin</span>
        		</a>
            </li>
        @endif
 	@endif
 	@if(collect($group_content->type)->contains('twitter'))
        @if (!is_null($sitesettings->get('twitter_url')))
        	<li>
        		<a href="{{ $sitesettings->get('twitter_url') }}"class="si-twitter">
        			<span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span>
        		</a>
            </li>	         
        @endif
    @endif
	@if(collect($group_content->type)->contains('youtube'))
        @if (!is_null($sitesettings->get('youtube_url')))
        	<li>
        		<a href="{{ $sitesettings->get('youtube_url') }}"class="si-youtube">
        			<span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">YouTube</span>
        		</a>
            </li>
        @endif
    @endif
	
</ul><!-- #top-social end -->