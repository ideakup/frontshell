<ul id="top-social">
	@if(collect($group_content->type)->contains('phone'))
		@if (!is_null($sitesettings->get('gsm')))
			<li>
        		<a href="tel:{{ $sitesettings->get('gsm') }}" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">{{ $sitesettings->get('gsm') }}</span>
	        	</a>
	    	</li> 
        @endif
    @endif

    @if(collect($group_content->type)->contains('email'))
		@if (!is_null($sitesettings->get('email')))
			<li>
				<a href="mailto::{{ $sitesettings->get('email') }}" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">:{{ $sitesettings->get('email') }}</span>
				</a>
			</li>
        @endif
    @endif
</ul>