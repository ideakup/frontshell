
<!--<i class="i-plain {{$group_content->icon}} m-0"></i>
<div class="he-text">
	{{$contVariable->title}}
	<span>{{$group_content->short_content}}</span>
</div>
-->
@if((!empty($group_content->type)))
	@if($group_content->type == 'normal')
		<div class="feature-box fbox-effect fbox-sm">
			<div class="fbox-icon">
				<a href="#"><i class="{{$group_content->icon}}"></i></a>
			</div>
			<div class="fbox-content">
				<h3>{{$contVariable->title}}</h3>
				<p>{{$group_content->short_content}}</p>
			</div>
		</div>
	@elseif($group_content->type == 'centered')
		<div class="feature-box fbox-center fbox-effect fbox-sm">
			<div class="fbox-icon">
				<a href="#"><i class="{{$group_content->icon}}"></i></a>
			</div>
			<div class="fbox-content">
				<h3>{{$contVariable->title}}</h3>
				<p>{{$group_content->short_content}}</p>
			</div>
		</div>
	@elseif($group_content->type == 'bordered')	
		<div class="feature-box fbox-center fbox-bg fbox-effect fbox-sm">
			<div class="fbox-icon">
				<a href="#"><i class="{{$group_content->icon}}"></i></a>
			</div>
			<div class="fbox-content">
				<h3>{{$contVariable->title}}<span class="subtitle">{{$group_content->short_content}}</span></h3>
			</div>
		</div>
	@endif
@endif