
<div class="copyrights-menu copyright-links ws-footer-content {{ $textAlign == 'center' ? 'text-center' : ($textAlign == 'right' ? 'text-end' : 'text-start' ) }}">
    @php
        $topmenus=$topmenus->sortBy('type');
    @endphp
    @foreach ($topmenus as $menuitem)
            @php
                if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
                    $menuitemVariable = $menuitem->thatElementForMenu->variable;
                }else{
                    $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
                }
            @endphp
        @if(!starts_with($menuitem->type, 'module'))
            <a 
            @if($menuitem->type != "menuitem") 
                href="/{{$menuitemVariable->slug}}" 
            @else
            @php
                $suballmenu=$menuitem->subMenu;                                                           
            @endphp
            @foreach($suballmenu as $sbm)
                @if(!starts_with($sbm->type, 'list'))
                    href="/{{$sbm->thatElementForMenu->variable->slug}}"
                @endif    
            @endforeach
            href="/{{$menuitemVariable->slug}}" 
            @endif>{{$menuitemVariable->menutitle}}</a>                                              


            @endif    
    @endforeach
</div>
