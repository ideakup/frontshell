<div class="col_half ws-footer-content">
    <div class="row ">
        <div class="col">
            <img  class="footer-logo {{ $textAlign == 'center' ? 'mx-auto' : ($textAlign == 'right' ? 'float-end' : 'float-start' ) }}" src="{{ env('APP_UPLOAD_PATH_V3') }}/logos/logo-light-tr.png" alt="" style="width: 200px;"/>
        </div>
    </div>
</div>