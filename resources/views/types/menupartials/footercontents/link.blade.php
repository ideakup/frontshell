<div class="ws-footer-content {{ $textAlign == 'center' ? 'text-center' : ($textAlign == 'right' ? 'text-end' : 'text-start' ) }}">
	<a  href="{!! json_decode($fcontent->variable->content)->link !!}" target="{{ (json_decode($fcontent->variable->content)->toogle == 'on') ? '_blank' : '_self' }}">{{ $contVariable->title }}
	</a>
</div>
