<div class="ws-footer-content d-flex {{ $textAlign == 'center' ? 'justify-content-center' : ($textAlign == 'right' ? 'justify-content-end' : 'justify-content-start' ) }}">

	@if(collect($group_content->type)->contains('facebook'))
		@if (!is_null($sitesettings->get('facebook_url')))
			<a href="{{ $sitesettings->get('facebook_url') }}" class="social-icon si-small si-borderless si-facebook">
		        <i class="icon-facebook"></i>
		        <i class="icon-facebook"></i>
		    </a>
        @endif
    @endif
    @if(collect($group_content->type)->contains('instagram'))
        @if (!is_null($sitesettings->get('instagram_url')))
            <a href="{{ $sitesettings->get('instagram_url') }}" class="social-icon si-small si-borderless si-instagram">
		        <i class="icon-instagram"></i>
		        <i class="icon-instagram"></i>
		    </a>
        @endif
    @endif
    @if(collect($group_content->type)->contains('linkedin'))
        @if (!is_null($sitesettings->get('linkedin_url')))
        	<a href="{{ $sitesettings->get('linkedin_url') }}" class="social-icon si-small si-borderless si-linkedin">
		        <i class="icon-linkedin"></i>
		        <i class="icon-linkedin"></i>
		    </a>
        @endif
 	@endif
 	@if(collect($group_content->type)->contains('twitter'))
        @if (!is_null($sitesettings->get('twitter_url')))
        	<a href="{{ $sitesettings->get('twitter_url') }}" class="social-icon si-small si-borderless si-twitter">
		        <i class="icon-twitter"></i>
		        <i class="icon-twitter"></i>
		    </a>	         
        @endif
    @endif
    @if(collect($group_content->type)->contains('youtube'))
        @if (!is_null($sitesettings->get('youtube_url')))
        	<a href="{{ $sitesettings->get('youtube_url') }}" class="social-icon si-small si-borderless si-youtube">
		        <i class="icon-youtube"></i>
		        <i class="icon-youtube"></i>
		    </a>
        @endif
    @endif
</div>