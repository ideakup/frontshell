<div class="ws-footer-content d-flex {{ $textAlign == 'center' ? 'justify-content-center' : ($textAlign == 'right' ? 'justify-content-end' : 'justify-content-start' ) }}">
    @if(collect($group_content->type)->contains('email'))
		@if (!is_null($sitesettings->get('email')))
			<i class="icon-envelope2"></i> {{ $sitesettings->get('email') }} <span class="middot">&middot;</span> 
        @endif
    @endif
	@if(collect($group_content->type)->contains('phone'))
		@if (!is_null($sitesettings->get('gsm')))
			<i class="icon-headphones"></i> {{ $sitesettings->get('gsm') }}
        @endif
    @endif
</div>





