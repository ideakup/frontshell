@if(!empty($group_content->type))
	@if($group_content->type == 'topmenuvertical')
		<div class="widget_links clearfix ws-footer-content d-flex {{ $textAlign == 'center' ? 'justify-content-center' : ($textAlign == 'right' ? 'justify-content-end' : 'justify-content-start' ) }}">
		    @php
		        $topmenus=$topmenus->sortBy('type');
		    @endphp
		    <ul>
			    @foreach ($topmenus as $menuitem)
			    	<li>
		                @php
		                    if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
		                        $menuitemVariable = $menuitem->thatElementForMenu->variable;
		                    }else{
		                        $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
		                    }
		                @endphp
				        @if(!starts_with($menuitem->type, 'module'))
				            <a 
				            	@if($menuitem->type != "menuitem") 
				                	href="/{{$menuitemVariable->slug}}" 
				            	@else
				            		@php
				                		$suballmenu=$menuitem->subMenu;                                                           
				            		@endphp
				            		@foreach($suballmenu as $sbm)
				                		@if(!starts_with($sbm->type, 'list'))
				                    		href="/{{$sbm->thatElementForMenu->variable->slug}}"
				                		@endif    
				            		@endforeach
				            		href="/{{$menuitemVariable->slug}}" 
				            	@endif>
				            		{{$menuitemVariable->menutitle}}
				            </a>                                          
				    	@endif  
				    </li>  
			    @endforeach
			</ul>
		</div>
	@elseif($group_content->type == 'topmenuhorizontal')
		<div class="copyrights-menu ws-footer-content d-flex {{ $textAlign == 'center' ? 'justify-content-center' : ($textAlign == 'right' ? 'justify-content-end' : 'justify-content-start' ) }}">
		    @php
		        $topmenus=$topmenus->sortBy('type');
		    @endphp
		    @foreach ($topmenus as $menuitem)
		            @php
		                if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
		                    $menuitemVariable = $menuitem->thatElementForMenu->variable;
		                }else{
		                    $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
		                };
		            @endphp
		        @if(!starts_with($menuitem->type, 'module'))
		        	@if($loop->first == 0)
		        		/
		        	@endif
		            <a 
		            @if($menuitem->type != "menuitem") 
		                href="/{{$menuitemVariable->slug}}" 
		            @else
		            @php
		                $suballmenu=$menuitem->subMenu;                                                           
		            @endphp
		            @foreach($suballmenu as $sbm)
		                @if(!starts_with($sbm->type, 'list'))
		                    href="/{{$sbm->thatElementForMenu->variable->slug}}"
		                @endif    
		            @endforeach
		            href="/{{$menuitemVariable->slug}}" 
		            @endif>{{$menuitemVariable->menutitle}}</a>                                              
		    	@endif    
		    @endforeach
		</div>
	@elseif($group_content->type == 'submenu')
		<div class="row ws-footer-content d-flex {{ $textAlign == 'center' ? 'justify-content-center' : ($textAlign == 'right' ? 'justify-content-end' : 'justify-content-start' ) }}"> 
			@php
			$topmenus=$topmenus->sortBy('order');
			@endphp

		        @foreach ($topmenus as $menuitem)

		            @php

		                if (empty($menuitem->thatElementForMenu->variableLang($lang))) {
		                    $menuitemVariable = $menuitem->thatElementForMenu->variable;
		                }else{
		                    $menuitemVariable = $menuitem->thatElementForMenu->variableLang($lang);
		                }
		                 $submenu=$menuitem->subMenu;
		            @endphp
		           
		      
		           @if(!starts_with($menuitem->type, 'module'))
		                <div style="text-align: left !important;" class="col-3 widget_links">

		                    
		                       <ul style="margin-top: 30px !important; ">
			                        <h5>
			                               <a 
					                            @if($menuitem->type != "menuitem") 
					                                href="/{{$menuitemVariable->slug}}" 
					                            @else
					                                @php
					                                    $suballmenu=$menuitem->subMenu;                                                           
					                                @endphp
					                                @foreach($suballmenu as $sbm)
					                                        @if(!starts_with($sbm->type, 'list'))
					                                            href="/{{$sbm->thatElementForMenu->variable->slug}}"
					                                        @endif    
					                                @endforeach
					                                href="/{{$menuitemVariable->slug}}" 
					                            @endif>{{$menuitemVariable->menutitle}}
					                        </a>    
			                        </h5>
			                                                               
			                       @foreach($submenu as $submenuitem)
			                            @php
			                                if (empty($submenuitem->thatElementForMenu->variableLang($lang))) {
			                                    $submenuitemVariable = $submenuitem->thatElementForMenu->variable;
			                                }else{
			                                    $submenuitemVariable = $submenuitem->thatElementForMenu->variableLang($lang);
			                                }
			                            @endphp
			                            <li ><a  href="/{{$submenuitemVariable->slug}}">{{$submenuitemVariable->menutitle}}</a></li>
			                        @endforeach
			                    </ul>  
		                   
		                                                                  
		               </div>
		            @endif    
		        @endforeach
		</div>
	@endif
@endif

