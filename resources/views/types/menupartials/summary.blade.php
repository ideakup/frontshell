@php
//dump($content->contentThs->first());
	if(!empty(json_decode($contVariable->content)->tag)){
		$tag_ids = json_decode($contVariable->content)->tag;
	}
	if(!empty(json_decode($contVariable->content)->visible_type)){
		$visible_type = json_decode($contVariable->content)->visible_type;
	}else{
		$visible_type = 'default';
	}
	$content_type = json_decode($contVariable->content)->type;

	$sum_contents = App\Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->where('top_model','Content')->where('lang_code',$lang)
                           ->whereIn('type', $content_type)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $sum_contents = $sum_contents->unique(); 
    if(!empty($tag_ids)){
    	$content_tag = App\ContentHasTag::whereIn('tag_id',$tag_ids)->pluck('content_id');
    	$sum_contents = $sum_contents->whereIn('sub_id',$content_tag);
    }
    // section içine koyulmadığın carousel özelliği için
    if(empty($section_type)){
    	$section_type = 'grid';
    }

@endphp
	@if($visible_type =='list')
		@if(starts_with($section_type,'carousel'))
          	<div class="oc-item">
          		<div class="posts-sm row col-mb-30">
        @elseif($section_type == 'grid')
        	<div class="{{ json_decode($ths->props)->props_colvalue }}">
        		<div class="posts-sm row col-mb-30">
        @endif
    @endif
	@foreach($sum_contents as $sum_content)
			@if($visible_type !='list')
				@if(starts_with($section_type,'carousel'))
		          	<div class="oc-item">
		        @elseif($section_type == 'grid')
		        	<div class="{{ json_decode($content->contentThs->first()->props)->props_colvalue }}">
		        @endif
	        @endif
			@if($sum_content->type =='group-blog')
				@php
					$group_content = json_decode($sum_content->content);
					$href_link = 'blog/'.$sum_content->slug;
					if(!empty($group_content->photo)){
						$photo = $group_content->photo; 
					}
					$title = $sum_content->title;
					if(!empty($group_content->short_content)){
						$short_content = $group_content->short_content;
					}
					$created_at = $sum_content->created_at;
				@endphp
				@if($visible_type =='default')
					@include('types.posttypes.blog')
				@elseif($visible_type =='list')
					@include('types.posttypes.list')
				@endif
			@elseif($sum_content->type =='group-product')
				@php
					$group_content = json_decode($sum_content->content);
					$href_link = 'product/'.$sum_content->slug;
				    if(!empty($group_content->photo)){
						$photo = $group_content->photo; 
					}
				    $title = $sum_content->title;
				    $stock = $sum_content->stock;
				@endphp
				@if($visible_type =='default')
					@include('types.posttypes.product')
				@elseif($visible_type =='list')
					@include('types.posttypes.list')
				@endif
			@endif
		@if($visible_type !='list')
			</div>	
		@endif
	@endforeach
	@if($visible_type =='list')
		@if(starts_with($section_type,'carousel'))
          	</div>
          </div>
        @elseif($section_type == 'grid')
	        	</div>
	        </div>
        @endif
    @endif



