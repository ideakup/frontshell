
<div id="top-cart" class="header-misc-icon">
    <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span id="top-cart-count" class="top-cart-number">{{Cart::instance('wl')->count()}}</span></a>
    <div id="cartcontent" class="top-cart-content">
        <div class="top-cart-title">
          <h4>Shopping Cart</h4>
        </div>
        <div id="cartitems">
          @foreach($cartcontent as $row)
            <div id="insideofcart" class="top-cart-items insideofcart">
                <div class="top-cart-item clearfix">
                    <div class="top-cart-item-image">
                        @php
                          $allcontent=$allcontent->where('content_id',$row->id)->first();
                          $group_content=json_decode($allcontent->content);
                        @endphp
                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'medium/'. $group_content->photo) }}" alt="" />
                    </div>
                    <div class="top-cart-item-desc">
                      {{$row->name}}
                      <span  class="top-cart-item-price">{{number_format((float)$row->total, 2, ',', '') }} ₺</span>
                      <span style="margin-left: 20px;" id="iocqty" class="top-cart-item-quantity"> {{ $row->qty}}</span>
                    </div>
                </div>
            </div>
          @endforeach
        </div>  
        <div class="top-cart-action clearfix cartbottom">
          <span class="fleft top-checkout-price alltotal">{{Cart::instance('wl')->total(2,",","")}} ₺</span>
          <a href="{{ url('shop/cart') }}"><button class="button button-3d button-small nomargin fright">Sepete Git</button></a> 
        </div>
    </div>
</div><!-- #top-cart end -->
  
