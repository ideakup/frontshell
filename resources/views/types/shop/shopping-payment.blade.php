@php

	$identifier=$_COOKIE["shop_cookie"];
    $instance="wl";
    Cart::instance($instance)->store($identifier,$instance);
	$order=App\ShoppingOrder::where('order_no',Request::segment(3))->first();


	$shop_settigs=App\ShoppingOrderSettings::find(1);
	$API_keys=json_decode($shop_settigs->extra);
	//dd($API_keys);

	$shipping_cost=$shop_settigs->shipping_cost;
	$min_shippingcost=$shop_settigs->min_shippingcost;
	$shipping_cost_price=$shop_settigs->shipping_cost_price;
	if($shop_settigs->shipping_type==1){
		$shipping_cost_price=$shipping_cost_price*Cart::instance('wl')->count();
	}
	$total=Cart::instance('wl')->total(2,".","");
	if($shipping_cost){
		if(Cart::instance('wl')->total(2,".","")<$min_shippingcost){
			$total=$total+$shipping_cost_price;
		}
	}
	//dd($total);
	$order_content=json_decode($order->order_content);
	$address_information=json_decode($order->address_information);
	$adress_phone=json_decode($address_information->adress);

	if(is_null($order->user_id)){
		$firstname_name=$adress_phone->firstname;
		$lastname_name=$adress_phone->lastname;
		$user_id=0;
	}
	else{
		$firstname_name=$address_information->firstname;
		$lastname_name=$address_information->lastname;
		$user_id=$order->user_id;
	}
	/*dump($order);
	dump(collect($order_content));
	dump($address_information);

	dd($adress_phone);*/
@endphp

@php
$request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
$request->setLocale(\Iyzipay\Model\Locale::TR);
$request->setConversationId("123456789");
$request->setPrice(Cart::instance('wl')->total(2,".",""));
$request->setPaidPrice($total);
$request->setCurrency(\Iyzipay\Model\Currency::TL);
$request->setBasketId($order->order_no);
$request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
$request->setCallbackUrl(url("/")."/paymentcheck");
$request->setEnabledInstallments(array(2, 3, 6, 9));

$buyer = new \Iyzipay\Model\Buyer();
$buyer->setId($user_id);
$buyer->setName($firstname_name);
$buyer->setSurname($lastname_name);
$buyer->setGsmNumber($adress_phone->phone);
$buyer->setEmail($address_information->email);
$buyer->setIdentityNumber($adress_phone->identity_no);

$buyer->setRegistrationAddress($adress_phone->address);
$buyer->setIp("85.34.78.112");
$buyer->setCity($address_information->city);
$buyer->setCountry("Türkiye");


$request->setBuyer($buyer);
$shippingAddress = new \Iyzipay\Model\Address();
$shippingAddress->setContactName($firstname_name." ".$lastname_name);
$shippingAddress->setCity($address_information->city);
$shippingAddress->setCountry("Türkiye");
$shippingAddress->setAddress($adress_phone->address);
$request->setShippingAddress($shippingAddress);

$billingAddress = new \Iyzipay\Model\Address();
$billingAddress->setContactName($firstname_name." ".$lastname_name);
$billingAddress->setCity($address_information->city);
$billingAddress->setCountry("Türkiye");
$billingAddress->setAddress($adress_phone->address);
$request->setBillingAddress($billingAddress);



$piece = 0;
    foreach($order_content as $product){
    	//dd($product);

        $BasketItem = new \Iyzipay\Model\BasketItem();
        $BasketItem->setId($product->rowId);
        $BasketItem->setName($product->name);
        $BasketItem->setCategory1($product->name);
        $BasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
        $BasketItem->setPrice($product->qty*$product->price);
        $basketItems[$piece] = $BasketItem;

        $piece++;
    }

$request->setBasketItems($basketItems);




$options = new \Iyzipay\Options();
$options->setApiKey($API_keys->API_key);
$options->setSecretKey($API_keys->Sec_key);
$options->setBaseUrl("https://sandbox-api.iyzipay.com");



$checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, $options);

//dd($checkoutFormInitialize)
@endphp



		<div id="iyzipay-checkout-form" class="responsive"></div>




@section("inline-scripts")
	{!!$checkoutFormInitialize->getCheckoutFormContent() !!}
@endsection