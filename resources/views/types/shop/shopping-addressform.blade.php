

@if(Auth::check())
	@php
	//dd($errors);
	    $user = Auth::user();
	@endphp
	<div class="container clearfix">
		<div class="row">
			<div class="col-4"></div>
			<div class="col-4">					
			<form id="withoutLogin-form" name="withoutLogin-form" class="nobottommargin" action="{{url('sta/addresrecord')}}" method="post">

				<h3>Yeni Adres Oluştur</h3>
					 {{ csrf_field() }}

				<div class="form-group">
					<label for="name">İsim:</label>
					<input type="text" id="name" name="name" value="{{$user->firstname}}" class="form-control" readonly required  />
				</div>
				<div class="form-group">
					<label for="lastname">Soyisim:</label>
					<input type="text" id="lastname" name="lastname" value="{{$user->lastname}}"  class="form-control" readonly required />
				</div>
				<div class="form-group">
					<label for="email">Email Address:</label>
					<input type="text" id="email" name="email" value="{{$user->email}}"  class="form-control" readonly required />
				</div>
				<div class="form-group @if ($errors->has('identity_no')) has-danger @endif" >
					<label for="identity_no">TC Kimlik:</label>
					<input type="text" id="identity_no" name="identity_no"  class="form-control " required />
					@if ($errors->has('identity_no'))
                        <div id="identity_no-error" class="form-control-feedback">{{ $errors->first('identity_no') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('phone')) has-danger @endif" >
					<label for="phone">Phone:</label>
					<input type="text" id="phone" name="phone" placeholder="55XXXXXXXX" class="form-control " value="{{old('phone')}}" required />
					@if ($errors->has('phone'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('phone') }}</div>
                    @endif
				</div>																
				<div class="form-group @if ($errors->has('city')) has-danger @endif">
					<label for="city">İl:</label> <br>
					<select style="width: 100%;" class="form-control" name="city" id="city" required>
						<option value="">Seçiniz...</option>                                          
                        @foreach ($cities as $city)
                            <option @if( $city->variable->name == old('city') ) selected @endif > {{ $city->variable->name }} </option>
                        @endforeach
					</select>
					@if ($errors->has('city'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('city') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('town')) has-danger @endif">
					<label for="town">İlçe:</label> <br>
					<select style="width: 100%;" class="form-control" name="town" id="town" required>
						<option value="">Seçiniz...</option>
	                        @foreach ($counties as $county)
	                            <option @if( $county->variable->name == old('town') ) selected @endif > {{ $county->variable->name }} </option>
	                        @endforeach
					</select>
					@if ($errors->has('town'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('town') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('adress')) has-danger @endif">
					<label for="adress">Adres:</label>
					<textarea class="form-control" id="withoutLogin_form_adress" name="adress" rows="3"></textarea>
					@if ($errors->has('adress'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adress') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('adressname')) has-danger @endif">
					<label for="adressname">Adres İsmi:</label>
					<input type="text" id="adressname" name="adressname" value="" class="form-control" required />
					@if ($errors->has('adressname'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adressname') }}</div>
                    @endif
				</div>
				<div class="col_full nobottommargin">
					<button class="button button-3d button-black nomargin" id="withoutLogin_form_submit" name="withoutLogin_form_submit" value="withoutLogin">Devam et</button>
				</div>
			</form>
			</div>
		</div>	
	</div>

@elseif(empty(Auth::user()) && !empty($_COOKIE["shop_cookie"]))

	@php
		$UWLI_email=$shoppingcheckout->where('shop_cookie',$_COOKIE["shop_cookie"])->first()->user_email;
		$useraddreses=$alladdresses->where('email',$UWLI_email)->where('user_id',null)->first();
	@endphp

	<div class="container clearfix">

		<div class="row ">
			<div class="col-4"></div>
			<div class="col-4">					
			<form id="withoutLogin-form" name="withoutLogin-form" class="nobottommargin" action="{{url('sta/addresupdate')}}" method="post">

				<h3>Adres Düzenle</h3>
					 {{ csrf_field() }}

				<div class="col_full">
					<label for="name">İsim:</label>
					<input type="text" id="name" name="name" value="{{json_decode($useraddreses->adress)->firstname}}" class="form-control"readonly required />
				</div>
				<div class="col_full">
					<label for="lastname">Soyisim:</label>
					<input type="text" id="lastname" name="lastname" value="{{json_decode($useraddreses->adress)->lastname}}"  class="form-control"readonly required />
				</div>
				<div class="col_full">
					<label for="email">Email Address:</label>
					<input type="text" id="email" name="email" value="{{$useraddreses->email}}"  class="form-control" readonly required />
				</div>
				<div class="form-group @if ($errors->has('identity_no')) has-danger @endif" >
					<label for="identity_no">TC Kimlik:</label>
					<input type="text" id="identity_no" name="identity_no" value="{{json_decode($useraddreses->adress)->identity_no}}" class="form-control " required />
					@if ($errors->has('identity_no'))
                        <div id="identity_no-error" class="form-control-feedback">{{ $errors->first('identity_no') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('phone')) has-danger @endif" >
					<label for="phone">Phone:</label>
					<input type="text" id="phone" name="phone" value="{{json_decode($useraddreses->adress)->phone}}"  class="form-control" required/>
					@if ($errors->has('phone'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('phone') }}</div>
                    @endif
				</div>																
				<div class="form-group @if ($errors->has('city')) has-danger @endif">
					<label for="city">İl:</label><br>
					<select style="width: 100%;" class="form-control" name="city" id="city" required>
						<option>{{$useraddreses->city}}</option>                                          
                        @foreach ($cities as $city)
                            <option @if( $city->variable->name == old('city') ) selected @endif> {{ $city->variable->name }} </option>
                        @endforeach
					</select>
					@if ($errors->has('city'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('city') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('town')) has-danger @endif">
					<label for="town">İlçe:</label><br>
					<select style="width: 100%;" class="form-control" name="town" id="town" required>
						<option> {{$useraddreses->town}}</option>
	                        @foreach ($counties as $county)
	                            <option  @if( $county->variable->name == old('town') ) selected @endif  > {{ $county->variable->name }} </option>
	                        @endforeach
					</select>
					@if ($errors->has('town'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('town') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('adress')) has-danger @endif">
					<label for="adress">Adres:</label>
					<textarea class="form-control" id="withoutLogin_form_adress" name="adress" rows="3">{{json_decode($useraddreses->adress)->address}}</textarea>
					@if ($errors->has('adress'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adress') }}</div>
                    @endif
				</div>
				<div class="form-group @if ($errors->has('adressname')) has-danger @endif">
					<label for="adressname">Adres İsmi:</label>
					<input type="text" id="adressname" name="adressname" value="{{$useraddreses->adress_name}}" class="form-control" required/>
					@if ($errors->has('adressname'))
                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adressname') }}</div>
                    @endif
				</div>
				<div class="col_full nobottommargin">
					<button class="button button-3d button-black nomargin" id="withoutLogin_form_submit" name="withoutLogin_form_submit" value="withoutLogin">Devam et</button>
				</div>
			</form>
			</div>
		</div>	
	</div>

	@endif

@section("inline-scripts")
  <script type="text/javascript">

 


    $(document).ready(function() {
        $('#city').select2();
        $('#town').select2();

    });

    $('#city').change(function(){ 
            var cityname = $(this).val();
            console.log(cityname);

            $.ajax({
                  url: "/api/city",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  cityname:cityname,
           
                },
                success:function(response){
                   console.log(response[0]['name']);

                   $('#town').empty(); 
          
                       for (let i = 0; i < response.length; i++) {
                        $("#town").append('\
                                <option>'+response[i]['name']+'</option>\
                         ');
                    }
                      
                },
            });

    });


  </script>
@endsection