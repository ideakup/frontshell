
@if(!empty($_COOKIE["shop_cookie"]))
		@php
				//dd($shop_settigs);
        //dd(json_decode($allcontent->whereIn('content_id',$cartsubcontent->pluck("id"))));
        $identifier=$_COOKIE["shop_cookie"];
        $instance="sub";

        $cart_content=Cart::instance($instance)->content()->first();
        foreach($cartsubcontent as $row){
          
          $matched_content=$allcontent->where('content_id',$row->id)->first();

          $itemContent = json_decode($matched_content->content);
          $periodandprice = collect($itemContent->periodandprice);


          if($cart_content->options->subscriptionperiod != 0){
            $price=$periodandprice[$cart_content->options->subscriptionperiod];
          }else{
            $price=$cart_content->price;
          }
          
          $tax_rate=0;
          if(!empty($itemContent->tax_rate)){
              $tax_rate=$itemContent->tax_rate;
          }
          
          $row->name= $matched_content->title;
          $row->price= $price;
          $row->options['tax_rate']=$tax_rate;

        }
          Cart::instance($instance)->store($identifier,$instance);
    @endphp
 	<form id="register-form" name="register-form" class="nobottommargin" action="
 			@if(json_decode($shop_settigs->extra)->payment_method=='parampos')
				{{url('sta/parampos_place_order')}}
 			@elseif(json_decode($shop_settigs->extra)->payment_method=='iyzico')		
				{{url('sta/iyzico_place_order')}}
 			@elseif(json_decode($shop_settigs->extra)->payment_method=='halk_bankası')		
				{{url('sta/halk_bankası_place_order')}}
 			@endif		
		" method="post"> 
 		{{ csrf_field() }}
		<div class="container clearfix">
			<div class="row clearfix">
				<div class="col-lg-8">
					<h4>Adresleriniz</h4>
					@if(!empty( $user = Auth::user()))
						<div class="card">
							<div class="card-body">
								<h5 class="card-text">
									{{$user->firstname." ".$user->lastname}}
								</h5>
								@if($alladdresses->where('user_id',$user->id)->count()<1)
									<div class="card">
										<div class="card-body">
											<span class="alert alert-warning"> Kayıtlı adresiniz yok!</span>									
										</div>
									</div>
								@else
									@php
										$useraddreses=$alladdresses->where('user_id',$user->id);
										//dd($useraddreses);
									@endphp
									<div class="row">
									@foreach($useraddreses as $ua)
										<div id="insidecard{{$ua->id}}" class="col-lg-4 card card-inside">
											<div class="card-body">

												<h5 class="card-text"> <input style="top: 2px;" type="radio" id="adress_id"  name="adress_id" value="{{$ua->id}}" required> {{$ua->adress_name}}</h5>
												{{json_decode($ua->adress)->address}}
												<strong>{{$ua->town }}/{{$ua->city }}</strong>
												<br><br>
												{{json_decode($ua->adress)->phone}}																		
											</div>
											
										</div>
									@endforeach
									</div>		
								@endif
								@if(empty(Request::segment(3)))		
									<a href="{{url('shop/addressformsub')}}"class=" fright"><i class="fas fa-edit"></i>Adres Ekle</a>
								@else
									<a href="{{url('shop/addressformsub/')}}/{{Request::segment(3)}}/{{Request::segment(4)}}"class=" fright"><i class="fas fa-edit"></i>Adres Ekle</a>
								@endif
							</div>
						</div>
					@endif
					<div class="table-responsive">
						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-name">Ürün</th>
								 	@if(Request::segment(4) != 'upgrade')
			                            <th class="cart-product-subscriptionperiod">Abonelik @if(!is_null(Request::segment(3))) Uzatma  @endif Süresi</th> 
			                        @endif
									<th class="cart-product-domain">Domain</th>
									<th class="cart-product-subtotal">Tutar</th>
								</tr>
							</thead>
							<tbody>
								@foreach($cartsubcontent as $row)
									<tr class="cart_item">
										@if(Request::segment(4) != 'upgrade')
					                        @php
					                         	$allcontent=$allcontent->where('content_id',$row->id)->first();
					                            $group_content=json_decode($allcontent->content);
				                          	@endphp
											<td class="cart-product-name">
						                       {{$row->name}}
						                    </td>
											 <td class="cart-product-subscriptionperiod">
												<div class="cart-product-subscriptionperiod">
													{{$row->options->subscriptionperiod}} Ay
												</div>
											</td>
										@else
					                        @php
					                         	$allcontent=$allcontent->where('content_id',$row->id)->first();
					                            $group_content=json_decode($allcontent->content);
				                          	@endphp
					                          <td class="cart-product-name">
						                       {{$row->name}} [Paket yükseltmesi]
						                    </td>
						                @endif
										@if(is_null(Request::segment(3)))
											<td class="cart-product-domain">
							                    <span class="amount">{{$row->options->domain}}<span>
							                </td>
							            @else
							            	@php
							                  $order=App\ShoppingOrder::find(Request::segment(3));
							                  $domain = App\Domain::where('order_no',$order->order_no)->first();
						                  	@endphp
						                  	<td class="cart-product-domain">
							                    <span class="amount">{{$domain->domain_name}}<span>
							                </td>
							            @endif
										<td class="cart-product-subtotal">
						                    <span class="amount">{{$row->total(2,",","") }} ₺<span>
						                </td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-lg-4">
					<h4>Sipariş Özeti</h4>
					<div class="table-responsive">
			            <table class="table cart">
			              <tbody>
			                  <tr class="cart_item">
			                    <td class="cart-product-name">
			                      <strong>Vergisiz Fiyat</strong>
			                    </td>
			                    <td class="cart-product-name">
			                      <span class="amount">{{Cart::instance('sub')->subtotal(2,",","")}} ₺</span>
			                    </td>
			                  </tr>
			                  <tr class="cart_item">
			                    <td class="cart-product-name">
			                      <strong>Vergi</strong>
			                    </td>
			                    <td class="cart-product-name">
			                      <span class="amount">{{Cart::instance('sub')->tax(2,",","")}} ₺</span>
			                    </td>
			                  </tr>

			                  <tr class="cart_item">
			                    <td class="cart-product-name">
			                      <strong>Toplam</strong>
			                    </td>
			                    <td class="cart-product-name">
			                    	@php
			                    		
			                    		$total=Cart::instance('sub')->total(2,".","");


			                    	@endphp
			                      <span class="amount color lead checkout-total"><strong id="checkout-total">{{number_format((float)$total, 2, ',', '')}} ₺</strong></span>
			                    </td>
			                  </tr>
			              </tbody>
			            </table>
		        	</div>
		      		<div class=" card">
						<div class="card-body">
							<input  type="checkbox" id="agreementcheck"  name="agreement" required>
							<a href="#myModal1" data-lightbox="inline" >	Mesafeli Satış Sözleşmesi</a> 'ni okudum, onaylıyorum.
						</div>
					</div>

					@if(json_decode($shop_settigs->extra)->payment_method!='closed')

						<br>

						@if($errors->any())
							<div  id="cart-alert" class="alert alert-danger" role="alert">
					  			<p id="cart-alert-message">{{$errors->first()}}</p>
							</div>
						@endif
										
	        			<div class="form-group  ">
	                      <label for="exampleFormControlInput1">Kart Numarası</label>
	                      <input type="text" class="form-control" name="credit_card_no" id="credit_card_no" type="text"  >
	                  	</div>
	                  	<div class="form-group">
	                      	<label for="exampleFormControlInput1">Kart Üzerindeki İsim</label>
	                      	<input type="text" class="form-control" name="credit_card_name" id="credit_card_name" type="text" >
	                  	</div>
	                  	<label for="exampleFormControlInput1">Son Kullanma Tarihi</label>
	                  	<div class="form-row row"> 
			                <div  class="form-group col-lg-3">
		                        <select name="credit_card_month" class="form-control" id="credit_card_month">
				                    <option value="01">01</option>
				                    <option value="02">02</option>
				                    <option value="03">03</option>
				                    <option value="04">04</option>
				                    <option value="05">05</option>
				                    <option value="06">06</option>
				                    <option value="07">07</option>
				                    <option value="08">08</option>
				                    <option value="09">09</option>
				                    <option value="10">10</option>
				                    <option value="11">11</option>
				                    <option value="12">12</option>
			                  	</select>
			                </div>
			               	<div class="form-group col-lg-4">
		                    	<select name="credit_card_year" class="form-control" id="credit_card_year">
	                          		<option value="2022">2022</option>
		                          	<option value="2023">2023</option>
		                          	<option value="2024">2024</option>
		                          	<option value="2025">2025</option>
		                          	<option value="2026">2026</option>
		                          	<option value="2027">2027</option>
		                          	<option value="2028">2028</option>
		                          	<option value="2029">2029</option>
		                          	<option value="2030">2030</option>
		                          	<option value="2031">2031</option>
			                    </select>
			                </div>
		                  	<div class="form-group col-lg-4">
			                       <input name="credit_card_cvs" class="form-control fright"  id="credit_card_cvs" type="text" class="fleft" placeholder="CVS" size="3">
		                  	</div>
		                  	@if(json_decode($shop_settigs->extra)->payment_method == 'iyzico')
								<div id="installment_div"></div>
							@endif
						</div>
						@if(json_decode($shop_settigs->extra)->payment_method != 'halk_bankası')
							<input  type="checkbox" id="threeDsecure"  name="threeDsecure" value="3D"  > 3D secure  
						@endif	

						<div class="form-row fright">
		  					<button type="submit" id="parampos-place-order" class="button button-3d place-order">Siparişi Tamamla</button> 
			  			</div>
					@endif  
				</div>
			</div>
		</div>
		<input type="hidden" name="product_type" id="product_type" value="sub">
		<input type="hidden" name="alltotal" id="alltotal" value="{{$total}}">
		<input type="hidden" name="total" id="total" value="{{Cart::instance('sub')->total(2,".","")}}">
		<input type="hidden" name="identifier" id="identifier" value="{{$identifier}}">
		<input type="hidden" name="payment_info" id="payment_info" value="">
		@if(!is_null(Request::segment(3)))
			<input type="hidden" name="order_id" id="order_id" value="{{Request::segment(3)}}">
		@endif


	</form>  	

				
@else
	@include("types.shop.shopping-emptycart")
@endif	

@section("inline-scripts")
	<script type="text/javascript">

			$('input:radio[name="adress_id"]').change(function() {

				var adress_id=$("input[name=adress_id]:checked").val()
				var insdecardid="insidecard"+adress_id;

				var insedecards = document.getElementsByClassName("card-inside");
				for (let i = 0; i < insedecards.length; i++) {
				  insedecards[i].style.border  = "1px solid rgba(0, 0, 0, 0.125)";
				}
				document.getElementById(insdecardid).style.border  = "1px solid green";
			});
			$(document).ready(function(){
	            $("#place-order").click(function(event){

	            	var valid = this.form.checkValidity(); // for demonstration purposes only, will always b "true" here, in this case, since HTML5 validation will block this "click" event if form invalid (i.e. if "required" field "foo" is empty)
		            $("#adress_id").html(valid);
		            if (valid) {
		            event.preventDefault(); 
		                var adress_id=$("input[name=adress_id]:checked").val()

	            	var x = getCookie("shop_cookie");
	                if(x!==""){
	                  var shop_cookie=x;
	                }
	                console.log(adress_id);
	                $.ajax({
		                url: "/api/place_order",
		                type:"POST",
		                headers: { 'X-CSRF-TOKEN': $('#token').val()},
		                data:{
		                  adress_id:adress_id,
                      userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
	                  	identifier:shop_cookie,

		                },
		                success:function(response){
		                  console.log(response);
		                  if(response['success']==1){
		                  	window.location.href = "/shop/payment/"+response['order_no'];		                  }
		                  else if(response['success']==2){
		                  	window.location.href = "/shop/payment/"+response['order_no'];
		                  }
		                },
	               });
		            }

	            	
	            });


		          $("#agreebtn").click(function(event){
									document.getElementById("agreementcheck").checked = true;
		          });

		         	function getCookie(cname) {
		              var name = cname + "=";
		              var decodedCookie = decodeURIComponent(document.cookie);
		              var ca = decodedCookie.split(';');
		              for(var i = 0; i <ca.length; i++) {
		                var c = ca[i];
		                while (c.charAt(0) == ' '){
		                  c = c.substring(1);
		                }
		                if (c.indexOf(name) == 0) {
		                  return c.substring(name.length, c.length);
		                }
		              }
		              return "";
		          }
	        });
	        $('#credit_card_no').on('keypress change blur', function () {
			  $(this).val(function (index, value) {
			    return value.replace(/[^a-z0-9]+/gi, '').replace(/(.{4})/g, '$1 ');
			  });
			});
			$('#credit_card_no').on('copy cut paste', function () {
			  setTimeout(function () {
			    $('#credit_card_no').trigger("change");
			  });
			});

			$("#credit_card_no").change(function(){
			  
                var card_no=$("#credit_card_no").val();
                var total_price = {{$total}};
                var x = getCookie("shop_cookie");
	            if(x!==""){
	              var shop_cookie=x;
	            }
                $.ajax({
	                url: "/api/installment",
	                type:"POST",
	                headers: { 'X-CSRF-TOKEN': $('#token').val()},
	                data:{
	                  card_no:card_no,
	                  total_price:total_price,
	                  identifier:shop_cookie,

	                },

	                success:function(response){
	                 	if(response) {    

	                 		$('.installment').remove();
		                    var obj = jQuery.parseJSON(response);
		                 	 $("#installment_div").append('\
		                 	 	<div id="installment_div">\
						            <table class="table installment">\
						            	<thead>\
						            	  	<tr>\
											    <th>Taksit sayısı</th>\
											    <th>Aylık</th>\
											    <th>Toplam</th>\
										  	</tr>\
									  	</thead>\
									  	<tbody id="installment_tbody">');
		                 	 for (const [key, value] of Object.entries(obj['installmentPrices'])){

		                 	  $("#installment_tbody").append('\
		                 	 	<tr class="installmentitem">\
		                 	 	\
				                    <td class="installment_radio_td"> <div class="form-check">\
											<input class="form-check-input installment_radio" type="radio" name="installment" value="'+`${value["installmentNumber"]}`+'_'+`${value["totalPrice"]}`+'" required>'+`${value["installmentNumber"]}`+' Taksit\
										</div>\</td>\
				                    <td class="installment_price "> '+`${value["installmentPrice"]}`+' ₺</td>\
				                    <td class="total_price "> '+`${value["totalPrice"]}`+' ₺</td>\
			                  	</tr>');
            	
	                    	}


		                 	 $("#installment_div").append('</tbody></table>');
		            
		                 	 
		                 	 var payment_info = [obj['bankName'], obj['cardAssociation'], obj['cardFamilyName'], obj['cardType']];
		                 	 $('#payment_info').val(payment_info);
		                 	 $('input:radio[name="installment"]').change(function() {
		                 	 	var installment=$("input[name=installment]:checked").val();
		                 	 	const myArray = installment.split("_");
				            	$('#alltotal').val(myArray[1]);
				            	$('#checkout-total').text(myArray[1]+" ₺");
				            	

							});
	                  	}
                	},
               	});	
            });
            

            function getCookie(cname) {
	          var name = cname + "=";
	          var decodedCookie = decodeURIComponent(document.cookie);
	          var ca = decodedCookie.split(';');
	          for(var i = 0; i <ca.length; i++) {
	            var c = ca[i];
	            while (c.charAt(0) == ' '){
	              c = c.substring(1);
	            }
	            if (c.indexOf(name) == 0) {
	              return c.substring(name.length, c.length);
	            }
	          }
	          return "";
	        }

	</script>
@endsection




<div class="modal1 mfp-hide" id="myModal1">
	<div class="block divcenter" style="background-color: #FFF; max-width: 500px;">
		<div class="center" style="padding: 50px;">
			<h3> Mesafeli Satış Sözleşmesi</h3>
						<div style="height:200px;  border: solid 1px black; overflow: auto; font-size: 12px;"class="single-arthor-detail">
              <div class="single-arthor-widget">

              	{!!$shop_settigs->shopping_text!!}
              </div>
            
          </div>
		</div>
		<div class="section center nomargin" style="padding: 30px;">
			<a id="agreebtn" href="#" class="button" onClick="$.magnificPopup.close();return false;">Onaylıyorum</a>
		</div>
	</div>
</div>

