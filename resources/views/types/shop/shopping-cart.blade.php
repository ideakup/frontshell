
@if(!empty($_COOKIE["shop_cookie"]))
 
      @php

        //dd(json_decode($allcontent->whereIn('content_id',$cartcontent->pluck("id"))));
        $identifier=$_COOKIE["shop_cookie"];
        $instance="wl";
        foreach($cartcontent as $row){
          
          $matched_content=$allcontent->where('content_id',$row->id)->first();

          $itemContent = json_decode($matched_content->content);
          $price=$itemContent->price;
          if(!is_null($itemContent->discounted_price)){
              $price=$itemContent->discounted_price;
          }
          
          $tax_rate=0;
          if(!is_null($itemContent->tax_rate)){
              $tax_rate=$itemContent->tax_rate;
          }
          
          $row->name= $matched_content->title;
          $row->price= $price;
          $row->options['tax_rate']=$tax_rate;
          $row->options['img']=$itemContent->photo;


        }
          Cart::instance($instance)->store($identifier,$instance);
      @endphp
        <div id="xxxx" class="container clearfix">
          <div  class="row clearfix"> 
            <div class="col-lg-8 ">
              <h4>Sepetim</h4>
                <div class="table-responsive">
                  <table class="table cart">
                      <thead>
                        <tr>
                          <th class="cart-product-remove">&nbsp;</th>
                          <th class="cart-product-thumbnail">&nbsp;</th>
                          <th class="cart-product-name">Ürünler</th>
                          <th class="cart-product-price">Fiyat</th>
                          <th class="cart-product-taxt">Vergi</th>
                          <th class="cart-product-quantity">Adet</th>
                          <th class="cart-product-subtotal">Toplam</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($cartcontent as $row)
                              <tr id="{{$row->rowId}}" class="cart_item">
                                  <td class="cart-product-remove">
                                    <a href="#" data-rowId="{{$row->rowId}}" class="remove" title="Remove this item"><i class="icon-trash2"></i></a>
                                  </td>
                                  <td style="width: 100px;" class="cart-product-thumbnail">
                                      @php
                                        $allcontent=$allcontent->where('content_id',$row->id)->first();;
                                        $group_content=json_decode($allcontent->content);
                                      @endphp
                                      <img width="64" height="64" src="{{ url(env('APP_UPLOAD_PATH_V3').'medium/'. $group_content->photo) }}">
                                  </td>
                                  <td class="cart-product-name">
                                      {{$row->name}}
                                  </td>   
                                  <td class="cart-product-price">
                                      <span class="amount">{{number_format((float)$row->price, 2, ',', '')}} </span>
                                  </td>
                                  <td  class="cart-product-tax">
                                      <span class="amount">{{ number_format((float)($row->price/100)*$group_content->tax_rate, 2, ',', '')}}</span>
                                  </td>
                                  <td style="width: 160px;" class="cart-product-quantity">
                                      <div class="quantity clearfix">
                                        <input data-rowId="{{$row->rowId}}" type="button" value="-" class="minus">
                                        <input type="text" name="qty" value="{{ $row->qty}}" class="qty" disabled/>
                                        <input data-rowId="{{$row->rowId}}" type="button" value="+" class="plus">
                                      </div>
                                  </td>
                                  <td style="width: 160px;" class="cart-product-subtotal">
                                      <span class="amount productallcost">{{number_format((float)$row->total, 2, ',', '') }} </span> ₺
                                  </td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
                </div>
            </div>
            <div class="col-lg-4 ">
              <h4>Sipariş Özeti</h4>
               <div class="table-responsive">
                  <table class="table cart">
                    <tbody>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Vergisiz Fiyat</strong>
                        </td>
                        <td class="cart-product-name">
                          <span id="subtotal" class="amount ">{{Cart::instance('wl')->subtotal(2,",","")}}</span> ₺
                        </td>
                      </tr>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Vergi</strong>
                        </td>
                        <td class="cart-product-name">
                          <span id="tax" class="amount ">{{Cart::instance('wl')->tax(2,",","")}}</span> ₺
                        </td>
                      </tr>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Toplam</strong>
                        </td>
                        <td class="cart-product-name">
                          <span  class="amount color cart-total"><strong>{{Cart::instance('wl')->total(2,",","")}}</strong></span> ₺
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                  <a href="
                    @if(Auth::check()) 
                          {{ url('shop/checkout')}}
                     @else
                          @if($shoppingcheckout->where('shop_cookie',$_COOKIE["shop_cookie"])->count()>0)
                               {{ url('shop/checkout')}}
                          @else
                               {{ url('shop/islogin')}} 
                          @endif
                     @endif " 
                   class="button button-3d notopmargin fright">Sepeti Onayla</a>
            </div>
          </div>
       </div>
@else
       @include("types.shop.shopping-emptycart")
@endif  





@section("inline-scripts")

    <script type="text/javascript">
        var imgbaseurl="{{ url(env('APP_UPLOAD_PATH_V3').'medium/')}}";

        settings();

        $(document).ready(function(){
            $(".remove").click(function(event){
                event.preventDefault();

                var rowId=this.attributes["data-rowId"].value;
                var x = getCookie("shop_cookie");
                if(x!==""){
                  var shop_cookie=x;
                }

                $.ajax({
                url: "/api/remove",
                type:"POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val()},
                data:{
                  rowId:rowId,
                  identifier:shop_cookie,

                },
                success:function(response){
                  console.log(response);
                  if(response) {
                    var obj = jQuery.parseJSON( response );
                     ajaxDataManipulation(obj); 
                    document.getElementById(rowId).remove();


                  }
                },
               });
            });

            $(".minus").click(function(event){
                event.preventDefault();

                var rowId=this.attributes["data-rowId"].value;
                var x = getCookie("shop_cookie");
                if(x!==""){
                  var shop_cookie=x;
                }

                $.ajax({
                url: "/api/minus",
                type:"POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val()},
                data:{
                  rowId:rowId,
                  identifier:shop_cookie,

                },
                success:function(response){
                  console.log(response);
                  if(response) {    
                    var obj = jQuery.parseJSON( response );
                     ajaxDataManipulation(obj);
                      

                  }
                },
               });
            });

            $(".plus").click(function(event){
                event.preventDefault();

                var rowId=this.attributes["data-rowId"].value;
                var x = getCookie("shop_cookie");
                if(x!==""){
                  var shop_cookie=x;
                }

                $.ajax({
                url: "/api/plus",
                type:"POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val()},
                data:{
                  rowId:rowId,
                  identifier:shop_cookie,

                },
                success:function(response){
                  if(response) {    
                    var obj = jQuery.parseJSON( response );
                    console.log(obj);
                   ajaxDataManipulation(obj);
                                

                  }
                },
               });
            });

            function getCookie(cname) {
              var name = cname + "=";
              var decodedCookie = decodeURIComponent(document.cookie);
              var ca = decodedCookie.split(';');
              for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' '){
                  c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
                }
              }
              return "";
            }

            function ajaxDataManipulation(obj){
                    $('.insideofcart').remove();
                    $('.cartbottom').remove();
                    settings();
                    
                     var i=0;
             
                    for (const [key, value] of Object.entries(obj[0])) {

                       var productsubtotal = document.getElementsByClassName("productallcost");
                        var productqty = document.getElementsByClassName("qty");
                        if(value["options"]["tax_rate"]){
                          var rowtotal=value["subtotal"]+((((value["options"]["tax_rate"])*value["subtotal"])/100));
                        }else{
                          var rowtotal=value["subtotal"];
                        }
                     $("#cartcontent ").append('\
                      <div id="insideofcart" class="top-cart-items insideofcart">\
                        <div class="top-cart-item clearfix">\
                          <div class="top-cart-item-image">\
                            <a href="#"><img src="'+imgbaseurl+`${value["options"]["img"]}`+'"/></a>\
                          </div>\
                          <div class="top-cart-item-desc">\
                            <a href="#">'+`${value["name"]}`+'</a>\
                            <span class="top-cart-item-price">'+parseFloat(rowtotal).toFixed(2)+'</span>\
                            <span id="iocqty" class="top-cart-item-quantity">x '+`${value["qty"]}`+'</span>\
                          </div>\
                        </div>\
                      </div>\
                      ');
                       
                       
                       
                        
                        productsubtotal[i].textContent=parseFloat(rowtotal).toFixed(2);
                        console.log( value["options"]["tax_rate"]);
                        productqty[i].setAttribute('value',`${value["qty"]}`);;            
              
                        i=i+1;
                    }
                    $("#cartcontent ").append('\
                      <div class="top-cart-action clearfix cartbottom">\
                        <span class="fleft top-checkout-price cart-total">'+obj[1][2]+'</span>\
                           <a href="cart"><button class="button button-3d button-small nomargin fright">Sepete Git</button></a>\
                      </div>\
                      ');

                    $('#subtotal').text(obj[1][0]);
                    $('#tax').text(obj[1][1]);
                    $('.cart-total').text(obj[1][2]);
                    $('#top-cart-count').text(obj[1][3]);
            }
                  
            $( "#logoutBtn" ).click(function() {
                $('#logoutForm').submit();
            });

        });

        function settings(){
          var a = document.getElementsByClassName("qty");
          for(var i=0; i<a.length;i++){
              if(a[i].value==1){
                document.getElementsByClassName("minus")[i].disabled = true;
              }else{
                document.getElementsByClassName("minus")[i].disabled = false;
              }
          }
        }
    </script>
@endsection    
