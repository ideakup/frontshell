@php

     // $order_id = Request::segment(3);
  
@endphp
@if(!empty($_COOKIE["shop_cookie"]))
 
      @php
        $identifier=$_COOKIE["shop_cookie"];
        $instance="sub";
        $cart_content=Cart::instance($instance)->content()->first();

        foreach($cartsubcontent as $row){
          $matched_content=$allcontent->where('content_id',$row->id)->where('lang_code',$lang)->first();
          $itemContent = json_decode($matched_content->content);
          $periodandprice = collect($itemContent->periodandprice);

          if($cart_content->options->subscriptionperiod != 0){
            $price=$periodandprice[$cart_content->options->subscriptionperiod];
          }else{
            $price=$cart_content->price;
          }
          $tax_rate=0;
          if(!empty($itemContent->tax_rate)){
              $tax_rate=$itemContent->tax_rate;
          }
          $row->name= $matched_content->title;
          $row->price= $price;
          $row->options['tax_rate']=$tax_rate;

        }

      Cart::instance($instance)->store($identifier,$instance);
      @endphp
        <div id="xxxx" class="container clearfix">
          <div  class="row clearfix"> 
            <div class="col-lg-8 ">
              <h4>Sepetim</h4>
                <div class="table-responsive">
                  <table class="table cart">
                      <thead>
                        <tr>
                          @if(Request::segment(4) == 'extend' || is_null(Request::segment(3)))
                            <th class="cart-product-name">Ürün</th>
                            <th class="cart-product-subscriptionperiod">Abonelik @if(!is_null(Request::segment(3))) Uzatma  @endif Süresi</th>
                          @elseif(Request::segment(4) == 'upgrade')
                            <th class="cart-product-name">Ürün</th>
                            <th class="cart-product-name">Kalan Abonelik  Süresi</th>
                          @elseif(Request::segment(4) == 'subscribe')
                            <th class="cart-product-name">Ürün</th>
                            <th class="cart-product-name">Abonelik Süresi</th>
                          @endif
                          <th class="cart-product-price">Fiyat</th>
                          <th class="cart-product-taxt">Vergi</th>
                          <th class="cart-product-subtotal">Toplam</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($cartsubcontent as $row)
                              @php
                              
                                $allcontent=$allcontent->where('content_id',$row->id)->where('lang_code',$lang)->first();;
                                $group_content=json_decode($allcontent->content);
                              @endphp
                              <tr id="{{$row->rowId}}" class="cart_item">
                                  @if(Request::segment(4) == 'extend' || is_null(Request::segment(3)))
                                    <td  class="cart-product-name">
                                        {{$row->name}}
                                    </td>   
                                    <td class="cart-product-subscriptionperiod">
                                        <select class="form-control m-select2" id="periodandprice" name="periodandprice" >
                                            @foreach ($periodandprice as $key => $colv)
                                                <option value="{{ $key }}"    
                                                @if($key == $row->options->subscriptionperiod) selected @endif  
                                                    >{{$key}} Ay
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                  @elseif(Request::segment(4) == 'upgrade')
                                    <td  class="cart-product-name">
                                          {{$row->name}}
                                    </td>
                                    @php
                                      $user_order=App\ShoppingOrder::find(Request::segment(3));
                                      $dt = \Carbon\Carbon::parse($user_order->end_date);
                                      $now =\Carbon\Carbon::now();
                                      $diff_months = $dt->diffInMonths($now);
                                      $asd = $dt->copy()->subMonths($diff_months);
                                      $diff_days = $now->diffInDays($asd);
                                      $free_days =$diff_days%15;
                                      //dd($asd);
                                    @endphp 
                                    <td class="cart-product-subscriptionperiod">
                                         @if($diff_months >0){{$diff_months}} Ay @endif @if($diff_days >0) {{$diff_days}} Gün @endif <br>
                                         ({{$free_days}} Gün Ücretsiz)
                                    </td>
                                  @elseif(Request::segment(4) == 'subscribe')
                                    @php
                                      $subscription_contents = App\Content::where('type','group-productsubscription')->get();
                                    @endphp
                                    <td  class="cart-product-name">
                                        <select class="form-control m-select2" id="selectpackage" name="selectpackage" >
                                            @foreach ($subscription_contents as $subscription_content)
                                                <option value="{{ $subscription_content->variableLang($lang)->content_id}}"    
                                                @if($subscription_content->variableLang($lang)->content_id == $row->id) selected @endif  
                                                    >{{$subscription_content->variableLang($lang)->title}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>   
                                    <td class="cart-product-subscriptionperiod">
                                        <select class="form-control m-select2" id="periodandprice" name="periodandprice" >
                                            @foreach ($periodandprice as $key => $colv)
                                                <option value="{{ $key }}"    
                                                @if($key == $row->options->subscriptionperiod) selected @endif  
                                                    >{{$key}} Ay
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                  @endif
                                  <td class="cart-product-price">
                                      <span id="cart-product-price" class="amount">{{number_format((float)$row->price, 2, ',', '')}} </span>
                                  </td>
                                  <td  class="cart-product-tax">
                                      <span id="cart-product-tax" class="cart-product-tax">{{ number_format((float)($row->price/100)*$group_content->tax_rate, 2, ',', '')}}</span>
                                  </td>
                                  <td style="width: 160px;" class="cart-product-subtotal sub-cart-total">
                                      <span class="amount productallcost">{{number_format((float)$row->total, 2, ',', '') }} </span> ₺
                                  </td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
                </div>
                @if(is_null(Request::segment(3)))
                  <div class="style-msg alertmsg">
                    <div class="sb-msg"><i class="icon-warning-sign"></i>Domain sahibi iseniz giriniz. Bu işlemi abonelik sonrasında da yapabilirsiniz
                    </div>
                  </div>
                
                  <div class="form-group row">
                      <label for="domainlabel" class="col-sm-2 col-form-label">Domain</label>
                      <div class="col-sm-10">
                        <input type="domain" class="form-control" id="domain">
                      </div>
                  </div>
                @else
                  @php
                    $order=App\ShoppingOrder::find(Request::segment(3));
                    $domain = App\Domain::where('order_no',$order->order_no)->first();
                  @endphp
                  <div class="form-group row">
                      <label for="domainlabel" class="col-sm-2 col-form-label">Domain</label>
                      <div class="col-sm-10">
                        <input  type="domain" class="form-control" value="{{$domain->domain_name}}" id="domain" readonly>
                      </div>
                  </div>
                @endif
            </div>
            <div class="col-lg-4 ">
              <h4>Sipariş Özeti</h4>
               <div class="table-responsive">
                  <table class="table cart">
                    <tbody>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Vergisiz Fiyat</strong>
                        </td>
                        <td class="cart-product-name">
                          <span id="subtotal" class="amount ">{{Cart::instance($instance)->subtotal(2,",","")}}</span> ₺
                        </td>
                      </tr>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Vergi</strong>
                        </td>
                        <td class="cart-product-name">
                          <span id="tax" class="amount ">{{Cart::instance($instance)->tax(2,",","")}}</span> ₺
                        </td>
                      </tr>
                      <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Toplam</strong>
                        </td>
                        <td class="cart-product-name">
                          <span id="cart-total" class="amount color sub-cart-total"><strong>{{Cart::instance($instance)->total(2,",","")}}</strong></span> ₺
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                  @if(is_null(Request::segment(3)))
                    <button id="confirmcartsub" class="button button-3d notopmargin fright"> Sepeti Onayla </button>
                  @else
                    <a href="{{ url('shop/checkoutsub') }}/{{Request::segment(3)}}/{{Request::segment(4)}}" class="button button-3d notopmargin fright">Sepeti Onayla</a>
                  @endif
            </div>
          </div>
       </div>
@else
       @include("types.shop.shopping-emptycart")
@endif  

@section("inline-scripts")
    <script type="text/javascript">
      $(document).ready(function(){

        $("#periodandprice").change(function(event){
          event.preventDefault();

          var rowId=$(".cart_item").attr('id');
          var period = $("#periodandprice" ).val();
          var contvarid = {{$allcontent->id}};
          var domain = $("#domain" ).val();
          var subcahangetype = 'period';
          var x = getCookie("shop_cookie");
          if(x!==""){
            var shop_cookie=x;
          }

            $.ajax({
            url: "/api/subchange",
            type:"POST",
            headers: { 'X-CSRF-TOKEN': $('#token').val()},
            data:{
              subcahangetype:subcahangetype,
              rowId:rowId,
              identifier:shop_cookie,
              period:period,
              domain:domain,
              contvarid:contvarid,

            },
            success:function(response){
              if(response) {    
                var obj = jQuery.parseJSON( response );
                console.log(obj);
                 ajaxDataManipulation(obj);
              }
            },
           });
        });
        $("#confirmcartsub").click(function(event){
            event.preventDefault();

            var domain = $("#domain" ).val();
            var rowId=$(".cart_item").attr('id');
            var contvarid = {{$allcontent->id}};
            var period = $("#periodandprice" ).val();
            var x = getCookie("shop_cookie");
            if(x!==""){
              var shop_cookie=x;
            }

            $.ajax({
                  url: "/api/domain",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  rowId:rowId,
                  domain:domain,
                  period:period,
                  identifier:shop_cookie,
                  contvarid:contvarid,
                  
                },
                success:function(response){
                  location.href = "{{url('/shop/checkoutsub')}}"
                },
           });


            if(x!==""){
              var shop_cookie=x;
            }
        });
        $("#selectpackage").change(function(event){
                event.preventDefault();
                var subcahangetype = 'packagechange';
                var package_id = $("#selectpackage" ).val();
                var x = getCookie("shop_cookie");
                var domain = $("#domain" ).val();
                @if(!empty(Request::segment(3)))
                  var order_id = {{Request::segment(3)}};
                @endif
                if(x!==""){
                  var shop_cookie=x;
                }
                
                $.ajax({
                  url: "/api/subchange",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  subcahangetype:subcahangetype,
                  package_id:package_id,
                  identifier:shop_cookie,
                  domain:domain,
                  userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
                },
                success:function(response){
                  window.location.reload();
                },
               });
        });
        function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' '){
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }
        function ajaxDataManipulation(obj){
          $('#cart-product-price').text(obj[1][0]);
          $('#cart-product-tax').text(obj[1][1]);
          $('#subtotal').text(obj[1][0]);
          $('#tax').text(obj[1][1]);
          $('.sub-cart-total').text(obj[1][2]);
        }     
        $( "#logoutBtn" ).click(function() {
            $('#logoutForm').submit();
        });
      });
    </script>
@endsection    
