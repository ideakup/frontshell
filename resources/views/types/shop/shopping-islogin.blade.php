
<div class="d-flex justify-content-center">
	<div class="tabs divcenter nobottommargin clearfix" id="tab-login-register" data-active="{{old('tab_number')}}" style="max-width: 500px; ">

		<ul class="tab-nav tab-nav2 center clearfix">
			<li class="inline-block"><a href="#tab-login">Giriş Yap</a></li>
			<li class="inline-block"><a href="#tab-register">Üye Ol</a></li>
			<li class="inline-block "><a href="#tab-withoutLogin">Üye Olmadan Devam Et</a></li>
		</ul>
		<div class="tab-container">

			<div class="tab-content clearfix" id="tab-login">
				<div class="card nobottommargin">
					<div class="card-body" style="padding: 40px;">
						<form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="post">
							 	{{ csrf_field() }} 
							<input type="hidden"  name="tab_number" value="1" />	          								 
							<h3>Giriş Yap</h3>
							<div class="col_full">
								<label for="login-form-username">E-posta:</label>
								<input type="email" id="email" name="email" placeholder="E-posta" required class="form-control" value="{{old('email')}}" />
							</div>
							<div class="col_full">
								<label for="login-form-password">Şifre:</label>
								<input type="password" id="password" name="password" placeholder="Şifre" required class="form-control" />
							</div>
							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Giriş Yap</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="tab-content clearfix active" id="tab-register">
				<div class="card nobottommargin">
					<div class="card-body" style="padding: 40px;">
						<h3>Üye Ol</h3>

						<form id="register-form" name="register-form" class="nobottommargin" action="{{url('sta/manualregister')}}" method="post">
								 {{ csrf_field() }}
						 	<input type="hidden"  name="tab_number" value="2" />
							<div class="form-group @if ($errors->has('firstname')) has-danger @endif" >
								<label for="register-form-name">İsim:</label>
								<input type="text" id="register-form-firstname" name="firstname" value="{{old('firstname')}}" class="form-control" />
								@if ($errors->has('firstname'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('firstname') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('lastname')) has-danger @endif" >
								<label for="register-form-lastname">Soyisim:</label>
								<input type="text" id="register-form-lastname" name="lastname" value="{{old('lastname')}}" class="form-control" />
								@if ($errors->has('lastname'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('lastname') }}</div>
			                    @endif
							</div>

							<div class="form-group @if ($errors->has('email')) has-danger @endif" >
								<label for="register-form-email">E-Posta Adresi :</label>
								<input type="text" id="register-form-email" name="email" value="{{old('email')}}" class="form-control" />
								@if ($errors->has('email'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('email') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('password')) has-danger @endif" >
								<label for="register-form-password">Şifre Belirle:</label>
								<input type="password" id="register-form-password" name="password" value="" class="form-control" />
								@if ($errors->has('password'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('password') }}</div>
			                    @endif
							</div>

							<div class="form-group @if ($errors->has('repassword')) has-danger @endif" >
								<label for="register-form-repassword">Şifre Tekrar:</label>
								<input type="password" id="register-form-repassword" name="repassword" value="" class="form-control" />
								@if ($errors->has('repassword'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('repassword') }}</div>
			                    @endif
							</div>
							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Kayıt Ol</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="tab-content clearfix" id="tab-withoutLogin">
				<div class="card nobottommargin">
					<div class="card-body" style="padding: 40px;">
						<form id="withoutLogin-form" name="withoutLogin-form" class="nobottommargin" action="{{url('sta/infowithoutlogin')}}" method="post">
								 {{ csrf_field() }}
					 		<input type="hidden"  name="tab_number" value="3" />

							<h3>Yeni Adres Oluştur</h3>

							<div class="form-group @if ($errors->has('name')) has-danger @endif" >
								<label for="withoutLogin-form-name">İsim:</label>
								<input type="text" id="withoutLogin-form-name" name="name"  class="form-control" required value="{{ old('name') }}"/>
								@if ($errors->has('name'))
				                    <div id="phone-error" class="form-control-feedback">{{ $errors->first('name') }}</div>
				                @endif
							</div>
							<div class="form-group @if ($errors->has('lastname')) has-danger @endif" >
								<label for="withoutLogin-form-lastname">Soyisim:</label>
								<input type="text" id="withoutLogin-form-lastname" name="lastname" value="{{ old('lastname') }}" required class="form-control" />
								@if ($errors->has('lastname'))
		                        	<div id="lastname-error" class="form-control-feedback">{{ $errors->first('lastname') }}</div>
		                    	@endif
							</div>
							<div class="form-group @if ($errors->has('email')) has-danger @endif" >
								<label for="withoutLogin-form-email">E-posta:</label>
								<input type="text" id="withoutLogin-form-email" name="email" value="{{ old('email') }}" class="form-control" required />

								@if ($errors->has('email'))
			                        <div id="email-error" class="form-control-feedback">{{ $errors->first('email') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('identity_no')) has-danger @endif" >
								<label for="identity_no">TC Kimlik:</label>
								<input type="text" id="identity_no" name="identity_no" value="{{ old('identity_no') }}" class="form-control " required />

								@if ($errors->has('identity_no'))
			                        <div id="identity_no-error" class="form-control-feedback">{{ $errors->first('identity_no') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('phone')) has-danger @endif" >
								<label for="phone">Telefon:</label>

								<input type="text" id="phone" name="phone" placeholder="55XXXXXXXX" class="form-control " value="{{old('phone')}}" required />

								@if ($errors->has('phone'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('phone') }}</div>
			                    @endif
							</div>																	
							<div class="form-group @if ($errors->has('city')) has-danger @endif">
								<label for="city">İl:</label><br>
								<select style="width: 100%;" class="form-control" name="city" id="city" required>
									<option value="">Seçiniz...</option>                                          
			                        @foreach ($cities as $city)
			                            <option @if( $city->variable->name == old('city') ) selected @endif > {{ $city->variable->name }} </option>
			                        @endforeach
								</select>
								@if ($errors->has('city'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('city') }}</div>
			                    @endif
							</div>

							<div id="towndiv" class="form-group @if ($errors->has('town')) has-danger @endif">
								<label for="town">İlçe:</label><br>
								<select style="width: 100%;" class="form-control" name="town" id="town" required >
									<option value="">Seçiniz...</option>
				                        @foreach ($counties as $county)
				                            <option @if( $county->variable->name == old('town') ) selected @endif >  {{ $county->variable->name }} </option>
				                        @endforeach
								</select>
								@if ($errors->has('town'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('town') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('adress')) has-danger @endif">
								<label for="adress">Adres:</label>
								<textarea class="form-control" id="withoutLogin_form_adress" name="adress" rows="3" >{{ old('adress') }}</textarea>
								@if ($errors->has('adress'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adress') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('adressname')) has-danger @endif">
								<label for="adressname">Adres İsmi:</label>
								<input type="text" id="adressname" name="adressname" class="form-control" value="{{ old('adressname') }}" required/>

								@if ($errors->has('adressname'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('adressname') }}</div>
			                    @endif
							</div>
							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="withoutLogin_form_submit" name="withoutLogin_form_submit" value="withoutLogin">Devam et</button>
							</div>									
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@section("inline-scripts")
  <script type="text/javascript">

 


    $(document).ready(function() {
        $('#city').select2();
        $('#town').select2();

        $('#city').change(function(){ 
            var cityname = $(this).val();
            console.log(cityname);

            $.ajax({
                  url: "/api/city",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  cityname:cityname,
           
                },
                success:function(response){
                   console.log(response[0]['name']);
                   
                   $('#town').empty(); 
          
                   	for (let i = 0; i < response.length; i++) {
						$("#town").append('\
								<option>'+response[i]['name']+'</option>\
	          		   ');
					}
             	 	
                },
        	});
		});
	});


  </script>
@endsection