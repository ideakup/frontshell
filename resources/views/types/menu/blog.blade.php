@php
	$blog_contents = App\Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->where('content.type', 'group-blog')->where('slug', '!=' ,'prototype_blog')->where('lang_code',$lang)
                           ->where('deleted', 'no')->where('status', 'active')->orderBy('new_top_has_sub.created_at', 'desc')->get();

    $blog_contents = $blog_contents->unique();                      
                     
@endphp
<div class="section {{ $menu->variable->slug }} notopmargin nobottommargin">
    <div class="container clearfix"> 
        <div class="row">
    	 	@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                <div class="postcontent col-lg-9 
                @if(json_decode($menu->topHasSub->first()->props)->sidebar == 'left') 
                    order-lg-last
                @endif    
                ">
                <div class="row">         
            @endif
	        	@foreach($blog_contents as $b_c)
	        		@php
	        			$group_content=json_decode($b_c->content);
	        			$href_link = '/blog/'.$b_c->slug;
						$title = $b_c->title;
						$created_at = $b_c->created_at;

						if(!empty($group_content->photo)){
							$photo = $group_content->photo; 
						}
						if(!empty($group_content->short_content)){
							$short_content = $group_content->short_content;
						}
						if(!empty($group_content->short_content)){
							$toogle = $group_content->toogle;
						}
	        		@endphp
	        		<div class="col-lg-4">
	        			@include('types.posttypes.blog')
					</div>
				@endforeach
			@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                </div>
                </div>
                @include('types.sidebar.sidebar')
            @endif	
		</div>
	</div>
</div>
	