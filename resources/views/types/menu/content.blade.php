@php
    $contentsths = $menu->topHasSubContent;
@endphp
<!--header-stick -->

@foreach ($contentsths as $ths)
    @php
        $ths_props=json_decode($ths->props);
    @endphp
    @if($ths_props->section_props_section == 'header-stick')
        @php
            if($ths->sub_model=='Content'){
                $content = $ths->thatElementForContent;
            }
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
            
        @endphp
        @if(starts_with($content->type,'section'))
            @include('types.menupartials.section')
        @endif
    @endif
@endforeach

<!-- Container-->

@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
    <div class="section  {{ $slug }} notopmargin nobottommargin">
        <div class="container clearfix"> 
            <div class="row">
                <div class="postcontent col-lg-9 
                @if(json_decode($menu->topHasSub->first()->props)->sidebar == 'left') 
                    order-lg-last
                @endif    
                ">
                <div class="row">
@endif
@if(count($contentsths) == 0)
    <div class="col-lg-12">
        <p>hebele hübele</p>
    </div>
@endif
@foreach ($contentsths as $ths)
    @php
        $ths_props=json_decode($ths->props);
    @endphp
    @if($ths_props->section_props_section == 'container')
        @php
            if($ths->sub_model=='Content'){
                $content = $ths->thatElementForContent;
            }
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
        @endphp        
        @if(starts_with($content->type,'section'))
            @include('types.menupartials.section')
        @endif
    @endif
@endforeach
@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                </div>
            </div>
            @include('types.sidebar.sidebar')
        </div>
    </div>
</div>

@endif

<!--Footer-stick -->

@foreach ($contentsths as $ths)
    @php
        $ths_props=json_decode($ths->props);
    @endphp
    @if($ths_props->section_props_section == 'footer-stick')
    @php
        if($ths->sub_model=='Content'){
            $content = $ths->thatElementForContent;
        }
        
        if (empty($content->variableLang($lang))) {
            $contVariable = $content->variable;
        }else{
            $contVariable = $content->variableLang($lang);
        }
    @endphp   
        @if(starts_with($content->type,'section'))
            @include('types.menupartials.section')
        @endif
    @endif
@endforeach