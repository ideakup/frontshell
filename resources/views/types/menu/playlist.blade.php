@php
	$playlist_contents = App\Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->where('content.type', 'group-playlist')->where('lang_code',$lang)
                           ->where('deleted', 'no')->where('status', 'active')->orderBy('new_top_has_sub.created_at', 'desc')->get();


    $playlist_contents = $playlist_contents->unique();                      
                     
@endphp
<div class="section {{ $menu->variable->slug }} notopmargin nobottommargin">
    <div class="container clearfix"> 
        <div class="row">
    	 	@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                <div class="postcontent col-lg-9 
                @if(json_decode($menu->topHasSub->first()->props)->sidebar == 'left') 
                    order-lg-last
                @endif    
                ">
                <div class="row">         
            @endif
	        	@foreach($playlist_contents as $p_c)
	        		@php
	        			$group_content=json_decode($p_c->content);
	        			$href_link = '/playlist/'.$p_c->slug;
	        			$photo = $group_content->photo; 
						$title = $p_c->title;
	        		@endphp
	        		<div class="col-lg-4">
	        			<div class="grid-inner">
						    @if(!empty($photo))
						        <div class="entry-image">
						            <a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$photo }}" alt="{{$title}}"></a>
						        </div>
						    @endif
						    <div class="entry-title">
						        <h2><a href="{{$href_link}}">{{$title}}</a></h2>
						    </div>
						</div>
					</div>
				@endforeach
			@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                </div>
                </div>
                @include('types.sidebar.sidebar')
            @endif	
		</div>
	</div>
</div>
	