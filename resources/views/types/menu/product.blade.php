@php
	$product_contents = App\Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->where('type', 'group-product')->where('slug', '!=' ,'prototype_product')->where('lang_code',$lang)
                           ->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    $product_contents = $product_contents->unique();       

@endphp
<div class="section {{ $menu->variable->slug }} notopmargin nobottommargin">
    <div class="container clearfix"> 
        <div class="row">
        	@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                <div class="postcontent col-lg-9 
                @if(json_decode($menu->topHasSub->first()->props)->sidebar == 'left') 
                    order-lg-last
                @endif    
                ">
                <div class="row">
                      
            @endif
        	@foreach($product_contents as $p_c)

        		@php
        			$group_content=json_decode($p_c->content);
                    $slug =$p_c->slug;
        			$href_link = 'product/'.$p_c->slug;
				    $title = $p_c->title;
                    if(!empty($group_content->photo)){
                        $photo = $group_content->photo; 
                    } 
                    if(!empty($p_c->stock)){
                        $stock =  $p_c->stock;
                    }

        		@endphp
        		<div class="col-lg-4">
					@include('types.posttypes.product')
				</div>
			@endforeach
			@if (json_decode($menu->topHasSub->first()->props)->sidebar != 'no') 
                </div>
                </div>
                @include('types.sidebar.sidebar')
            @endif	
		</div>
	</div>
</div>
	