@php
	$user = Auth::user();
	$user_orders=App\ShoppingOrder::where('user_id',$user->id)->orderBy('end_date','desc')->orderBy('id','desc')
	->where('product_type','sub')
	->get()->unique('order_no');


	$aylar = array(1=>"Ocak",2=>"Şubat",3=>"Mart",4=>"Nisan",5=>"Mayıs",6=>"Haziran",7=>"Temmuz",8=>"Ağustos",9=>"Eylül",10=>"Ekim",11=>"Kasım",12=>"Aralık");
	$kisaaylar = array(1=>"Oca",2=>"Şub",3=>"Mar",4=>"Nis",5=>"May",6=>"Haz",7=>"Tem",8=>"Ağu",9=>"Eyl",10=>"Eki",11=>"Kas",12=>"Ara");

	
@endphp
@if(!empty( Auth::user()))
	<div class="d-flex justify-content-center">
        <div class="col-lg-10 ">
        	@foreach($user_orders as $user_order)
        		@if($user_order->payment_information != 'free')
	        		@php
	        			$package_id = array_values(json_decode($user_order->order_content, true))[0]['id'];
	        			$domain = App\Domain::where('order_no',$user_order->order_no)->first();
	        		@endphp
					<div class="card">
						<div class="card-body">
							<table class="table order">
								<thead>
									<tr>
										<th class="order-product-name">Ürün</th>
										<th class="order-product-name">Domain</th>
										<th class="order-product-enddate">Bitiş Tarihi</th>
									</tr>
								</thead>
								<tbody>
									@foreach(json_decode($user_order->order_content) as $row)		
										@php
											$dt = \Carbon\Carbon::parse($user_order->end_date);
										@endphp					
										<tr  class="order_item">		
											<td  class="order-product-name-td">
						                       {{$row->name}}
						                    </td>			                
											<td  class="order-product-name-td">
						                       {{$domain->domain_name}}
						                    </td>
					                       	<td class="order-product-enddate">
						                        <span class="enddate">{{$dt->day}} {{$aylar[$dt->month]}} {{$dt->year}}</span>
					                       	</td>
					                       	<td class="order-product-upgrade">
					                       		@if($package_id != '15')
						                        	<a data-slug="{{$user_order->id}}-{{$row->id}}-upgrade" href="#" class="btn btn-danger w-100 btn-sm extend_upgrade ">Paketi Yükselt</a>
						                       	@endif
					                       	</td>
					                       	<td class="order-product-extend">
						                        <a data-slug="{{$user_order->id}}-{{$row->id}}-extend" href="#" class="btn btn-danger w-100 btn-sm extend_upgrade ">Uzat</a>
					                       	</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="card-body">
							<div data-readmore="true" data-readmore-maskcolor="#DDD" data-readmore-masksize="100%" data-readmore-trigger-open="<i class='icon-angle-down i-plain i-large m-0 float-none'></i>" data-readmore-trigger-close="<i class='icon-angle-up i-plain i-large m-0 float-none'></i>">
								@php
									$extends=App\ShoppingOrder::where('order_no',$user_order->order_no)->orderBy('end_date','desc')->orderBy('id','desc')->get();
								@endphp
								<table class="table order">
									<thead>
										<tr>
											<th class="order-product-name">Ürün</th>
											<th class="order-product-subtotal">Abonelik Süresi</th>
											<th class="order-product-price">Fiyat</th>
											<th class="order-product-price">Vergi</th>	
											<th class="order-product-subtotal">Tutar</th>
											<th class="order-product-subtotal">Ödeme</th>
											<th class="order-product-enddate">Bitiş Tarihi</th>
										</tr>
									</thead>
									@foreach($extends as $extend)
										@if($extend->payment_information != 'free')
											<tbody>
												@php
													$payment_info = explode(",",$extend->payment_information);
						            				$instalment_totalpayment =explode("_",$payment_info[4]);
						            				$total_payment = $instalment_totalpayment[1];
												@endphp
												@foreach(json_decode($extend->order_content) as $row)		
													@php
														$dt = \Carbon\Carbon::parse($extend->end_date);
													@endphp					
													<tr class="order_item">					                
														<td id="{{$row->id}}" class="order-product-name-td">
									                       {{$row->name}}
									                    </td>
									                    @if($row->options->subscriptionperiod != 0)
										                    <td class="order-product-extendpriod">
										                        <span class="extendpriod">{{$row->options->subscriptionperiod}} Ay</span>
									                       </td>
									                    @else
									                    	<td class="order-product-extendpriod">
										                        <span class="extendpriod">Paket Yükseltmesi</span>
									                       </td>
									                    @endif
														<td class="cart-product-pricee">
					                                      <span class="price">{{number_format((float)$row->price, 2, ',', '')}}</span>
					                                  	</td>
					                                  	<td  class="cart-product-tax">
					                                      <span class="amount">{{ number_format((float)($row->price/100)*$row->options->tax_rate, 2, ',', '')}}</span>
					                                  	</td>
						                                @php
						                                  	$rowtotal=$row->price+(($row->price)*($row->options->tax_rate)/100);
						                                  	$rowtotal1=$rowtotal*$row->qty;
						                                @endphp
														<td class="order-product-subtotal">
									                        <span class="amount">{{number_format((float)$rowtotal1, 2, ',', '')}}</span>
								                       </td>
								                       <td class="order-product-subtotal">
									                        <span class="amount">{{number_format((float)$total_payment, 2, ',', '')}}</span>
								                       </td>
								                       <td class="order-product-enddate">
									                        <span class="enddate">{{$dt->day}} {{$aylar[$dt->month]}} {{$dt->year}}</span>
								                       </td>
													</tr>
												@endforeach
											</tbody>
										@else
											<tbody>
												@php
													$dt = \Carbon\Carbon::parse($extend->end_date);
												@endphp					
												<tr class="order_item">					                
													<td class="order-product-name-td">
								                       {{json_decode($extend->order_content)->package_name}}
								                    </td>
							                    	<td class="order-product-extendpriod">
								                        <span class="extendpriod">Deneme</span>
							                       	</td>
													<td class="cart-product-pricee"></td>
				                                  	<td  class="cart-product-tax"></td>
													<td class="order-product-subtotal"> </td>
							                       	<td class="order-product-subtotal">
       						                        	<span class="badge bg-warning text-dark py-1 px-2">Free</span>
							                       	</td>
							                       	<td class="order-product-enddate">
								                        <span class="enddate">{{$dt->day}} {{$aylar[$dt->month]}} {{$dt->year}}</span>
							                       	</td>
												</tr>
											</tbody>
										@endif
									@endforeach
								</table>
								<a href="#" class="read-more-trigger"></a>
							</div>
						</div>
					</div>
				@else
					@php
	        			$sub_name = json_decode($user_order->order_content)->package_name;
	        			$domain = App\Domain::where('order_no',$user_order->order_no)->first();
	        		@endphp
					<div class="card">
						<div class="card-body">
							<table class="table order">
								<thead>
									<tr>
										<th class="order-product-name">Ürün</th>
										<th class="order-product-name">Domain</th>
										<th class="order-product-enddate">Bitiş Tarihi</th>
									</tr>
								</thead>
								<tbody>
									@php
										$dt = \Carbon\Carbon::parse($user_order->end_date);
									@endphp					
									<tr  class="order_item">		
										<td  class="order-product-name-td">
					                       {{$sub_name}}
					                    </td>			                
										<td  class="order-product-name-td">
					                       {{$domain->domain_name}}
					                    </td>
				                       	<td class="order-product-enddate">
					                        <span class="enddate">{{$dt->day}} {{$aylar[$dt->month]}} {{$dt->year}}</span>
					                        <span class="badge bg-warning text-dark py-1 px-2">Free</span>
				                       	</td>
				                       	<td class="order-product-extend">
					                        <a data-slug="{{$user_order->id}}-{{$domain->package_id}}-subscribe" href="#" class="btn btn-danger w-100 btn-sm extend_upgrade ">Abone Ol</a>
				                       	</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				@endif
        	@endforeach
       	</div>
	</div>
@endif


@section("inline-scripts")
  	<script type="text/javascript">
        $(document).ready(function(){
            $(".extend_upgrade").click(function(event){
                event.preventDefault();

                var order_content_id=this.attributes["data-slug"].value;
                var x = getCookie("shop_cookie");

                if(x!==""){
                  var shop_cookie=x;
                }
                var lang = "{{$lang}}";

                $.ajax({
                  url: "/api/extend_upgrade",
                  type:"POST",
                  headers: { 'X-CSRF-TOKEN': $('#token').val()},
                  data:{
                  order_content_id:order_content_id,
                  lang:lang,
                  identifier:shop_cookie,
                  userid:"@if(!empty(Auth::user())){{Auth::user()->id}}@endif",
                },
                success:function(response){
                	var obj = jQuery.parseJSON( response );
                  		location.href = "{{url('/shop/cartsub')}}"+"/"+obj['order_id']+"/"+obj['function_type']
                	
                },
               });
            });
            function getCookie(cname) {
              var name = cname + "=";
              var decodedCookie = decodeURIComponent(document.cookie);
              var ca = decodedCookie.split(';');
              for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                  c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
                }
              }
              return "";
            }
            function setCookie(cname, cvalue) {
              document.cookie = cname + "=" + cvalue;
            }
            $(document).ready(function(){
                  $( "#logoutBtn" ).click(function() {
                      $('#logoutForm').submit();
                  });
            });
        });
  	</script>
@endsection