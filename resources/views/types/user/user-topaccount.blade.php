@php
	$user = Auth::user();
@endphp
	

@if(Auth::check())
	<div id="top-user" class="header-misc-icon">
    <a href="#" id="top-user-trigger"><i class="icon-user4"></i></a>
    <div id="usercontent" class="top-user-content">
        <h5 class="text-center">{{$user->firstname." ".$user->lastname }}</h5>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item tleft" href="{{ url('user/profile') }}"><i class="far fa-id-card"></i> Profil ve Ayarlar </a>
			<a class="dropdown-item tleft" href="{{ url('user/orders') }}"><i class="fas fa-box-open"></i> Siparişlerim </a>
			<a class="dropdown-item tleft" href="{{ url('user/subscription') }}"><i class="fas fa-recycle"></i> Aboneliklerim </a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item tleft" id="logoutBtn" href="#"><i class="icon-signout"></i> Logout </a>
			<form id="logoutForm" action="{{ url('sta/manuallogout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
            </form>
    </div>
</div><!-- #top-cart end -->
@else

<div id="top-user" class="header-misc-icon">
    <a href="#" id="top-user-trigger"><i class="icon-user4"></i></a>
    <div id="usercontent" class="top-user-content">
       	<a class="dropdown-item tleft" href="{{ url('user/login') }}">Giriş</a>
		<div class="dropdown-divider"></div>
		<a class="dropdown-item tleft" href="{{ url('user/login') }}">Kayıt Ol</a>	
    </div>
</div><!-- #top-cart end -->
  
	
@endif			





			