@php
	$user=null;
	if(!empty(Request::segment(3))){
		$user=App\user::where('remember_token',Request::segment(3))->first();
	}
@endphp
@if(!is_null($user))
	<div class="container clearfix">

		<div class="tabs divcenter nobottommargin clearfix" id="tab-login-register" style="max-width: 400px;">
			
			<div class="tab-container">
				<div class="tab-content clearfix" id="tab-password">
					<h4>{{$user->firstname}} {{$user->lastname}}</h4>
					<form class="nobottommargin" action="{{url('sta/changeforgotPassword')}}" method="post">

							 {{ csrf_field() }}
					    <input type="hidden" name="remember_token" value="{{ Request::segment(3) }}">

						<div class="col_full">
							<label for="lastname">Yeni Şifre:</label>
							<input type="password" id="newPassword" name="newPassword"   class="form-control"  />
							@if ($errors->has('newPassword'))
		                        <div id="newPassword-error" class="form-control-feedback">{{ $errors->first('newPassword') }}</div>
		                    @endif
						</div>
						<div class="col_full">
							<label for="email">Yeni Şifre Tekrar:</label>
							<input type="password" id="newPassword_confirmation" name="newPassword_confirmation"   class="form-control"   />
							@if ($errors->has('newPassword_confirmation'))
		                        <div id="newPassword_confirmation-error" class="form-control-feedback">{{ $errors->first('newPassword_confirmation') }}</div>
		                    @endif
						</div>
						<div class="col_full nobottommargin text-center">
							<button class="button button-3d button-black nomargin" id="" name="" value="withoutLogin">Güncelle</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
@endif	



		
							
