@php
	$user = Auth::user();
@endphp
 <div class="row">
 	<div class="col-lg-4">

		<div class="heading-block noborder">
			<h3>Profil ve Ayarlar</h3>
			<span>{{$user->firstname." ".$user->lastname }}</span>
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			@if(session('success'))
            	<div class="alert alert-success"><strong>{{session('success')}}</strong></div>		   
			@endif
		</div>
	</div>	

	<div class="col-lg-8">

		<div class="tabs tabs-alt clearfix" id="tabs-profile">

			<ul class="tab-nav clearfix">
				<li><a href="#tab-info"><i class="fas fa-info-circle"></i> Bilgilerim</a></li>
				<li><a href="#tab-password"><i class="fas fa-key"></i> Şifre Değiştirme</a></li>
			</ul> 

			<div class="tab-container">

				<div class="tab-content clearfix" id="tab-info">
					<form id="" name="" class="nobottommargin" action="{{url('sta/changeUserInfo')}}" method="post">

							 {{ csrf_field() }}

						<div class="col_full">
							<label for="name">İsim:</label>
							<input type="text" id="firstname" name="firstname" value="{{$user->firstname}}" class="form-control"  />
						</div>
						<div class="col_full">
							<label for="lastname">Soyisim:</label>
							<input type="text" id="lastname" name="lastname" value="{{$user->lastname }}"  class="form-control"  />
						</div>
						<div class="col_full">
							<label for="email">Email Address:</label>
							<input type="text" id="email" name="email" value="{{$user->email }}"  class="form-control"readonly/>
						</div>
						<div class="col_full nobottommargin">
							<button class="button button-3d button-black nomargin" id="withoutLogin_form_submit" name="withoutLogin_form_submit" value="withoutLogin">Güncelle</button>
						</div>
					</form>

				</div>
		
				<div class="tab-content clearfix" id="tab-password">
					<form class="nobottommargin" action="{{url('sta/changePassword')}}" method="post">

							 {{ csrf_field() }}

						<div class="col_full">
							<label for="name">Eski Şifre:</label>
							<input type="password" id="oldPassword" name="oldPassword"  class="form-control"  />
							@if ($errors->has('firstname'))
		                        <div id="oldPassword-error" class="form-control-feedback">{{ $errors->first('oldPassword') }}</div>
		                    @endif
						</div>
						<div class="col_full">
							<label for="lastname">Yeni Şifre:</label>
							<input type="password" id="newPassword" name="newPassword"   class="form-control"  />
							@if ($errors->has('newPassword'))
		                        <div id="newPassword-error" class="form-control-feedback">{{ $errors->first('newPassword') }}</div>
		                    @endif
						</div>
						<div class="col_full">
							<label for="email">Yeni Şifre Tekrar:</label>
							<input type="password" id="newPassword_confirmation" name="newPassword_confirmation"   class="form-control"   />
							@if ($errors->has('newPassword_confirmation'))
		                        <div id="newPassword_confirmation-error" class="form-control-feedback">{{ $errors->first('newPassword_confirmation') }}</div>
		                    @endif
						</div>
						<div class="col_full nobottommargin">
							<button class="button button-3d button-black nomargin" id="" name="" value="withoutLogin">Güncelle</button>
						</div>
					</form>

				</div>
			
			</div>

		</div>

	</div>
 </div>
						

					
