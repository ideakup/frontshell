
<div class="d-flex justify-content-center">
	<div class="tab-container">
		@if (\Session::has('success'))
			<div class="style-msg successmsg">
				<div class="sb-msg"><i class="icon-thumbs-up"></i><strong>Tebrikler!</strong>{!! \Session::get('success') !!} </div>
			</div>
		@else
			<div class="tab-content clearfix" id="tab-subrecord">
				<div class="card nobottommargin">
					<div class="card-body" style="padding: 40px;">
						<h3>Hesabınızı Oluşturun </h3>
						@if($errors->any())
							<div class="style-msg errormsg">
								<div class="sb-msg"><i class="icon-remove"></i><strong>HATA!</strong> {{$errors->first()}}</div>
							</div>
						@endif
						

						<form id="subrecord-form" name="subrecord" class="nobottommargin" action="{{url('sta/subrecord')}}" method="post">
								 {{ csrf_field() }}

							<div class="form-group @if ($errors->has('firstname')) has-danger @endif" >
								<label for="subrecord-form-name">İsim:</label>
								<input type="text" id="subrecord-form-firstname" name="firstname" value="{{old('firstname')}}" class="form-control" />
								@if ($errors->has('firstname'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('firstname') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('lastname')) has-danger @endif" >
								<label for="subrecord-form-lastname">Soyisim:</label>
								<input type="text" id="subrecord-form-lastname" name="lastname" value="{{old('lastname')}}" class="form-control" />
								@if ($errors->has('lastname'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('lastname') }}</div>
			                    @endif
							</div>

							<div class="form-group @if ($errors->has('email')) has-danger @endif" >
								<label for="subrecord-form-email">E-Posta Adresi :</label>
								<input type="text" id="subrecord-form-email" name="email" value="{{old('email')}}" class="form-control" />
								@if ($errors->has('email'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('email') }}</div>
			                    @endif
							</div>
							<div class="form-group @if ($errors->has('password')) has-danger @endif" >
								<label for="subrecord-form-password">Şifre Belirle:</label>
								<input type="password" id="subrecord-form-password" name="password" value="" class="form-control" />
								@if ($errors->has('password'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('password') }}</div>
			                    @endif
							</div>

							<div class="form-group @if ($errors->has('repassword')) has-danger @endif" >
								<label for="subrecord-form-repassword">Şifre Tekrar:</label>
								<input type="password" id="subrecord-form-repassword" name="repassword" value="" class="form-control" />
								@if ($errors->has('repassword'))
			                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('repassword') }}</div>
			                    @endif
							</div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="subrecord-form-submit" name="subrecord-form-submit" value="subrecord">Kayıt Ol</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
		@endif
	</div>
</div>