@php
@endphp



				<div class="d-flex justify-content-center">

					<div class="tabs divcenter nobottommargin clearfix" id="tab-login-register" style="max-width: 500px;">
						<ul class="tab-nav tab-nav2 center clearfix">
							<li class="inline-block"><a href="#tab-login">Giriş Yap</a></li>
							<li class="inline-block"><a href="#tab-register">Kayıt Ol</a></li>
							<li class="inline-block"><a href="#tab-forgetpassword">Şifremi Unuttum</a></li>
						</ul>
						<div class="tab-container">
							<div class="tab-content clearfix" id="tab-login">
								<div class="card nobottommargin">
									<div class="card-body" style="padding: 40px;">


										<form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="post">
               								 {{ csrf_field() }}
											@if ($errors->has('email'))
			                                   <div class="alert alert-danger"> <strong >E-mail yada Şifre yanlış</strong></div>
		                                 	@endif

			                                @if ($errors->has('password'))
			                                   <div class="alert alert-danger"><strong>E-mail yada Şifre yanlış</strong></div>
			                                @endif
			                                @if ($errors->first()=="non_exist_email")
			                                   <div class="alert alert-danger"><strong>Girilen E-mail kayıtlı değil</strong></div>
			                                @endif

			                                @if(session('success'))
			                                	<div class="alert alert-success"><strong>{{session('success')}}</strong></div>
											   
											@endif
											<h3>Giriş Yap</h3>

											<div class="col_full">
												<label for="login-form-username">E-posta:</label>
												<input type="email" id="email" name="email" placeholder="E-posta" required class="form-control" />
											</div>

											<div class="col_full">
												<label for="login-form-password">Şifre:</label>
												<input type="password" id="password" name="password" placeholder="Şifre" required class="form-control" />
											</div>

											<div class="col_full nobottommargin">
												<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Giriş Yap</button>
											</div>

										</form>
									</div>
								</div>
							</div>
							<div class="tab-content clearfix" id="tab-register">
								<div class="card nobottommargin">
									<div class="card-body" style="padding: 40px;">
										<h3>Kayıt Ol</h3>

										<form id="register-form" name="register-form" class="nobottommargin" action="{{url('sta/manualregister')}}" method="post">
               								 {{ csrf_field() }}

											<div class="form-group @if ($errors->has('firstname')) has-danger @endif" >
												<label for="register-form-name">İsim:</label>
												<input type="text" id="register-form-firstname" name="firstname" value="{{old('firstname')}}" class="form-control" />
												@if ($errors->has('firstname'))
							                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('firstname') }}</div>
							                    @endif
											</div>
											<div class="form-group @if ($errors->has('lastname')) has-danger @endif" >
												<label for="register-form-lastname">Soyisim:</label>
												<input type="text" id="register-form-lastname" name="lastname" value="{{old('lastname')}}" class="form-control" />
												@if ($errors->has('lastname'))
							                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('lastname') }}</div>
							                    @endif
											</div>

											<div class="form-group @if ($errors->has('email')) has-danger @endif" >
												<label for="register-form-email">E-Posta Adresi :</label>
												<input type="text" id="register-form-email" name="email" value="{{old('email')}}" class="form-control" />
												@if ($errors->has('email'))
							                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('email') }}</div>
							                    @endif
											</div>
											<div class="form-group @if ($errors->has('password')) has-danger @endif" >
												<label for="register-form-password">Şifre Belirle:</label>
												<input type="password" id="register-form-password" name="password" value="" class="form-control" />
												@if ($errors->has('password'))
							                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('password') }}</div>
							                    @endif
											</div>

											<div class="form-group @if ($errors->has('repassword')) has-danger @endif" >
												<label for="register-form-repassword">Şifre Tekrar:</label>
												<input type="password" id="register-form-repassword" name="repassword" value="" class="form-control" />
												@if ($errors->has('repassword'))
							                        <div id="phone-error" class="form-control-feedback">{{ $errors->first('repassword') }}</div>
							                    @endif
											</div>

											<div class="col_full nobottommargin">
												<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Kayıt Ol</button>
											</div>

										</form>
									</div>
								</div>
							</div>
							<div class="tab-content clearfix" id="tab-forgetpassword">
								<div class="card nobottommargin">
									<div class="card-body" style="padding: 40px;">
										<form id="forgetpassword-form" name="forgetpassword-form" class="nobottommargin" 
										action="{{ url('sta/forgetpassword') }}" method="post">
               								 {{ csrf_field() }}
											
											<h3>Şifremi Unuttum</h3>

											<div class="col_full">
												<label for="login-form-eposta">E-posta:</label>
												<input type="email" id="forgotemail" name="forgotemail" placeholder="E-posta" required class="form-control" />
											</div>
											<div class="col_full nobottommargin text-center">
												<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-forgotemail" value="forgotemail">Şifremi Yenile</button>
											</div>

										</form>
									</div>
								</div>
							</div>					
						</div>
					</div>

				</div>



