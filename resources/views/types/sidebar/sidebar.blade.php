	@php
	if (empty($sidebar_id)) {
		$sidebar_id = $menu->id;

	}
	
	$sidebarElements=App\NewTopHasSub::where('top_id',$sidebar_id)->where('sub_model','Sidebar')->where('deleted','no')->where('status','active')->orderBy('order')->get();

	@endphp				

	<div class="sidebar col-lg-3">
		<div class="sidebar-widgets-wrap">


			@foreach($sidebarElements as $sidebarElement)
				@php
					$content = $sidebarElement->thatElementForContent;
					$contVariable = $content->variableLang($lang);
					$ths = $sidebarElement;

				@endphp
				<div class="widget clearfix">

					@if ($content->type == 'text')
	                    @include('types.menupartials.basiccontents.text')
	                @elseif ($content->type == 'title')
	                    @include('types.menupartials.basiccontents.title')
	                @elseif ($content->type == 'photo')
	                    @include('types.menupartials.basiccontents.photo')
	                @elseif ($content->type == 'link')
	                    @include('types.menupartials.basiccontents.link')
	                @elseif ($content->type == 'video')
	                    @include('types.menupartials.basiccontents.video')
	                @elseif ($content->type == 'seperator')
	                    @include('types.menupartials.basiccontents.seperator')
	                @elseif ($content->type == 'code')
	                    @include('types.menupartials.basiccontents.code')

	                <!-- Complex Contents-->   
	                @elseif ($content->type == 'photogallery')
	                    @include('types.menupartials.complexcontents.photogallery')
	                @elseif ($content->type == 'slide')
	                    @php $slideData = $content->slide; @endphp
	                    @include('types.menupartials.complexcontents.slide')
	                @elseif ($content->type == 'form')
	                    @include('types.menupartials.complexcontents.form')
	                @elseif ($content->type == 'sidebar_summary')
	                   @include('types.menupartials.sidebar_summary')
	                <!-- sidebar-->
	                @elseif ($content->type == 'sidemenu')
	                    @include('types.sidebar.sidemenu')
	                @elseif ($content->type == 'tags')
	                    @include('types.sidebar.tags')
	                @elseif ($content->type == 'cats')
	                    @include('types.sidebar.cats')
	                @endif

				</div>
			@endforeach

		

		</div>

	</div><!-- .sidebar end -->
