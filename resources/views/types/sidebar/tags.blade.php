@php
/*if(is_null(Request::get('cat'))){
	$tag_ids=App\ContentHasTag::whereIn('content_id',$content_ids)->pluck('tag_id');
	$tags=App\Tag::whereIn('id',$tag_ids)->get();
}else{
	$category_id =App\CategoryVariable::where('slug',Request::get('cat'))->pluck('category_id');
	$content_ths_id=App\CategoryTopHasSub::where('category_id',$category_id)->pluck('id');
	$content_ids =App\ContentHasCategory::where('category_id',$content_ths_id)->pluck('content_id');
	$tag_ids=App\ContentHasTag::whereIn('content_id',$content_ids)->pluck('tag_id');
	$tags=App\Tag::whereIn('id',$tag_ids)->get();
}sonra bakılacak*/ 
	$tag_ids=App\ContentHasTag::whereIn('content_id',$content_ids)->pluck('tag_id');
	$tags=App\Tag::whereIn('id',$tag_ids)->get();

@endphp

@if(count($tags) > 0)
	<div id="tags" class="widget clearfix">
		<h6>Etiketler</h6>
		<div class="tagcloud">
			@foreach($tags as $tag)
				@php 
					if (empty($tag->variableLang($lang))) {
	                    $tagVariable = $tag->variable;
	                }else{
	                    $tagVariable = $tag->variableLang($lang);
	                }

		            $link="";
		                if((!empty(Request::all()))){
		                    if(is_null(Request::get('tag'))){
		                        $link=Request::fullUrl()."&tag=".$tagVariable->slug;
		                    }
		                    else{
		                        $tag=Request::all();
		                       
		                        
		                        $tag['tag']=$tagVariable->slug;
		                        
		                        $link=Request::url()."?".http_build_query($tag, '', '&');
		                    }
		                }
		                else{
		                    $link=Request::fullUrl()."?tag=".$tagVariable->slug;;
		                }
		                
		               // dd(  $pag);
	            @endphp
				<a @if(Request::input('tag') == $tagVariable->slug) class="chosentag" @endif  href="{{ $link }}">{{ $tagVariable->title }} </a>
			@endforeach
		</div>
	</div>
@endif

