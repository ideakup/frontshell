@php
	$topCat=App\CategoryTopHasSub::whereIn('category_id',json_decode($contVariable->content)->cats)->get();		

@endphp


<div id="shortcodes" class="widget widget_links clearfix">

	<h6>
		Kategoriler
	</h6>
	@foreach($topCat as $cth)
		@php 
			if (empty($cth->variableLang($lang))) {
                $topcatVariable = $cth->variable;
            }else{
                $topcatVariable = $cth->variableLang($lang);
            }

            $link="";
            if((!empty(Request::all()))){
                if(is_null(Request::get('cat'))){
                    $link=Request::fullUrl()."&cat=".$topcatVariable->slug;

                }
                else{
                    $cat=Request::all();
                    $cat['cat']=$topcatVariable->slug;
                    
                    $link=Request::url()."?".http_build_query($cat, '', '&');

                }
                
            }
            else{
                $link=Request::fullUrl()."?cat=".$topcatVariable->slug;
            }

		@endphp	
		<ul>
			@if($cth->subCat->count()>0)
					<li>
					    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
				            <div>
			          			{{$topcatVariable->title}}   
				            </div>
				        </a>
			    	</li>    
					@php
						$topCat=$cth->subCat->sortBy('order');
					@endphp
						@foreach($topCat as $cth)
							@php 
								if (empty($cth->variableLang($lang))) {
					                $topcatVariable = $cth->variable;
					            }else{
					                $topcatVariable = $cth->variableLang($lang);
					            }

					            $link="";
					            if((!empty(Request::all()))){
					                if(is_null(Request::get('cat'))){
					                    $link=Request::fullUrl()."&cat=".$topcatVariable->slug;
					                }
					                else{
					                    $cat=Request::all();
					                   
					                   // dump($pag);
					                    $cat['cat']=$topcatVariable->slug;
					                    
					                    $link=Request::url()."?".http_build_query($cat, '', '&');

					                }
					            }
					            else{
					                $link=Request::fullUrl()."?cat=".$topcatVariable->slug;
					            }
							@endphp	
							<ul>
								@if($cth->subCat->count()>0)
										<li>
										    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
									            <div>
								          			{{$topcatVariable->title}}   
									            </div>
									        </a>
								    	</li>    
										@php
											$topCat=$cth->subCat->sortBy('order');
										@endphp
											@foreach($topCat as $cth)
												@php 
													if (empty($cth->variableLang($lang))) {
										                $topcatVariable = $cth->variable;
										            }else{
										                $topcatVariable = $cth->variableLang($lang);
										            }

										            $link="";
										            if((!empty(Request::all()))){
										                if(is_null(Request::get('cat'))){
										                    $link=Request::fullUrl()."&cat=".$topcatVariable->slug;
										                }
										                else{
										                    $cat=Request::all();
										                   
										                   // dump($pag);
										                    $cat['cat']=$topcatVariable->slug;
										                    
										                    $link=Request::url()."?".http_build_query($cat, '', '&');

										                }
										            }
										            else{
										                $link=Request::fullUrl()."?cat=".$topcatVariable->slug;
										            }
												@endphp	
												<ul>
													@if(Request::input('cat') == $cth->topCategoryVariable->slug || Request::input('cat') ==$topcatVariable->slug )
														 <li>
														    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
													            <div>
												          			{{$topcatVariable->title}}  
													            </div>
													        </a>
													    </li>
														@php
															$topCat=$cth->subCat->sortBy('order');
														@endphp
															@foreach($topCat as $cth)
																@php 
																	if (empty($cth->variableLang($lang))) {
														                $topcatVariable = $cth->variable;
														            }else{
														                $topcatVariable = $cth->variableLang($lang);
														            }

														            $link="";
														            if((!empty(Request::all()))){
														                if(is_null(Request::get('cat'))){
														                    $link=Request::fullUrl()."&=".$topcatVariable->slug;
														                }
														                else{
														                    $cat=Request::all();
														                   
														                   // dump($pag);
														                    $cat['cat']=$topcatVariable->slug;
														                    
														                    $link=Request::url()."?".http_build_query($cat, '', '&');

														                }
														            }
														            else{
														                $link=Request::fullUrl()."?cat=".$topcatVariable->slug;
														            }
														            //dump($cth->topCategoryVariable->slug);
																@endphp	
																<ul>
																	@if(Request::input('cat') == $cth->topCategoryVariable->slug || Request::input('cat') ==$topcatVariable->slug )
																		 <li>
																		    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
																	            <div>
																          			{{$topcatVariable->title}}  
																	            </div>
																	        </a>
																	    </li>
																		
																	@endif

																</ul>
														    @endforeach
													@endif

												</ul>
										    @endforeach
							    @else
								    <li>
									    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
								            <div>
							          			{{$topcatVariable->title}}  
								            </div>
								        </a>
								    </li>    
								@endif

							</ul>
					    @endforeach


			    @else
			    <li>
				    <a @if(Request::input('cat') == $topcatVariable->slug) class="chosencat" @endif  href="{{ $link }}">
			            <div>
		          			{{$topcatVariable->title}}  
			            </div>
			        </a>
			    </li>    
			@endif

		</ul>
    @endforeach

</div>


	