<div class="grid-inner row g-0">
	<div class="col-md-6">
		@if(!empty($group_content->photo ))
			<div class="entry-image">
				<a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$group_content->photo }}" alt="{{$contVariable->title}}"></a>
			</div>
		@endif
	</div>
	<div class="col-md-6 ps-md-4">
		<div class="entry-title title-sm">
			<h2><a href="{{$href_link}}">{{$contVariable->title}}</a></h2>
		</div>

		<div class="entry-content">
			@if(!empty($group_content->short_content ))
				<p>{{$group_content->short_content}}</p>
			@endif
			<a href="{{$href_link}}" class="more-link">Devamı için tılayınız...</a>
			@if(!empty($group_content->toogle ))
				@if($group_content->toogle == 'on')
					<div style="float: right; margin: 0;"  class="entry-meta ">
						<ul>
							<li><i class="icon-calendar3"></i> {{ $contVariable->created_at->format('d.m.Y') }} </li>
						</ul>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>