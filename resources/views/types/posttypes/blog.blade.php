
<div class="grid-inner">
    @if(!empty($photo))
        <div class="entry-image">
            <a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$photo }}" alt="{{$title}}"></a>
        </div>
    @endif
    <div class="entry-title">
        <h2><a href="{{$href_link}}">{{$title}}</a></h2>
    </div>
    
    <div class="entry-content">
        @if(!empty($short_content ))
            <p>{{$short_content}}</p>
        @endif
        <a href="{{$href_link}}" class="more-link">Devamı için tılayınız...</a>
        @if(!empty($toogle ))
            @if($toogle == 'on')
                <div style="float: right; margin: 0;"  class="entry-meta ">
                    <ul>
                        <li><i class="icon-calendar3"></i> {{ $created_at->format('d.m.Y') }} </li>
                    </ul>
                </div>
            @endif
        @endif
    </div>
</div>