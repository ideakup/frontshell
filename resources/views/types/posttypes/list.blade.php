	<div class="entry col-12">
		<div class="grid-inner row g-0">
			<div class="col-auto">
				@if(!empty($photo))
			        <div class="entry-image">
			            <a href="{{$href_link}}"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$photo }}" alt="{{$title}}"></a>
			        </div>
			    @endif
			</div>
			<div class="col ps-3">
				<div class="entry-title">
			        <h4><a href="{{$href_link}}">{{$title}}</a></h4>
			    </div>
				<div class="entry-meta">
					<ul>
						@if($sum_content->type =='group-product')
							<li> 
								<ins>{{number_format((float)$group_content->price+($group_content->price*$group_content->tax_rate/100), 2, '.', '')}} ₺</ins> /	
										
								<i class="icon-star3"></i>
								<i class="icon-star3"></i>
								<i class="icon-star3"></i>
								<i class="icon-star3"></i>
								<i class="icon-star-half-full"></i>
							</li>
						@elseif($sum_content->type =='group-blog')
							<li>{{ $created_at->format('d.m.Y') }}</li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>

