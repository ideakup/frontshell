    <div class="product grid-inner">
        <div class="product-image">
            @if(!empty($group_content->photo))
                <img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{$photo }}" alt="{{$title}}">
            @endif
            @if(!empty($stock))
                @if($stock == 'yok')
                    <div class="sale-flash badge bg-secondary p-2">Out of Stock</div>
                @endif
            @endif
            @if($menuformodule->where('type',"module-shop")->count() > 0 )
                @if($stock != 'yok')  
                    <div class="bg-overlay">
                        <div class="bg-overlay-content align-items-end justify-content-between" data-hover-animate="fadeIn" data-hover-speed="400">
                            <div>
                                <a href="#" data-slug="{{$slug}}" class="add-to-cart btn btn-dark me-2"><i class="icon-shopping-basket"></i><span> Sepete Ekle </span></a>
                            </div>
                            <div>
                                <a href="#" data-slug="{{$slug}}" class="buy-now btn btn-dark" ><i class="fas fa-shopping-bag"></i><span> Hemen Al </span></a>
                            </div>
                        </div>
                        <div class="bg-overlay-bg bg-transparent"></div>
                    </div>
                @endif
            @endif
        </div>
        <div class="product-desc">
            <div class="product-title"><h3><a href="{{$href_link }}">{{$title}}</a></h3></div>
            <div class="product-price">
            @if(!empty($group_content->stock))
                @if(!($group_content->stock=='yok'))
                    @if(!empty($group_content->discounted_price) && !empty($group_content->price))
                        <del>{{number_format((float)$group_content->price+($group_content->price*$group_content->tax_rate/100), 2, '.', '')}} ₺</del>
                        <ins>{{number_format((float)$group_content->discounted_price+($group_content->discounted_price*$group_content->tax_rate/100), 2, '.', '')}} ₺
                        </ins>
                    @else
                        @if(!empty($group_content->price))  
                            <ins>{{number_format((float)$group_content->price+($group_content->price*$group_content->tax_rate/100), 2, '.', '')}} ₺</ins>
                        @endif
                    @endif
                @endif  
            @endif
            <div style="float: right;" class="product-rating">
                <i class="icon-star3"></i>
                <i class="icon-star3"></i>
                <i class="icon-star3"></i>
                <i class="icon-star3"></i>
                <i class="icon-star-half-full"></i>
            </div>
          </div>
        </div>
    </div>
