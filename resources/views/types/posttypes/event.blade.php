@php
//dd($content->contentThs->first()->topElementForMenu->variable);
@endphp
    <div class="entry clearfix">
        <div class="entry-list-continer">

            <div class="entry-image">
                <a href="{{ url($langSlug.'/'.$content->contentThs->first()->topElementForMenu->variable->slug.'/'.$content->variableLang($lang)->slug) }}">
                    @php $isAvailable = false; @endphp
                        @if(!empty($group_content->photo))
                            <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'. $group_content->photo) }}" />
                            @php $isAvailable = true; @endphp
                        @endif
            
                    @if (!$isAvailable)
                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                    @endif
                </a>
                @if(!empty( $topmenu))
                    <div class="sale-flash">{{ $topmenu->variable->name}}</div>
                @endif
            </div>
            <div class="entry-title">
                <h2>
                <a href="{{ url($langSlug.'/'.$content->contentThs->first()->topElementForMenu->variable->slug.'/'.$content->variableLang($lang)->slug) }}">
                        {{ $content->variableLang($lang)->title }}
                    </a>
                </h2>
            </div>
            @php
           // dd($group_content->short_content);
            @endphp
            <div class="entry-content">
                @if(!empty($group_content->short_content))
                <a href="{{ url($langSlug.'/'.$content->contentThs->first()->topElementForMenu->variable->slug.'/'.$content->variableLang($lang)->slug) }}">
                        <p>{{ $group_content->short_content }}</p>
                    </a>
                @endif
            </div> 
            
            
        </div>
    </div>

